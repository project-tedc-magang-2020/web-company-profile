<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Foto;

class FotoController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $fotos = DB::table('tb_foto')->where('nama_kegiatan', 'LIKE', '%' .$request->search.'%');
        }else{
            $fotos = DB::table('tb_foto');
        }
        $fotos = $fotos->get();

        $q = DB::table('tb_foto')->select(DB::raw('MAX(RIGHT(id_foto,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('website.foto', compact('fotos','kd'));
    }

    public function create()
    {

        return view('website.foto');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_foto' => 'required',
            'nama_kegiatan' => 'required',
            'gambar' => 'required',
            'tanggal' => 'required',
        ]);

        if ($request->hasFile('gambar')) {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        $foto = foto::create([
        'id_foto' => $request->id_foto,
        'nama_kegiatan' => $request->nama_kegiatan,
        'gambar' => $namaFileBaru,
        'tanggal' => $request->tanggal,
        ]
    );
    }

        if($foto){
        //redirect dengan pesan sukses
            return redirect()->route('website.foto')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('website.foto')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_foto)
    {
        $fotos = foto::where('id_foto',$id_foto)->get();

        return view('website.foto', compact('fotos'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);
            
        DB::table('tb_foto')->where('id_foto',$request->id_foto)->update([
            'nama_kegiatan' => $request->nama_kegiatan,
            'gambar' => $namaFileBaru,
            'tanggal' => $request->tanggal,
        ]);

        return redirect('/website/foto')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_foto)
    {
        DB::table('tb_foto')->where('id_foto', $no)->delete();

        return redirect ('/website/foto')->with(['success' => 'Data Deleted Successfully!']);
    }
}
