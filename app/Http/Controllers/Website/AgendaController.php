<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Agenda;

class AgendaController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $agendas = DB::table('tb_agenda')->where('nama_agenda', 'LIKE', '%' .$request->search.'%');
        }else{
            $agendas = DB::table('tb_agenda');
        }
        $agendas = $agendas->get();

        $q = DB::table('tb_agenda')->select(DB::raw('MAX(RIGHT(id_agenda,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('website.agenda', compact('agendas', 'kd'));
    }

    public function create()
    {

        return view('website.agenda');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_agenda' => 'required',
            'nama_agenda' => 'required',
            'tempat' => 'required',
            'waktu' => 'required',

        ]);

        $agenda = agenda::create([
        'id_agenda' => $request->id_agenda,
        'nama_agenda' => $request->nama_agenda,
        'tempat' => $request->tempat,
        'waktu' => $request->waktu,

        ]);

        if($agenda){
        //redirect dengan pesan sukses
            return redirect()->route('website.agenda')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('website.agenda')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_agenda)
    {
        $agendas = agenda::where('id_agenda',$id_agenda)->get();

        return view('website.agenda', compact('agendas'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_agenda')->where('id_agenda',$request->id_agenda)->update([
            'nama_agenda' => $request->nama_agenda,
            'tempat' => $request->tempat,
            'waktu' => $request->waktu,
        ]);

        return redirect('/website/agenda')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_agenda)
    {
        DB::table('tb_agenda')->where('id_agenda', $id_agenda)->delete();

        return redirect ('/website/agenda')->with(['success' => 'Data Deleted Successfully!']);
    }
}
