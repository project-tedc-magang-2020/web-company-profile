<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\DownloadFile;

class DownloadFileController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $downloadfiles = DB::table('tb_download_file')->where('nama_file', 'LIKE', '%' .$request->search.'%');
        }else{
            $downloadfiles = DB::table('tb_download_file');
        }
        $downloadfiles = $downloadfiles->get();

        $q = DB::table('tb_download_file')->select(DB::raw('MAX(RIGHT(id_download,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('website.downloadfile', compact('downloadfiles','kd'));
    }

    public function create()
    {

        return view('website.downloadfile');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_download' => 'required',
            'nama_file' => 'required',
        ]);

        $downloadfile = DownloadFile::create([
        'id_download' => $request->id_download,
        'nama_file' => $request->nama_file,
        ]
    );
        if($downloadfile){
        //redirect dengan pesan sukses
            return redirect()->route('website.downloadfile')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('website.downloadfile')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_download)
    {
        $downloadfiles = DownloadFile::where('id_download_file',$id_download)->get();

        return view('website.downloadfile', compact('downloadfiles'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_download_file')->where('id_download',$request->id_download)->update([
            'nama_file' => $request->nama_file,
        ]);

        return redirect('/website/downloadfile')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_download_file)
    {
        DB::table('tb_download_file')->where('id_download', $no)->delete();

        return redirect ('/website/downloadfile')->with(['success' => 'Data Deleted Successfully!']);
    }
}
