<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Video;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $videos = DB::table('tb_video')->where('nama_kegiatan', 'LIKE', '%' .$request->search.'%');
        }else{
            $videos = DB::table('tb_video');
        }
        $videos = $videos->get();

        $q = DB::table('tb_video')->select(DB::raw('MAX(RIGHT(id_video,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('website.video', compact('videos','kd'));
    }

    public function create()
    {

        return view('website.video');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_video' => 'required',
            'nama_kegiatan' => 'required',
            'video' => 'required|file|mimetypes:video/mp4',
            'tanggal' => 'required',
        ]);

        if ($request->hasFile('video')) {
            $ambil=$request->file('video');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        $video = video::create([
        'id_video' => $request->id_video,
        'nama_kegiatan' => $request->nama_kegiatan,
        'video' => $namaFileBaru,
        'tanggal' => $request->tanggal,
        ]
    );
    }

        if($video){
        //redirect dengan pesan sukses
            return redirect()->route('website.video')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('website.video')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_video)
    {
        $videos = video::where('id_video',$id_video)->get();

        return view('website.video', compact('videos'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $ambil=$request->file('video');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        DB::table('tb_video')->where('id_video',$request->id_video)->update([
            'nama_kegiatan' => $request->nama_kegiatan,
            'video' => $namaFileBaru,
            'tanggal' => $request->tanggal,
        ]);

        return redirect('/website/video')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_video)
    {
        DB::table('tb_video')->where('id_video', $id_video)->delete();

        return redirect ('/website/video')->with(['success' => 'Data Deleted Successfully!']);
    }
}
