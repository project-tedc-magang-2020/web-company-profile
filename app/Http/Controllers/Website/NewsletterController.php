<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Newsletter;

class NewsletterController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $newsletters = DB::table('tb_newsletter')->where('nama', 'LIKE', '%' .$request->search.'%');
        }else{
            $newsletters = DB::table('tb_newsletter');
        }
        $newsletters = $newsletters->get();

        $q = DB::table('tb_newsletter')->select(DB::raw('MAX(RIGHT(id_newsletter,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('website.newsletter', compact('newsletters', 'kd'));
    }

    public function create()
    {

        return view('website.newsletter');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_newsletter' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'pesan' => 'required',

        ]);

        $newsletter = newsletter::create([
        'id_newsletter' => $request->id_newsletter,
        'nama' => $request->nama,
        'email' => $request->email,
        'pesan' => $request->pesan,

        ]);

        if($newsletter){
        //redirect dengan pesan sukses
            return redirect()->route('website.newsletter')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('website.newsletter')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_newsletter)
    {
        $newsletters = newsletter::where('id_newsletter',$id_newsletter)->get();

        return view('website.newsletter', compact('newsletters'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_newsletter')->where('id_newsletter',$request->id_newsletter)->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'pesan' => $request->pesan,
        ]);

        return redirect('/website/newsletter')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_newsletter)
    {
        DB::table('tb_newsletter')->where('id_newsletter', $id_newsletter)->delete();

        return redirect ('/website/newsletter')->with(['success' => 'Data Deleted Successfully!']);
    }
}
