<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Promosi;

class PromosiController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $promosis = DB::table('tb_promosi')->where('tanggal', 'LIKE', '%' .$request->search.'%');
        }else{
            $promosis = DB::table('tb_promosi');
        }
        $promosis = $promosis->get();

        $q = DB::table('tb_promosi')->select(DB::raw('MAX(RIGHT(id_promosi,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('website.promosi', compact('promosis', 'kd'));
    }

    public function create()
    {

        return view('website.promosi');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_promosi' => 'required',
            'nama_promosi' => 'required',
            'tanggal' => 'required',
            'keterangan' => 'required',


        ]);

        $promosi = promosi::create([
        'id_promosi' => $request->id_promosi,
        'nama_promosi' => $request->nama_promosi,
        'tanggal' => $request->tanggal,
        'keterangan' => $request->keterangan,

        ]);

        if($promosi){
        //redirect dengan pesan sukses
            return redirect()->route('website.promosi')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('website.promosi')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_promosi)
    {
        $promosis = promosi::where('id_promosi',$id_promosi)->get();

        return view('website.promosi', compact('promosis'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_promosi')->where('id_promosi',$request->id_promosi)->update([
            'nama_promosi' => $request->nama_promosi,
            'tanggal' => $request->tanggal,
            'keterangan' => $request->keterangan,

        ]);

        return redirect('/website/promosi')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_promosi)
    {
        DB::table('tb_promosi')->where('id_promosi', $id_promosi)->delete();

        return redirect ('/website/promosi')->with(['success' => 'Data Deleted Successfully!']);
    }
}
