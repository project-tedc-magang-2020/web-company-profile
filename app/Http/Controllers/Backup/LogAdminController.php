<?php

namespace App\Http\Controllers\Backup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LogAdmin;

class LogAdminController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $logadmins = DB::table('tb_log_admin')->where('user', 'LIKE', '%' .$request->search.'%');
        }else{
            $logadmins = DB::table('tb_log_admin');
        }
        $logadmins = $logadmins->get();

        $q = DB::table('tb_log_admin')->select(DB::raw('MAX(RIGHT(id_log_admin,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('backup.logadmin', compact('logadmins', 'kd'));
    }

    public function create()
    {

        return view('backup.logadmin');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_log_admin' => 'required',
            'user' => 'required',
            'waktu' => 'required',
            'keterangan' => 'required',

        ]);

        $logadmin = logadmin::create([
        'id_log_admin' => $request->id_log_admin,
        'user' => $request->user,
        'waktu' => $request->waktu,
        'keterangan' => $request->keterangan,

        ]);

        if($logadmin){
        //redirect dengan pesan sukses
            return redirect()->route('backup.logadmin')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('backup.logadmin')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_logadmin)
    {
        $log_admins = logadmin::where('id_log_admin',$id_log_admin)->get();

        return view('backup.logadmin', compact('logadmins'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_log_admin')->where('id_log_admin',$request->id_log_admin)->update([
            'user' => $request->ip_user,
            'waktu' => $request->waktu,
            'keterangan' => $request->keterangan,

        ]);

        return redirect('/backup/logadmin')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_log_admin)
    {
        DB::table('tb_log_admin')->where('id_log_admin', $id_log_admin)->delete();

        return redirect ('/backup/logadmin')->with(['success' => 'Data Deleted Successfully!']);
    }
}
