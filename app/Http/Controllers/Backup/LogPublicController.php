<?php

namespace App\Http\Controllers\Backup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LogPublic;

class LogPublicController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $logpublics = DB::table('tb_log_public')->where('os', 'LIKE', '%' .$request->search.'%');
        }else{
            $logpublics = DB::table('tb_log_public');
        }
        $logpublics = $logpublics->get();

        $q = DB::table('tb_log_public')->select(DB::raw('MAX(RIGHT(id_log_public,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('backup.logpublic', compact('logpublics', 'kd'));
    }

    public function create()
    {

        return view('backup.logpublic');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_log_public' => 'required',
            'ip_address' => 'required',
            'browser' => 'required',
            'os' => 'required',
            'waktu' => 'required',

        ]);

        $logpublic = logpublic::create([
        'id_log_public' => $request->id_log_public,
        'ip_address' => $request->ip_address,
        'browser' => $request->browser,
        'os' => $request->os,
        'waktu' => $request->waktu,

        ]);

        if($logpublic){
        //redirect dengan pesan sukses
            return redirect()->route('backup.logpublic')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('backup.logpublic')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_logpublic)
    {
        $log_publics = logpublic::where('id_log_public',$id_log_public)->get();

        return view('backup.logpublic', compact('logpublics'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_log_public')->where('id_log_public',$request->id_log_public)->update([
            'ip_address' => $request->ip_address,
            'browser' => $request->browser,
            'os' => $request->os,
            'waktu' => $request->waktu,
        ]);

        return redirect('/backup/logpublic')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_log_public)
    {
        DB::table('tb_log_public')->where('id_log_public', $id_log_public)->delete();

        return redirect ('/backup/logpublic')->with(['success' => 'Data Deleted Successfully!']);
    }
}
