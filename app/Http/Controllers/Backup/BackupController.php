<?php

namespace App\Http\Controllers\Backup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Backup;

class BackupController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $backups = DB::table('tb_backup')->where('user', 'LIKE', '%' .$request->search.'%');
        }else{
            $backups = DB::table('tb_backup');
        }
        $backups = $backups->get();

        $q = DB::table('tb_backup')->select(DB::raw('MAX(RIGHT(id_backup,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('backup.backup', compact('backups', 'kd'));
    }

    public function create()
    {

        return view('backup.backup');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_backup' => 'required',
            'user' => 'required',
            'activity' => 'required',
            'waktu' => 'required',

        ]);

        $backup = backup::create([
        'id_backup' => $request->id_backup,
        'user' => $request->user,
        'activity' => $request->activity,
        'waktu' => $request->waktu,

        ]);

        if($backup){
        //redirect dengan pesan sukses
            return redirect()->route('backup.backup')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('backup.backup')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_backup)
    {
        $backups = backup::where('id_backup',$id_backup)->get();

        return view('backup.backup', compact('backups'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_backup')->where('id_backup',$request->id_backup)->update([
                'user' => $request->user,
                'activity' => $request->activity,
                'waktu' => $request->waktu,

        ]);

        return redirect('/backup/backup')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_backup)
    {
        DB::table('tb_backup')->where('id_backup', $id_backup)->delete();

        return redirect ('/backup/backup')->with(['success' => 'Data Deleted Successfully!']);
    }
}
