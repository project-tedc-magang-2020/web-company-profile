<?php

namespace App\Http\Controllers\Logo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LogoAdmin;

class LogoAdminController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $logoadmins = DB::table('tb_logo_admin')->where('logo', 'LIKE', '%' .$request->search.'%');
        }else{
            $logoadmins = DB::table('tb_logo_admin');
        }
        $logoadmins = $logoadmins->get();

        $q = DB::table('tb_logo_admin')->select(DB::raw('MAX(RIGHT(id_logo_admin,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('logo.logoadmin', compact('logoadmins', 'kd'));
    }

    public function create()
    {

        return view('logo.logoadmin');
    }


    public function edit($id_logo_admin)
    {
        $logoadmins = logadmin::where('id_logo_admin',$id_logo_admin)->get();

        return view('logo.logoadmin', compact('logoadmins'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

            $ambil=$request->file('logo');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        DB::table('tb_logo_admin')->where('id_logo_admin',$request->id_logo_admin)->update([
            'logo' => $namaFileBaru,

        ]);

        return redirect('/logo/logoadmin')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_logo_admin)
    {
        DB::table('tb_logo_admin')->where('id_logo_admin', $id_logo_admin)->delete();

        return redirect ('/logo/logoadmin')->with(['success' => 'Data Deleted Successfully!']);
    }
}
