<?php

namespace App\Http\Controllers\Logo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LogoUser;

class LogoUserController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $logousers = DB::table('tb_logo_user')->where('logo', 'LIKE', '%' .$request->search.'%');
        }else{
            $logousers = DB::table('tb_logo_user');
        }
        $logousers = $logousers->get();

        $q = DB::table('tb_logo_user')->select(DB::raw('MAX(RIGHT(id_logo_user,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('logo.logouser', compact('logousers', 'kd'));
    }

    public function create()
    {

        return view('logo.logouser');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_logo_user' => 'required',
            'logo' => 'required',


        ]);

        if ($request->hasFile('logo')) {
            $ambil=$request->file('logo');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        $logo = logouser::create([
            'id_logo_user' => $request->id_logo_user,
            'logo' => $namaFileBaru,

        ]);
    }

        if($logo){
        //redirect dengan pesan sukses
            return redirect()->route('logouser')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('logouser')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_logo_user)
    {
        $logousers = logouser::where('id_logo_user',$id_logo_user)->get();

        return view('logo.logouser', compact('logousers'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_logo_user')->where('id_logo_user',$request->id_logo_user)->update([
            'logo' => $namaFileBaru,

        ]);

        return redirect('/logo/logouser')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_logo_user)
    {
        DB::table('tb_logo_user')->where('id_logo_user', $id_logo_user)->delete();

        return redirect ('/logo/logouser')->with(['success' => 'Data Deleted Successfully!']);
    }
}
