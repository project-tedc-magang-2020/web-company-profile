<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UserManagement;

class UserManagementController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $usermanagements = DB::table('tb_user_management')->where('nama', 'LIKE', '%' .$request->search.'%');
        }else{
            $usermanagements = DB::table('tb_user_management');
        }
        $usermanagements = $usermanagements->get();

        $q = DB::table('tb_user_management')->select(DB::raw('MAX(RIGHT(id_user_management,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('management.usermanagement', compact('usermanagements', 'kd'));
    }

    public function create()
    {

        return view('management.usermanagement');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_user_management' => 'required',
            'username' => 'required',
            'nama' => 'required',
            'role' => 'required',
            'email' => 'required',
            'update_password' => 'required',
            'status' => 'required',

    

        ]);

        $usermanagement = usermanagement::create([
            'id_user_management' => $request->id_user_management,
            'username' => $request->username,
            'nama' => $request->nama,
            'role' => $request->role,
            'email' => $request->email,
            'update_password' => $request->update_password,
            'status' => $request->status,

        ]);

        if($usermanagement){
        //redirect dengan pesan sukses
            return redirect()->route('management.usermanagement')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('management.usermanagement')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_user_management)
    {
        $usermanagements = usermanagement::where('id_user_management',$id_user_management)->get();

        return view('management.usermanagement', compact('usermanagements'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_user_management')->where('id_user_management',$request->id_user_management)->update([
            'username' => $request->username,
            'nama' => $request->nama,
            'role' => $request->role,
            'email' => $request->email,
            'update_password' => $request->update_password,
            'status' => $request->status,
    
        ]);

        return redirect('/management/usermanagement')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_user_management)
    {
        DB::table('tb_user_management')->where('id_user_management', $id_user_management)->delete();

        return redirect ('/management/usermanagement')->with(['success' => 'Data Deleted Successfully!']);
    }
}
