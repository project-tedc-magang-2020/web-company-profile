<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Submenuadmin;
use App\Models\Admin;

class SubmenuAdminController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $subadmins = DB::table('tb_sub_menu_admin')->where('nama_menu', 'LIKE', '%' .$request->search.'%');
        }else{
            $subadmins = DB::table('tb_sub_menu_admin');
        }
        $subadmins = $subadmins->get();

        $data = DB::table('tb_admin')
            ->join('tb_sub_menu_admin', 'tb_sub_menu_admin.id_admin', '=', 'tb_admin.id_admin')
            ->get();

        $admins = DB::table('tb_admin')->get();


        return view('management.submenuadmin', compact('subadmins', 'data', 'admins'));
    }

    public function create()
    {

        return view('management.submenuadmin');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_admin' => 'required',
            'nama_sub_menu' => 'required',
            'link' => 'required',
            'keterangan' => 'required',
            'status' => 'required',


        ]);

        $subadmin = Submenuadmin::create([
            'id_admin' => $request->id_admin,
            'nama_sub_menu' => $request->nama_sub_menu,
            'link' => $request->link,
            'keterangan' => $request->keterangan,
            'icon' => $request->icon,
            'status' => $request->status,

        ]);

        if($subadmin){
        //redirect dengan pesan sukses
            return redirect()->route('management.subadmin')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('management.subadmin')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_admin)
    {
        $subadmins = Submenuadmin::where('id',$id)->get();

        return view('management.submenuadmin', compact('subadmins'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_sub_menu_admin')->where('id',$request->id)->update([
            'id_admin' => $request->id_admin,
            'nama_sub_menu' => $request->nama_sub_menu,
            'link' => $request->link,
            'keterangan' => $request->keterangan,
            'icon' => $request->icon,
            'status' => $request->status,
        ]);

        return redirect('/management/submenuadmin')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_admin)
    {
        DB::table('tb_sub_menu_admin')->where('id', $id)->delete();

        return redirect ('/management/submenuadmin')->with(['success' => 'Data Deleted Successfully!']);
    }
}
