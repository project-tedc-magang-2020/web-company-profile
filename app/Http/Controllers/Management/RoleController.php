<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Role;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $roles = DB::table('tb_role')->where('role_name', 'LIKE', '%' .$request->search.'%');
        }else{
            $roles = DB::table('tb_role');
        }
        $roles = $roles->get();

        $q = DB::table('tb_role')->select(DB::raw('MAX(RIGHT(id_role,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('management.role', compact('roles', 'kd'));
    }

    public function create()
    {

        return view('management.role');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_role' => 'required',
            'role_name' => 'required',
            'keterangan' => 'required',
    

        ]);

        $role = role::create([
            'id_role' => $request->id_role,
            'role_name' => $request->role_name,
            'keterangan' => $request->keterangan,

        ]);

        if($role){
        //redirect dengan pesan sukses
            return redirect()->route('management.role')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('management.role')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_role)
    {
        $roles = role::where('id_role',$id_role)->get();

        return view('management.role', compact('roles'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_role')->where('id_role',$request->id_role)->update([
            'role_name' => $request->role_name,
            'keterangan' => $request->keterangan,
    
        ]);

        return redirect('/management/role')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_role)
    {
        DB::table('tb_role')->where('id_role', $id_role)->delete();

        return redirect ('/management/role')->with(['success' => 'Data Deleted Successfully!']);
    }
}
