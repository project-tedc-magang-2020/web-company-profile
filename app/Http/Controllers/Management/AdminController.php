<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $admins = DB::table('tb_admin')->where('nama_menu', 'LIKE', '%' .$request->search.'%');
        }else{
            $admins = DB::table('tb_admin');
        }
        $admins = $admins->get();

        return view('management.admin', compact('admins'));
    }

    public function create()
    {

        return view('management.admin');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'nama_menu' => 'required',
            'link' => 'required',
            'keterangan' => 'required',
            'icon' => 'required',
            'status' => 'required',
            'sub_menu' => 'required',

        ]);

        $admin = admin::create([
            'nama_menu' => $request->nama_menu,
            'link' => $request->link,
            'keterangan' => $request->keterangan,
            'icon' => $request->icon,
            'status' => $request->status,
            'sub_menu' => $request->sub_menu,

        ]);

        if($admin){
        //redirect dengan pesan sukses
            return redirect()->route('management.admin')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('management.admin')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_admin)
    {
        $admins = admin::where('id_admin',$id_admin)->get();

        return view('management.admin', compact('admins'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_admin')->where('id_admin',$request->id_admin)->update([
            'nama_menu' => $request->nama_menu,
            'link' => $request->link,
            'keterangan' => $request->keterangan,
            'icon' => $request->icon,
            'status' => $request->status,
            'sub_menu' => $request->sub_menu,
        ]);

        return redirect('/management/admin')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_admin)
    {
        DB::table('tb_admin')->where('id_admin', $id_admin)->delete();

        return redirect ('/management/admin')->with(['success' => 'Data Deleted Successfully!']);
    }
}
