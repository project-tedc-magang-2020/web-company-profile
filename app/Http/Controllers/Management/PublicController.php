<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\ModulePublic;

class PublicController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $publics = DB::table('tb_public')->where('nama_menu', 'LIKE', '%' .$request->search.'%');
        }else{
            $publics = DB::table('tb_public');
        }
        $publics = $publics->get();

        $q = DB::table('tb_public')->select(DB::raw('MAX(RIGHT(id_public,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('management.public', compact('publics', 'kd'));
    }

    public function create()
    {

        return view('management.public');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_public' => 'required',
            'nama_menu' => 'required',
            'link' => 'required',
            'keterangan' => 'required',
            'icon' => 'required',
            'status' => 'required',
            'urutan' => 'required',

        ]);

        $public = modulepublic::create([
            'id_public' => $request->id_public,
            'nama_menu' => $request->nama_menu,
            'link' => $request->link,
            'keterangan' => $request->keterangan,
            'icon' => $request->icon,
            'status' => $request->status,
            'urutan' => $request->urutan,

        ]);

        if($public){
        //redirect dengan pesan sukses
            return redirect()->route('management.public')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('management.public')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_public)
    {
        $publics = modulepublic::where('id_public',$id_public)->get();

        return view('management.public', compact('publics'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        DB::table('tb_public')->where('id_public',$request->id_public)->update([
            'nama_menu' => $request->nama_menu,
            'link' => $request->link,
            'keterangan' => $request->keterangan,
            'icon' => $request->icon,
            'status' => $request->status,
            'urutan' => $request->urutan,
        ]);

        return redirect('/management/public')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_public)
    {
        DB::table('tb_public')->where('id_public', $id_public)->delete();

        return redirect ('/management/public')->with(['success' => 'Data Deleted Successfully!']);
    }
}
