<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Authority;

class AuthorityController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $authoritys = DB::table('tb_authority')->where('nama_role', 'LIKE', '%' .$request->search.'%');
        }else{
            $authoritys = DB::table('tb_authority');
        }
        $authoritys = $authoritys->get();

        $q = DB::table('tb_authority')->select(DB::raw('MAX(RIGHT(id_authority,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }
        $menus = DB::table('tb_menu')->get();
        return view('management.authority', compact('authoritys','menus', 'kd'));
    }

    public function create()
    {

        return view('management.authority');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_authority' => 'required',
            'nama_role' => 'required',
            'type' => 'required',


        ]);

        $authority = authority::create([
            'id_authority' => $request->id_authority,
            'nama_role' => $request->nama_role,
            'type' => $request->type,

        ]);

        if($authority){
        //redirect dengan pesan sukses
            return redirect()->route('management.authority')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('management.authority')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_authority)
    {
        $authoritys = authority::where('id_authority',$id_authority)->get();

        return view('management.authority', compact('authoritys'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_authority')->where('id_authority',$request->id_authority)->update([
            'nama_role' => $request->nama_role,
            'type' => $request->type,

        ]);

        return redirect('/management/authority')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_authority)
    {
        DB::table('tb_authority')->where('id_authority', $id_authority)->delete();

        return redirect ('/management/authority')->with(['success' => 'Data Deleted Successfully!']);
    }
}
