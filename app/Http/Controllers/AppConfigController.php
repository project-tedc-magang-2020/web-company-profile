<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\AppConfig;

class AppConfigController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $appconfigs = DB::table('tb_app_config')->where('nama_website', 'LIKE', '%' .$request->search.'%');
        }else{
            $appconfigs = DB::table('tb_app_config');
        }
        $appconfigs = $appconfigs->get();

        $q = DB::table('tb_app_config')->select(DB::raw('MAX(RIGHT(id_app_config,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('appconfig', compact('appconfigs','kd'));
    }

    public function create()
    {

        return view('appconfig');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'nama_website' => 'required',
            'inisial' => 'required',
            'deskripsi' => 'required',
            'keywoard' => 'required',
            'alamat' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'facebook' => 'required',
            'instagram' => 'required',
            'twitter' => 'required',
            'google_plus' => 'required',
            'youtube' => 'required',
            'maps' => 'required',
            'favicon' => 'required',
            'logo_user' => 'required',
            'logo_admin' => 'required',
            'background' => 'required',
        ]);

        if ($request->hasFile('gambar')) {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        $appconfigs = AppConfig::create([
        'nama_website' => $request->nama_website,
        'inisial' => $request->insial,
        'deskripsi' => $request->deskripsi,
        'keywoard' => $request->keywoard,
        'alamat' => $request->alamat,
        'no_telp' => $request->no_telp,
        'email' => $request->email,
        'facebook' => $request->facebook,
        'instagram' => $request->instagram,
        'twitter' => $request->twitter,
        'google_plus' => $request->google_plus,
        'youtube' => $request->youtube,
        'maps' => $request->maps,
        'favicon' => $request->favicon,
        'logo_user' => $request->logo_user,
        'logo_admin' => $request->logo_admin,
        'background' => $request->background,
        ]);
        if($appconfigs){
            //redirect dengan pesan sukses
                return redirect()->route('appconfig')->with(['success' => 'Data Saved Successfully!']);
            }else{
            //redirect dengan pesan error
                return redirect()->route('appconfig')->with(['error' => 'Data Save Failed!']);
            }
    }

    }

    public function edit($id_app_config)
    {
        $appconfigss = AppConfig::where('id_app_config',$id_app_config)->get();

        return view('appconfig', compact('appconfigs'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_app_config')->where('id_app_config',$request->id_app_config)->update([
            'nama_website' => $request->nama_website,
            'inisial' => $request->insial,
            'deskripsi' => $request->deskripsi,
            'keywoard' => $request->keywoard,
            'alamat' => $request->alamat,
            'no_telp' => $request->no_telp,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'twitter' => $request->twitter,
            'google_plus' => $request->google_plus,
            'youtube' => $request->youtube,
            'maps' => $request->maps,
            'favicon' => $request->favicon,
            'logo_user' => $request->logo_user,
            'logo_admin' => $request->logo_admin,
            'background' => $request->background,
        ]);

        return redirect('/appconfig')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_app_config)
    {
        DB::table('tb_app_config')->where('id_app_config', $no)->delete();

        return redirect ('/appconfig')->with(['success' => 'Data Deleted Successfully!']);
    }
}
