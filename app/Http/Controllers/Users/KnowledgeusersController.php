<?php
  
namespace App\Http\Controllers\Users;
  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Knowledge;

  
class KnowledgeusersController extends Controller
{
    public function index()
    {
        $berita= Knowledge::all();
        return view('users.knowledge.knowledge', compact('berita'));
    }
    public function show($id)
    {
        $berita= Knowledge::all()->where('id', $id);
        return view('users.knowledge.show', ['berita' => $berita]);
    }
}