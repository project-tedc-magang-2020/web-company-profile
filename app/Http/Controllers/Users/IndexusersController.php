<?php
  
namespace App\Http\Controllers\Users;
  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Career;
use App\Models\About;
use App\Models\Knowledge;

  
class IndexusersController extends Controller
{
    public function index()
    {
        $konten = About::all();
        $career = Career::all();
        $desk = Career::where('judul', '=', 'Deskripsi')->get();
        $berita= Knowledge::all();
        return view('users.index', compact('konten', 'career', 'desk', 'berita'));
    }
    public function show($id)
    {
        $berita= Knowledge::all()->where('id', $id);
        return view('users.knowledge.show', ['berita' => $berita]);
    }
}