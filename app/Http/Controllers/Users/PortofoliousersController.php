<?php
  
namespace App\Http\Controllers\Users;
  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Portofolio;
  
class PortofoliousersController extends Controller
{
    public function index()
    {
    	$portofolio= Portofolio::all();
        return view('users.portofolio.portofolio', compact('portofolio'));
    }
}