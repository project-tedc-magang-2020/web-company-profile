<?php
  
namespace App\Http\Controllers\Users;
  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
  
class HomeusersController extends Controller
{
    public function index()
    {
        return view('users.home.home');
    }
}