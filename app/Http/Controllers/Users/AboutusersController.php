<?php
  
namespace App\Http\Controllers\Users;
  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
  
class AboutusersController extends Controller
{
    public function index()
    {
        $konten = About::all();
        return view('users.about.about', compact('konten'));
    }
}