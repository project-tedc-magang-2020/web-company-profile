<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $kategoris = DB::table('tb_kategori')->where('type', 'LIKE', '%' .$request->search.'%');
        }else{
            $kategoris = DB::table('tb_kategori');
        }
        $kategoris = $kategoris->get();

        $q = DB::table('tb_kategori')->select(DB::raw('MAX(RIGHT(id_kategori,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('administration.kategori', compact('kategoris', 'kd'));
    }

    public function create()
    {

        return view('administration.kategori');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_kategori' => 'required',
            'nama_kategori' => 'required',
            'type' => 'required',
            'keterangan' => 'required',


        ]);

        $kategori = kategori::create([
        'id_kategori' => $request->id_kategori,
        'nama_kategori' => $request->nama_kategori,
        'type' => $request->type,
        'keterangan' => $request->keterangan,

        ]);

        if($kategori){
        //redirect dengan pesan sukses
            return redirect()->route('administration.kategori')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('administration.kategori')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_kategori)
    {
        $kategoris = kategori::where('id_kategori',$id_kategori)->get();

        return view('administration.kategori', compact('kategoris'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_kategori')->where('id_kategori',$request->id_kategori)->update([
            'nama_kategori' => $request->nama_kategori,
            'type' => $request->type,
            'keterangan' => $request->keterangan,

        ]);

        return redirect('/administration/kategori')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_kategori)
    {
        DB::table('tb_kategori')->where('id_kategori', $id_kategori)->delete();

        return redirect ('/administration/kategori')->with(['success' => 'Data Deleted Successfully!']);
    }
}
