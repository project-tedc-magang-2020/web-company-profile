<?php

namespace App\Http\Controllers\Administration;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Tags;

class TagsController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $tagss = DB::table('tb_tags')->where('nama_tags', 'LIKE', '%' .$request->search.'%');
        }else{
            $tagss = DB::table('tb_tags');
        }
        $tagss = $tagss->get();

        $q = DB::table('tb_tags')->select(DB::raw('MAX(RIGHT(id_tags,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('administration.tags', compact('tagss', 'kd'));
    }

    public function create()
    {

        return view('administration.tags');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_tags' => 'required',
            'nama_tags' => 'required',


        ]);

        $tags = tags::create([
        'id_tags' => $request->id_tags,
        'nama_tags' => $request->nama_tags,

        ]);

        if($tags){
        //redirect dengan pesan sukses
            return redirect()->route('administration.tags')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('administration.tags')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_tags)
    {
        $tagss = tags::where('id_tags',$id_tags)->get();

        return view('administration.tags', compact('tagss'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_tags')->where('id_tags',$request->id_tags)->update([
            'nama_tags' => $request->nama_tags,

        ]);

        return redirect('/administration/tags')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_tags)
    {
        DB::table('tb_tags')->where('id_tags', $id_tags)->delete();

        return redirect ('/administration/tags')->with(['success' => 'Data Deleted Successfully!']);
    }
}
