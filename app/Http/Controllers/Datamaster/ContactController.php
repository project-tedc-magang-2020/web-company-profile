<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $contacts = DB::table('tb_contact')->where('nama', 'LIKE', '%' .$request->search.'%');
        }else{
            $contacts = DB::table('tb_contact');
        }
        $contacts = $contacts->get();

        $q = DB::table('tb_contact')->select(DB::raw('MAX(RIGHT(id_contact,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('datamaster.contact', compact('contacts', 'kd'));
    }

    public function create()
    {

        return view('datamaster.contact');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_contact' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'pesan' => 'required',
            'waktu' => 'required',

        ]);

        $contact = contact::create([
        'id_contact' => $request->id_contact,
        'nama' => $request->nama,
        'email' => $request->email,
        'subject' => $request->subject,
        'pesan' => $request->pesan,
        'waktu' => $request->waktu,

        ]);

        if($contact){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.contact')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.contact')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_contact)
    {
        $contacts = contact::where('id_contact',$id_contact)->get();

        return view('datamaster.contact', compact('contacts'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('tb_contact')->where('id_contact',$request->id_contact)->update([
            'nama' => $request->nama,
            'email' => $request->email,
            'subject' => $request->subject,
            'pesan' => $request->pesan,
        ]);

        return redirect('/datamaster/contact')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_contact)
    {
        DB::table('tb_contact')->where('id_contact', $id_contact)->delete();

        return redirect ('/datamaster/contact')->with(['success' => 'Data Deleted Successfully!']);
    }
}
