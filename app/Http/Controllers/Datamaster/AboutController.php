<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\About;

class AboutController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $abouts = DB::table('tb_about')->where('nama_client', 'LIKE', '%' .$request->search.'%');
        }else{
            $abouts = DB::table('tb_about');
        }
        $abouts = $abouts->get();

        $q = DB::table('tb_about')->select(DB::raw('MAX(RIGHT(id_about,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('datamaster.about', compact('abouts','kd'));
    }

    public function create()
    {

        return view('datamaster.about');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_about' => 'required',
            'gambar' => 'required',
            'gambar_client' => 'required',
            'gambar_project' => 'required',
            'konten' => 'required',
            'nama_client' => 'required',
            'alamat_client' => 'required',
        ]);

        if ($request->hasFile('gambar')) {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        if ($request->hasFile('gambar_client')) {
            $ambil=$request->file('gambar_client');
            $name=$ambil->getClientOriginalName();
            $namaFileBaruClient = uniqid();
            $namaFileBaruClient .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaruClient);
        
        if ($request->hasFile('gambar_project')) {
            $ambil=$request->file('gambar_project');
            $name=$ambil->getClientOriginalName();
            $namaFileBaruProject = uniqid();
            $namaFileBaruProject .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaruProject);

        $about = About::create([
        'id_about' => $request->id_about,
        'gambar' => $namaFileBaru,
        'gambar_client' => $namaFileBaruClient,
        'gambar_project' => $namaFileBaruProject,
        'konten' => $request->konten,
        'nama_client' => $request->nama_client,
        'alamat_client' => $request->alamat_client,
        ]
    );
    }
}
        }

        if($about){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.about')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.about')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_about)
    {
        $abouts = About::where('id_about',$id_about)->get();

        return view('datamaster.about', compact('abouts'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);
            
        DB::table('tb_about')->where('id_about',$request->id_about)->update([
            'gambar' => $namaFileBaru,
            'gambar_client' => $request->gambar_client,
            'gambar_project' => $request->gambar_project,
            'konten' =>  $request->konten,
            'nama_client' => $request->nama_client,
            'alamat_client' => $request->alamat_client,
        ]);

        return redirect('/datamaster/about')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_about)
    {
        DB::table('tb_about')->where('id_about', $id_about)->delete();

        return redirect ('/datamaster/about')->with(['success' => 'Data Deleted Successfully!']);
    }
}
