<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Knowledge;

class KnowledgeController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $knowledges = DB::table('tb_knowledge')->where('tanggal_berita', 'LIKE', '%' .$request->search.'%');
        }else{
            $knowledges = DB::table('tb_knowledge');
        }
        $knowledges = $knowledges->get();

        $q = DB::table('tb_knowledge')->select(DB::raw('MAX(RIGHT(id_knowledge,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('datamaster.knowledge', compact('knowledges', 'kd'));
    }

    public function create()
    {

        return view('datamaster.knowledge');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_knowledge' => 'required',
            'gambar' => 'required',
            'tanggal_berita' => 'required',
            'judul_berita' => 'required',
            'isi_konten' => 'required',
            'deskripsi' => 'required',


        ]);

        if ($request->hasFile('gambar')) {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        $knowledge = Knowledge::create([
        'id_knowledge' => $request->id_knowledge,
        'gambar' => $namaFileBaru,
        'tanggal_berita' => $request->tanggal_berita,
        'judul_berita' => $request->judul_berita,
        'isi_konten' => $request->isi_konten,
        'deskripsi' => $request->deskripsi,

        ]);
    }

        if($knowledge){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.knowledge')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.knowledge')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_knowledge)
    {
        $knowledges = Knowledge::where('id_knowledge',$id_knowledge)->get();

        return view('datamaster.knowledge', compact('knowledges'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        DB::table('tb_knowledge')->where('id_knowledge',$request->id_knowledge)->update([
            'gambar' => $namaFileBaru,
            'tanggal_berita' => $request->tanggal_berita,
            'judul_berita' => $request->judul_berita,
            'isi_konten' => $request->isi_konten,
            'deskripsi' => $request->deskripsi,

        ]);

        return redirect('/datamaster/knowledge')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_knowledge)
    {
        DB::table('tb_knowledge')->where('id_knowledge', $id_knowledge)->delete();

        return redirect ('/datamaster/knowledge')->with(['success' => 'Data Deleted Successfully!']);
    }
}
