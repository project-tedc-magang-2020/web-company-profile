<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Career;

class CareerController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $careers = DB::table('tb_career')->where('deskripsi', 'LIKE', '%' .$request->search.'%');
        }else{
            $careers = DB::table('tb_career');
        }
        $careers = $careers->get();

        

        return view('datamaster.career', compact('careers'));
    }

    public function create()
    {

        return view('datamaster.career');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'judul' => 'required',
            'gambar' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($request->hasFile('gambar')) {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);
        
        $career = Career::create([
        'judul' => $request->judul,
        'gambar' => $namaFileBaru,
        'deskripsi' => $request->deskripsi,
        ]);
    }

        if($career){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.career')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.career')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_career)
    {
        $careers = Career::where('id_career',$id_career)->get();

        return view('datamaster.career', compact('careers'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        DB::table('tb_career')->where('id_career',$request->id_career)->update([
            'judul' => $request->judul,
            'gambar' => $namaFileBaru,
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect('/datamaster/career')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_career)
    {
        DB::table('tb_career')->where('id_career', $id_career)->delete();

        return redirect ('/datamaster/career')->with(['success' => 'Data Deleted Successfully!']);
    }
}
