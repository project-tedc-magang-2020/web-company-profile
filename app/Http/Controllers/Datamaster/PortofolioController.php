<?php

namespace App\Http\Controllers\Datamaster;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Portofolio;

class PortofolioController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('search')){
            $portofolios = DB::table('tb_portofolio')->where('id_client', 'LIKE', '%' .$request->search.'%');
        }else{
            $portofolios = DB::table('tb_portofolio');
        }
        $portofolios = $portofolios->get();

        $q = DB::table('tb_portofolio')->select(DB::raw('MAX(RIGHT(id_portofolio,5)) as kode'));
        $kd="";
        if($q->count()>0)
        {
            foreach($q->get() as $k)
            {
                $tmp = ((int)$k->kode)+1;
                $kd = sprintf("%05s",$tmp);
            }
        }
        else {
            $kd = "00001";
        }

        return view('datamaster.portofolio', compact('portofolios', 'kd'));
    }

    public function create()
    {

        return view('datamaster.portofolio');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'id_portofolio' => 'required',
            'id_client' => 'required',
            'gambar' => 'required',
            'nama_client' => 'required',
            'judul_portofolio' => 'required',
            'desk_portofolio' => 'required',

        ]);

        if ($request->hasFile('gambar')) {
            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        $portofolio = Portofolio::create([
        'id_portofolio' => $request->id_portofolio,
        'id_client' => $request->id_client,
        'gambar' => $namaFileBaru,
        'nama_client' => $request->nama_client,
        'judul_portofolio' => $request->judul_portofolio,
        'desk_portofolio' => $request->desk_portofolio,

        ]);
    }

        if($portofolio){
        //redirect dengan pesan sukses
            return redirect()->route('datamaster.portofolio')->with(['success' => 'Data Saved Successfully!']);
        }else{
        //redirect dengan pesan error
            return redirect()->route('datamaster.portofolio')->with(['error' => 'Data Save Failed!']);
        }

    }

    public function edit($id_portofolio)
    {
        $portofolios = Portofolio::where('id_portofolio',$id_portofolio)->get();

        return view('datamaster.portofolio', compact('portofolio'));
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

            $ambil=$request->file('gambar');
            $name=$ambil->getClientOriginalName();
            $namaFileBaru = uniqid();
            $namaFileBaru .= $name;
            $ambil->move(\base_path()."/public/images", $namaFileBaru);

        DB::table('tb_portofolio')->where('id_portofolio',$request->id_portofolio)->update([
        'id_client' => $request->id_client,
        'gambar' => $namaFileBaru,
        'nama_client' => $request->nama_client,
        'judul_portofolio' => $request->judul_portofolio,
        'desk_portofolio' => $request->desk_portofolio,

        ]);

        return redirect('/datamaster/portofolio')->with(['success' => 'Data Updated Successfully!']);
    }

    public function destroy($id_portofolio)
    {
        DB::table('tb_portofolio')->where('id_portofolio', $id_portofolio)->delete();

        return redirect ('/datamaster/portofolio')->with(['success' => 'Data Deleted Successfully!']);
    }
}
