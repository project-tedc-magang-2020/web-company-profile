<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
    public $table = "tb_backup";
    protected $primaryKey = 'id_backup';
    public $timestamps = false;
   

    protected $fillable = [
        'id_backup', 'user', 'activty', 'waktu'
    ];
}

