<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public $table = "tb_kategori";
    protected $primaryKey = 'idkategori';
    public $timestamps = false;
   

    protected $fillable = [
        'id_kategori', 'nama_kategori', 'type', 'keterangan'
    ];
}

