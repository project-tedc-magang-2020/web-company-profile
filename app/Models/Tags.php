<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    public $table = "tb_tags";
    protected $primaryKey = 'id_tags';
    public $timestamps = false;
   

    protected $fillable = [
        'id_tags', 'nama_tags'
    ];
}

