<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogoUser extends Model
{
    public $table = "tb_logo_user";
    protected $primaryKey = 'id_logo_user';
    public $timestamps = false;
   

    protected $fillable = [
        'id_logo_user', 'logo'
    ];
}

