<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public $table = "tb_menu";
    protected $primaryKey = 'id';
    public $timestamps = false;
   

    protected $fillable = [
        'id', 'nama_menu', 'view','create', 'edit', 'delete', 'export', 'duplicate', 'unlock'
    ];
}

