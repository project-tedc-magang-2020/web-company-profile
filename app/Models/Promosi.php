<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promosi extends Model
{
    public $table = "tb_promosi";
    protected $primaryKey = 'id_promosi';
    public $timestamps = false;
   

    protected $fillable = [
        'id_promosi', 'nama_promosi', 'tanggal', 'keterangan'
    ];
}

