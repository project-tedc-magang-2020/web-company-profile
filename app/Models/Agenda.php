<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    public $table = "tb_agenda";
    protected $primaryKey = 'id_agenda';
    public $timestamps = false;
   

    protected $fillable = [
        'id_agenda', 'nama_agenda', 'tempat', 'waktu'
    ];
}

