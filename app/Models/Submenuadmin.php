<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submenuadmin extends Model
{
    public $table = "tb_sub_menu_admin";
    protected $primaryKey = 'id';
    public $timestamps = false;
   

    protected $fillable = [
        'id', 'id_admin', 'nama_sub_menu', 'link', 'keterangan', 'icon', 'status'
    ];
}

