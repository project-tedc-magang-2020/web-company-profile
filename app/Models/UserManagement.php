<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserManagement extends Model
{
    public $table = "tb_user_management";
    protected $primaryKey = 'id_user_management';
    public $timestamps = false;
   

    protected $fillable = [
        'id_user_management', 'username', 'nama', 'role', 'email', 'update_password', 'status'
    ];
}

