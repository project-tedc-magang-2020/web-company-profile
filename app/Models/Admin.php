<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public $table = "tb_admin";
    protected $primaryKey = 'id_admin';
    public $timestamps = false;
   

    protected $fillable = [
        'id_admin', 'nama_menu', 'link', 'keterangan', 'icon', 'status', 'sub_menu'
    ];
}

