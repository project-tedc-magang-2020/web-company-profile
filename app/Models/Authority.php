<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    public $table = "tb_authority";
    protected $primaryKey = 'id_authority';
    public $timestamps = false;
   

    protected $fillable = [
        'id_authority', 'nama_role', 'type'
    ];
}

