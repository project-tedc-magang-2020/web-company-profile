<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogAdmin extends Model
{
    public $table = "tb_log_admin";
    protected $primaryKey = 'id_log_admin';
    public $timestamps = false;
   

    protected $fillable = [
        'id_log_admin', 'user', 'waktu', 'keterangan'
    ];
}

