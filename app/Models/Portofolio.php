<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portofolio extends Model
{
    public $table = "tb_portofolio";
    protected $primaryKey = 'id_portofolio';
    public $timestamps = false;
   

    protected $fillable = [
        'id_portofolio', 'id_client', 'gambar', 'nama_client', 'judul_portofolio', 'desk_portofolio'
    ];
}

