<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    public $table = "tb_career";
    protected $primaryKey = 'id_career';
    public $timestamps = false;
   

    protected $fillable = [
        'id_career', 'judul', 'gambar', 'deskripsi'
    ];
}

