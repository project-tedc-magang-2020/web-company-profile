<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogoAdmin extends Model
{
    public $table = "tb_logo_admin";
    protected $primaryKey = 'id_logo_admin';
    public $timestamps = false;
   

    protected $fillable = [
        'id_logo_admin', 'logo'
    ];
}

