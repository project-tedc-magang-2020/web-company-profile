<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    public $table = "tb_newsletter";
    protected $primaryKey = 'id_newsletter';
    public $timestamps = false;
   

    protected $fillable = [
        'id_newsletter', 'nama', 'email', 'pesan'
    ];
}

