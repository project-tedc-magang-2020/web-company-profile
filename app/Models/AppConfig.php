<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppConfig extends Model
{
    public $table = "tb_app_config";
    protected $primaryKey = 'id_app_config';
    public $timestamps = false;
   

    protected $fillable = [
        'id_app_config', 'nama_website', 'inisial', 'deskripsi', 'keywoard', 'alamat', 'no_telp', 'email', 'facebook', 'instagram', 'twitter', 'google_plus', 'youtube', 'maps', 'favicon', 'logo_user', 'logo_admin', 'background'
    ];
}

