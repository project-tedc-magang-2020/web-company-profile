<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogPublic extends Model
{
    public $table = "tb_log_public";
    protected $primaryKey = 'id_log_public';
    public $timestamps = false;
   

    protected $fillable = [
        'id_log_public', 'ip_address', 'browser', 'os', 'waktu'
    ];
}

