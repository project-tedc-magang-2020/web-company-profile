<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DownloadFile extends Model
{
    public $table = "tb_download_file";
    protected $primaryKey = 'id_download';
    public $timestamps = false;
   

    protected $fillable = [
        'id_download', 'nama_file'
    ];
}

