<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    public $table = "tb_contact";
    protected $primaryKey = 'id_contact';
    public $timestamps = false;
   

    protected $fillable = [
        'id_contact', 'nama', 'email','subject', 'pesan', 'waktu'
    ];
}

