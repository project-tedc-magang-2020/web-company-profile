<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $table = "tb_role";
    protected $primaryKey = 'id_role';
    public $timestamps = false;
   

    protected $fillable = [
        'id_role', 'role_name', 'keterangan'
    ];
}

