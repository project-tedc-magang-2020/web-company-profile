<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModulePublic extends Model
{
    public $table = "tb_public";
    protected $primaryKey = 'id_public';
    public $timestamps = false;
   

    protected $fillable = [
        'id_public', 'nama_menu', 'link', 'keterangan', 'icon', 'status', 'urutan'
    ];
}

