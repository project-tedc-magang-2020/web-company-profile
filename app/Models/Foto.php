<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    public $table = "tb_foto";
    protected $primaryKey = 'id_foto';
    public $timestamps = false;
   

    protected $fillable = [
        'id_foto','nama_kegiatan', 'gambar', 'tanggal'
    ];
}

