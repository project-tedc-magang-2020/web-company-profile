<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public $table = "tb_video";
    protected $primaryKey = 'id_video';
    public $timestamps = false;
   

    protected $fillable = [
        'id_video','nama_kegiatan', 'video', 'tanggal'
    ];
}

