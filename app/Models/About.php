<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    public $table = "tb_about";
    protected $primaryKey = 'id_about';
    public $timestamps = false;
   

    protected $fillable = [
        'id_about', 'gambar', 'gambar_client', 'gambar_project', 'konten', 'nama_client','alamat_client'
    ];
}

