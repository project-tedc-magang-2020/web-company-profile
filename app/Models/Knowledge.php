<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Knowledge extends Model
{
    public $table = "tb_knowledge";
    protected $primaryKey = 'id_knowledge';
    public $timestamps = false;
   

    protected $fillable = [
        'id_knowledge', 'gambar', 'tanggal_berita', 'judul_berita', 'isi_konten', 'deskripsi'
    ];
}

