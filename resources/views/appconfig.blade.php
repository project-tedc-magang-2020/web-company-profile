@extends('layouts.home.app')
@section('content')
<div class="container">
<center><h2>App Configuration</h2></center>
<br />
@include('layouts.messages')
<form action="{{ route('storeappconfig') }}" method="POST" id="editform">
				@csrf
				<div class="form-group">
					<label>Nama Website</label>
					<input type="text" name="nama_website" value="PT. Crop Inspirasi Digital" class="form-control">
				</div>
				<div class="form-group">
					<label>Inisial Nama Website</label>
					<input type="text" name="inisial" value="CROP, PT Crop Inspirasi Digital, IT Consultan, Software Development, Website, Mobile Apps" class="form-control">
				</div>
				<div class="form-group">
					<label>Meta Deskripsi</label>
					<textarea name="deskripsi" class="form-control">PT CROP adalah suatu perusahaan yang bergerak dibidang Teknologi Informasi yang memberikan layanan dibidang IT Consultant,Software Development dan Cloud Service. Didukung oleh staff-staff yang menguasai konsep Teknologi Informasi untuk melakukan Transformasi Digital dan mengembangkan serta menerapkan teknologi di banyak organisasi.</textarea>
				</div>
				 <div class="form-group">
                	<label>Meta Keyword</label>
                	<input type="text" name="keywoard" value="Crop, IT, Solution, Kreatif, Website, Consultant, Sistem Informasi, Sistem, Aplikasi" class="form-control">
            	</div>
				<div class="form-group">
					<label>Alamat</label>
					<input type="text" name="alamat" value="Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, Kota Bandung, Jawa Barat 40152" class="form-control">
				</div>
				<div class="form-group">
					<label>No Telepon</label>
					<input type="text" name="no_telp" class="form-control">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="text" name="email" value="info@crop.co.id <br> crop.indonesia@gmail.com" class="form-control">
				</div>
				<div class="form-group">
					<label>Facebook</label>
					<input type="text" name="facebook" value="www.facebook.com" class="form-control">
				</div>
				<div class="form-group">
					<label>Instagram</label>
					<input type="text" name="instagram" value="https://www.instagram.com/" class="form-control">
				</div>
				<div class="form-group">
					<label>Twitter</label>
					<input type="text" name="twitter" value="https://twitter.com/" class="form-control">
				</div>
				<div class="form-group">
					<label>Google Plus</label>
					<input type="text" name="google_plus" value="https://plus.google.com/" class="form-control">
				</div>
				<div class="form-group">
					<label>Youtube</label>
					<input type="text" name="youtube" value="https://www.youtube.com/" class="form-control">
				</div>
                <div class="form-group">
					<label>Maps</label>
					<input type="text" name="maps" value="-6.874059, 107.576974" class="form-control">
				</div>

				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="table-dark">
						<tr>
							<th style="vertical-align: middle; text-align: center;" width="25%"><font color="white">Favicon</font></th>
							<th style="vertical-align: middle; text-align: center;" width="25%"><font color="white">Logo User</font></th>
                            <th style="vertical-align: middle; text-align: center;" width="25%"><font color="white">Logo Admin</font></th>
                            <th style="vertical-align: middle; text-align: center;" width="25%"><font color="white">Background</font></th>
						</tr>
						</thead>

						<tbody>
						
						<tr>
						@foreach ($appconfigs as $pe)
						<td style="vertical-align: middle; text-align: center;">
                                <img src="{{ url('images/'.$pe->favicon) }}" class="img-thumbnail" width="200" height="200">
                            </td>
							<td style="vertical-align: middle; text-align: center;">
                                <img src="{{ url('images/'.$pe->logo_user) }}" class="img-thumbnail" width="200" height="200">
                            </td>
							<td style="vertical-align: middle; text-align: center;">
                                <img src="{{ url('images/'.$pe->logo_admin) }}" class="img-thumbnail" width="200" height="200">
                            </td>
                            <td style="vertical-align: middle; text-align: center;">
                                <img src="{{ url('images/'.$pe->background) }}" class="img-thumbnail" width="200" height="200">
                            </td>
							@endforeach
						</tr>

						<tr>
						<td style="vertical-align: middle; text-align: center;">
						<input type="file" name="favicon" class="form-control">
                            </td>
							<td style="vertical-align: middle; text-align: center;">
							<input type="file" name="logo_user" class="form-control">
							</td>
							<td style="vertical-align: middle; text-align: center;">
							<input type="file" name="logo_admin" class="form-control">
							</td>
                            <td style="vertical-align: middle; text-align: center;">
							<input type="file" name="background" class="form-control">
							</td>
						</tr>
						</tbody>
					</table>
					</div>

				<br />
				<center><button type="submit" class="btn btn-primary">Simpan</button></center>
			</form>
  

            </div>
<br />
<br />

@endsection


