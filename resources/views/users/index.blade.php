@extends('layouts.users.app')
@section('content')

<body>

<!-- page wrapper start -->

<div class="page-wrapper">


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="http://www.crop.co.id/"> 
          <img id="logo-img" class="img-center" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" width="200px" alt=""></a>
          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <div>&nbsp; &nbsp; &nbsp;</div>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left nav -->
              <ul class = "navbar-nav mr-auto">
      
                    <li class="nav-item"><a class="nav-link active" href="/home"> Home</a></li>
                    <li class="nav-item"><a class="nav-link " href="/about"> About</a></li>
                    <li class="nav-item"><a class="nav-link " href="/service"> Service</a></li>
                    <li class="nav-item"><a class="nav-link " href="/knowledge"> Knowledge</a></li>
                    <li class="nav-item"><a class="nav-link " href="/career">Career</a></li>
                    <li class="nav-item"><a class="nav-link " href="/portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link " href="/contact">Contact Us</a></li>
            </div>

           <div class="right-nav align-items-center d-flex justify-content-end list-inline">
              <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav" class="navbar navbar-expand-lg"> <a href="#" class="ht-nav-toggle active"><span></span></a>
<div class="row">
  <div class="col-sm">&nbsp;</div>
  <div clas="col-sm">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" alt="">
        <p class="mb-5"><b>PT. Crop Inspirasi Digital</b> <br> Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p>
        <div class="form-info">
          <h4>Contact info</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span>ALAMAT:</span>
              <p>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Telepon:</span><a href="tel:"></a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>

<!--header end-->

<section id="top" class="fullscreen-banner banner p-0 o-hidden box-shadow" data-bg-img="https://crop.co.id/assetuser/images/pattern/01.png">
    <div class="insideText">CROP, PT Crop Inspirasi Digital, IT Consultan, Software Development, Website, Mobile Apps</div>
    <div class="align-center">
    <div class="container">
      <div class="row align-items-center">
       
      <div class="col-lg-5 col-md-12 order-lg-12">
      <h1 class="mb-4 wow bounceInLeft" data-wow-duration="1s" data-wow-delay="1s">Solving Technology Problem <span class="font-w-3">Solutions</span></h1>
      <p class="lead wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s"> PT CROP adalah suatu perusahaan yang bergerak dibidang Teknologi Informasi yang memberikan layanan dibidang IT Consultant,Software Development dan Cloud Service. Didukung oleh staff - staff yang menguasai konsep Teknologi Informasi untuk melakukan Transformasi Digital dan mengembangkan serta menerapkan teknologi di banyak organisasi.</p>
    </div>

    <div class="col-lg-7 col-md-12 order-lg-1 md-mt-5">
      <div class="mouse-parallax">
        <div class="bnr-img1 wow fadeInRight" data-wow-duration="1s" data-wow-delay="4s">
        <img class="img-center rotateme" src="https://crop.co.id/assetuser/images/banner/01.png" alt="">
        </div>
        <img class="img-center bnr-img2 wow zoomIn" data-wow-duration="1s" data-wow-delay="1s" src="images/home2.png" width="500" height="500" alt="">
      </div>
      </div>
    </div>
  </div>
  </div>
  </section>
  <br>

  <!--Segmentasi start-->
  <section class="grey-bg pos-r text-center" id="crop">
    <div class="pattern-3">
      <img class="img-fluid rotateme" src="http://www.crop.co.id/assetuser/images/pattern/03.png" alt="">
    </div>
    <div class="container">
      <div class="row">
      <div class="col-lg-8 col-md-12 ml-auto mr-auto">
        <div class="section-title">
        <h2 style="margin-top: 50px">Segmentasi Perusahaan</h2>
        <p class="mb-0">PT. Crop Inspirasi Digital memfokuskan layanan ke beberapa bidang yaitu:</p> 
        </div>
      </div>
      </div>
      <div class="row">
           <div class="col-lg-4 col-md-6 mt-3">
        <div class="featured-item text-center">
          <div class="featured-icon">
          <img class="img-center" src="images/embassy.png">
          </div>
          <div class="featured-title">
          <h5>Pemerintah, BUMN & BUMD</h5>
          </div>
        </div>
        </div>
          <div class="col-lg-4 col-md-6 mt-3">
        <div class="featured-item text-center">
          <div class="featured-icon">
          <img class="img-center" src="images/guideline.png" alt="">
          </div>
          <div class="featured-title">
          <h5>Swasta, Industri Perbankan & Reassure</h5>
          </div>
        </div>
        </div>
          <div class="col-lg-4 col-md-6 mt-3">
        <div class="featured-item text-center">
          <div class="featured-icon">
          <img class="img-center" src="images/freelance.png" alt="">
          </div>
          <div class="featured-title">
          <h5>Pasar Global/Pasar Bebas</h5>
          </div>
        </div>
        </div>
        </div>
    </div>
    </section>
<!--segmentasi end-->
  
<!--about start-->
<section id="about" class="pos-r" >
  <div class="container">
  <div class="row">
    
   <div class="col-lg-5 image-column bg-contain bg-pos-l">
    <div class="section-title mb-4">
    <!-- <h6>About Us</h6> -->
    <h2 style="margin-top: 50px; text-align: center"> About Us</h2> 
    </div>
    @foreach($konten as $k)  
    <p>{{$k->konten}}</p>
    <div class="white-bg box-shadow radius px-4 py-4 mt-5">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6">
          <div class="counter style-2">
          </div>
        </div>
      <div class="col-lg-3 col-md-3 col-sm-6">
          <div class="counter style-2">
            <img class="img-center" src="{{url('images/'.$k->gambar_project)}}" alt=""> <span class="count-number" data-to="12" data-speed="200">12</span>
            <h5>Project Done</h5>
          </div>
        </div>
      <div class="col-lg-3 col-md-3 col-sm-6 sm-mt-5">
          <div class="counter style-2">
            <img class="img-center" src="{{url('images/'.$k->gambar_client)}}" alt=""> <span class="count-number" data-to="16" data-speed="200">16</span>
            <h5>Client</h5>
          </div>
        </div>
    </div>
    </div> 
  </div>
  
  
  <div  class="col-lg-7 col-md-12 ml-auto md-mt-5" data-bg-img="https://crop.co.id/assetuser/images/pattern/07.png">
    <div class="round-animation">
     <img class="img-fluid" src="{{url('images/'.$k->gambar)}}" alt="">
     </div>
     @endforeach
   </div>
   </div>
  </div>
  </section>
<!--about end-->

<!--visi misi start-->
<br><br><br><br><br>
<section class="grey-bg animatedBackground" data-bg-img="images/pattern/05.png">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <h2>Visi & Misi Perusahaan</h2>
    </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="tab style-2 ">
        <!-- Nav tabs -->
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist"> 
            <a class="nav-item nav-link active col-lg-6" id="nav-tab1" data-toggle="tab" href="#tab1-1" role="tab" aria-selected="true">Visi</a>
            <a class="nav-item nav-link col-lg-6" id="nav-tab2" data-toggle="tab" href="#tab1-2" role="tab" aria-selected="false">Misi</a>
          </div>
        </nav>
        <!-- Tab panes -->
        <div class="tab-content" id="nav-tabContent">
          <div role="tabpanel" class="tab-pane fade show active" id="tab1-1">
            <div class="row align-items-center">
              <div class="col-lg-6 col-md-12">
                <img class="img-fluid" src="images/about/visi.png" alt="">
              </div>
              <div class="col-lg-6 col-md-12 md-mt-5">
                <h4 class="title">Visi Perusahaan</h4>
                <p>Menjadi
                perusahaan berbasis pengetahuan yang profesional, unggul, handal dan
                kompetitif dalam memberikan solusi terbaik dan terpercaya dalam bidang
                teknologi informasi dan transformasi digital</p>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="tab1-2">
            <div class="row align-items-center">
              <div class="col-lg-6 col-md-12">
                <img class="img-fluid" src="images/about/misi1.png" alt="">
              </div>
              <div class="col-lg-6 col-md-12 md-mt-5">
                <h4 class="title">Misi Perusahaan</h4>
                <ul class="list-unstyled list-icon">
                  <li class="mb-3"><i class="flaticon-tick"></i> Mengembangkan
produk dan manajemen perusahaan berbasis
pengetahuan yang berkualitas dan bernilai tinggil yang tepat guna</li>
                  <li class="mb-3"><i class="flaticon-tick"></i> Menyediakan
layanan konsultansi transformasi digital yang handal dan
professional</li>
                  <li><i class="flaticon-tick"></i> Meningkatkan
kinerja melalui efisiensi dan profesionalisme dalam rangka
menjaga komitmen kami sebagai perusahaan yang sehat dan dapat
diandalkan secara berkesinambungan</li>
                  <li><i class="flaticon-tick"></i>Membangun
dan mengembangkan kemitraan yang saling
menguntungkan</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<!--visi misi end-->

<!--client start-->
<section  class="pt-0"  data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
    <div class="container text-center">
    <div class="row">
      <div class="col-lg-8 col-md-12 ml-auto mr-auto">
        <div class="section-title">
          <h2 style="margin-top: 50px"> Our Client</h2>
          <!-- <p class="mb-0">PT. Crop Inspirasi Digital telah mempunyai client untuk o.</hp> -->
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="ht-clients d-flex flex-wrap align-items-center text-center">
                    <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552632186-Logo-Bank-BTN.jpg" alt="" width="140px" height="70px" title="PT Bank Tabungan Negara (Persero) Tbk">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552748781-dapen.png" alt="" width="140px" height="70px" title="Dapen Telkom">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1553067037-Bank-Jateng.png" alt="" width="140px" height="70px" title="PT Bank Pembangunan Daerah Jawa Tengah">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552748945-indonesiare.jpg" alt="" width="140px" height="70px" title="PT Reasuransi Indonesia Utama (Persero)">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552748903-Bank Nagari.jpg" alt="" width="140px" height="70px" title="PT Bank Pembangunan Daerah Sumatera Barat">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552826274-1465660736-Logosmkn4padalarang.jpg" alt="" width="140px" height="70px" title="SMKN 4 Padalarang">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552826360-1453986030-logo poltekpos.png" alt="" width="140px" height="70px" title="Politeknik Pos Indonesia">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552826395-1479300576-Logo-Pemerintah-Kota-Bandung-transparent.png" alt="" width="140px" height="70px" title="Dinas Kecamatan Bandung Wetan">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1553065277-bdv.png" alt="" width="140px" height="70px" title="Bandung Digital Valley">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1553065362-bekasi.jpg" alt="" width="140px" height="70px" title="BAPPEDA Kab. Bekasi">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1553065445-jabar.png" alt="" width="140px" height="70px" title="Dinas Tenaga Kerja dan Transmigrasi Provinsi Jawa Barat">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1583822707-DISHUB.png" alt="" width="140px" height="70px" title="Dinas Perhubungan Kabupaten Bandung Barat">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1605719913-benings.jpg" alt="" width="140px" height="70px" title="Benings Clinic">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1618952951-akti.png" alt="" width="140px" height="70px" title="Akademi Komunitas Toyota Indonesia">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1630167484-benings_indonesia_black.png" alt="" width="140px" height="70px" title="Benings Indonesia">
              </div>
                      <div class="clients-logo">
                <img class="img-center" src="http://www.crop.co.id/uploads/1595480113-kidzadventura.png" alt="" width="140px" height="70px" title="Kidz Adeventura">
              </div>
                  </div>
        </div>
      </div>
    </div>
    </section>
<!--client end-->
</div>

<div class="page-content">
<!--service start-->

<section class="grey-bg pos-r text-center" id="service">
  <div class="pattern-3">
    <img class="img-fluid rotateme" src="http://www.crop.co.id/assetuser/images/pattern/03.png" alt="">
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-12 ml-auto mr-auto">
        <div class="section-title">
          <h2 style="margin-top: 50px">Service</h2>
          <p class="mb-0">PT. Crop Inspirasi Digital adalah perusahaan yang bergerak di bidang <b>Teknologi Informasi dan Komunikasi (TIK)</b>.
         Untuk saat ini kami telah membuka layanan diantaranya:</p> 
        </div>
      </div>
    </div>
    <div class="row">
           <div class="col-lg-4 col-md-6 mt-3">
          <div class="featured-item text-center">
            <div class="featured-icon">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552998153-09.png" alt="">
            </div>
            <div class="featured-title">
              <h5>IT MANAGEMENT CONSULTANT</h5>
            </div>
            <div class="featured-desc">
              <p><p>Memberi konsultasi atau nasihat kepada perusahaan-perusahaan mengenai cara terbaik untuk mengelola dan mengoperasikan bisnis di bidang IT.
                  <br>Layanan kami berupa :
              </p></p>
              <ul class="list-unstyled list-icon-2" style="text-align: left;">
                  <li><a href="#itau" class="nav-toggle4">IT Audit</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="itau" style="display:none">
                          <p>Pemeriksaan dan evaluasi infrastruktur teknologi informasi organisasi, aplikasi, penggunaan dan manajemen data, kebijakan, prosedur, dan proses operasional terhadap standar yang diakui atau kebijakan yang ditetapkan.</p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <li><a href="#itg" class="nav-toggle4">IT Governance, Risk & Compliance</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="itg" style="display:none">
                          <p>Sebuah konsep yang komprehensif dalam pengintegrasian penerapan Manajemen Risiko, Tatakelola Organisasi yang Baik, dan Kesesuaian/Kepatuhan.</p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <li><a href="#itp" class="nav-toggle4">IT Procurement & PMO Services</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="itp" style="display:none">
                          <p>Pengadaan dan analisa kebutuhan/pengeluaran perusahaan (spend analysis) Menganalisa, mencari & menentukan barang/jasa yang memenuhi kebutuhan dari perusahaan (procurement & management) Mengetahui struktur biata barang, struktur biaya jasa, produksi dan biaya-biaya yang menyertai suatu pengadaan.</p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <li><a href="#its" class="nav-toggle4">IT Strategic Plan</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="its" style="display:none">
                          <p>Rencana induk jangka panjang yang bersifat strategis dan sangat krusial bagi sebuah perusahaan atau institusi yang mendambakan dukungan TI dalam pencapaian tujuan strategis perusahaanya.</p>
                        </div>
                      </div>
                    </div>
                  </li>
              </ul>
            </div>
          </div>
        </div>
            <div class="col-lg-4 col-md-6 mt-3">
          <div class="featured-item text-center">
            <div class="featured-icon">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552640868-06.png" alt="">
            </div>
            <div class="featured-title">
              <h5>SOFTWARE DEVELOPMENT</h5>
            </div>
            <div class="featured-desc">
              <p><p>Proyek IT yang berfokus pada penciptaan atau pengembangan perangkat lunak.
                  <br>Layanan kami berupa :
              </p></p>
              <ul class="list-unstyled list-icon-2" style="text-align: left;">
                <li><a href="#wd" class="nav-toggle4">Web Design</a>
                  <div class="post-desc">
                    <div class="post-desc">
                      <div class="description" id="wd" style="display:none">
                        <p>Pembuatan desain yang berfokus pada tampilan website tanpa menghilangkan fungsi utama dari website tersebut dan memberikan kenyamanan tambahan pada pembaca atau pengunjung website.</p>
                      </div>
                    </div>
                  </div>
                </li>

                <ul class="list-unstyled list-icon-2" style="text-align: left;">
                  <li><a href="#wde" class="nav-toggle4">Web Development</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="wde" style="display:none">
                          <p>Pengembangan sebuah situs web untuk world wide web atau internet</p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <ul class="list-unstyled list-icon-2" style="text-align: left;">
                    <li><a href="#ma" class="nav-toggle4">Mobile Application</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="ma" style="display:none">
                            <p>Proses pengembangan aplikasi untuk perangkat genggam seperti PDA, asisten digital perusahaan atau telepon genggam.</p>
                          </div>
                        </div>
                      </div>
                    </li>
                </ul>
            </div>
          </div>
        </div>
            <div class="col-lg-4 col-md-6 mt-3">
          <div class="featured-item text-center">
            <div class="featured-icon">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552998144-08.png" alt="">
            </div>
            <div class="featured-title">
              <h5>IT MANAGED SERVICE</h5>
            </div>
            <div class="featured-desc">
              <p><p>Penyedia layanan atau jasa untuk mengelola sistem atau infrastruktur yang di dalamnya mencakup mengelola sistem keamanan pada server, melakukan proses backup dan pengelolaan database, memantau kesehatan server, dan lainnya.
                  <br>Layanan kami berupa :
              </p></p>
              <ul class="list-unstyled list-icon-2" style="text-align: left;">
                <li><a href="#cc" class="nav-toggle4">Cloud Computing</a>
                  <div class="post-desc">
                    <div class="post-desc">
                      <div class="description" id="cc" style="display:none">
                        <p>Proses pengolahan daya komputasi  melalui jaringan internet  yang memiliki fungsi agar dapat menjalankan program melalui komputer yang telah terkoneksi satu sama lain pada waktu yang sama / Data Storage.</p>
                      </div>
                    </div>
                  </div>
                </li>

                <ul class="list-unstyled list-icon-2" style="text-align: left;">
                  <li><a href="#ims" class="nav-toggle4">Infrastructur Managed Service</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="ims" style="display:none">
                          <p>Mengelola teknologi, informasi, dan data dengan cara yang proaktif. </p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <ul class="list-unstyled list-icon-2" style="text-align: left;">
                  <li><a href="#ams" class="nav-toggle4">Application Managed Service</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="ams" style="display:none">
                          <p>Mengacu pada layanan manajemen aplikasi perusahaan yang disediakan oleh berbagai organisasi kepada perusahaan yang perlu melakukan outsourcing proses manajemen aplikasi perusahaan mereka.</p>
                        </div>
                      </div>
                    </div>
              </ul>
            </div>
          </div>
        </div>
        </div>
  </div>
  </section>
  
<!--service end-->

<div class="page-content">
<!--knowledge start-->

<section class="grey-bg pos-r text-center" id="knowledge">
<div class="pattern-3"></div>
<div class="page-title-pattern"><img class="img-fluid" src="images/bg/06.png" alt=""></div>
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-12">
            <h2 style="margin-top: 50px">Knowledge</h2>
        </div>
      </div>
    </div>
  </section>
  
  
  <section>
    <div class="container">
      <div class="row">
        @foreach($berita as $be)
        <div class="col-lg-6 col-md-6">
          <div class="post">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="{{url('images/'.$be->gambar)}}"  alt="">
            </div>
            <div class="post-desc">
              <div class="post-date"><span>{{$be->tanggal_berita}}</span>
              </div>
              <div class="post-title">
                <h5><a href="{{route('knowledge.users.show', $be->id)}}">{{$be->judul_berita}}</a></h5>
              </div>
              <p>{{$be->deskripsi}}<br></p>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  
<!--knowledge end-->

<!-- career start-->
<section class="page-title o-hidden text-center grey-bg bg-contain animatedBackground" data-bg-img="images/pattern/05.png">
  <div class="col-md-12">
    <h2 style="margin-top: 50px">Career</h2>
    <br><br><br><br><br>
</div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div id="accordion" class="accordion style-1">
          @foreach($desk as $d)
          <div class="card active">
            <div class="card-header">
              <h1 class="mb-0">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">{{$d->judul}}</a>
              </h1>
            </div>
            <div id="collapse1" class="collapse show" data-parent="#accordion">
              <div class="card-body">
                <p class="mb-0">
                  <div class="row">
            <div class="col-md-4"><img class="img-fluid w-100" src="" alt=""></div>
            <div class="col-md-4"><img class="img-fluid w-100" src="{{url('images/'.$d->gambar)}}" alt=""></div>
            <div class="col-md-4"><img class="img-fluid w-100" src="" alt=""></div>
            </div>
           <center><b>{{$d->deskripsi}}</b></center>
              </div>
            </div>
          </div>
          @endforeach
          @foreach($career as $car)
            <div class="card">
              <div class="card-header">
                <h6 class="mb-0">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$car->id_career}}">{{$car->judul}}</a>
                </h6>
              </div>
              <div id="collapse{{$car->id_career}}" class="collapse" data-parent="#accordion">
                <div class="card-body">
                <div class="row">
              <div class="col-md-4"><img class="img-fluid w-100" src="" alt=""></div>
              <div class="col-md-4"><img class="img-fluid w-100" src="{{url('images/'.$car->gambar)}}" alt=""></div>
              <div class="col-md-4"><img class="img-fluid w-100" src="" alt=""></div>
              </div>
              <center><b>{{$car->deskripsi}}</b></center>
              </div>
              </div>
            </div>
            @endforeach
  </section>
</section>
<!-- End Career Menu -->


<!--porrtofolio start-->
<br><br><br>
<h2 style="margin-top: 50px; text-align: center;">Portofolio</h2>
<br><br><br>
<section class="pt-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="post style-2">
          <div class="post-image">
            <img class="img-fluid h-100 w-100" src="images/porto/porto1.png" alt="">
          </div>
          <div class="post-desc">
            <div class="post-desc">
              <div class="post-date">Client: <span>PT. Telkom</span>
              </div>
              <div class="post-title">
                <h5><a href="/portofolio">Aplikasi Dashboard Integrasi Dana Pensiun Telkom </a></h5>
              </div>
              
              <div class="description" id="portofolio" style="display:none">
                <p>Aplikasi dashboard ini merupakan aplikasi sistem informasi yang menyajikan informasi mengenai                        indikator utama dari aktifitas organisasi secara sekilas dalam layar tunggal. 
                  Pembuatan model memperhatikan 3 (tiga) aspek utama dashboard yaitu penyajian data/informasi, 
                personalisasi, dan kolaborasi antar pengguna.</p>
              </div>
              <a href="#portofolio" class="nav-toggle">Read More</a>
            </div>
          </div>
        </div>

      </div>
      <div class="col-lg-4">
        <div class="post style-2">
          <div class="post-image">
            <img class="img-fluid h-100 w-100" src="images/porto/porto2.png" alt="">
          </div>
          <div class="post-desc">
            <div class="post-desc">
              <div class="post-date">Client: <span>PT. Telkom</span>
              </div>
              <div class="post-title">
                <h5><a href="/portofolio">Aplikasi Dashboard SK PMP Dana Pensiun Telkom </a></h5>
              </div>
              <div class="description" id="portofolio2" style="display:none">
                <p>Aplikasi dashboard SK PMP Dana Pensiun ini membantu untuk admin Dana Pensiun Telkom untuk
                  menambah data, hapus daya dan edit data. serta aplikasi ini bisa untuk menyajikan data yang ada dalam 
                aplikasi tersebut </p>
              </div>
              <a href="#portofolio2" class="nav-toggle">Read More</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="post style-2">
          <div class="post-image">
            <img class="img-fluid h-100 w-100" src="images/porto/porto3.png" alt="">
          </div>
          <div class="post-desc">
            <div class="post-desc">
              <div class="post-date">Client: <span>PT. Telkom</span>
              </div>
              <div class="post-title">
                <h5><a href="/portofolio">Aplikasi Performance Review and Budget Committee Dana Pensiun Telkom</a></h5>
              </div>
              <div class="description" id="portofolio3" style="display:none">
                <p>Aplikasi ini dibuat untuk bisa memantau kinerja dan anggaran komite dana pensiun Telkom</p>
              </div>
              <a href="#portofolio3" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto4.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>Komunitas Toyota Indonesia</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Web Profile Akademi Komunitas Toyota Indonesia
                  </a></h5>
                </div>
                <div class="description" id="portofolio4" style="display:none">
                  <p>Web Profile ini dibuat untuk memperkenalkan apa saja yang di punya oleh Komunitas Toyota Indonesia</p>
                </div>
                <a href="#portofolio4" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto5.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Telkom</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Managed Service Aplikasi Simpul Dana Pensiun Telkom</a></h5>
                </div>
                <div class="description" id="portofolio5" style="display:none">
                  <p>Aplikasi dashboard ini merupakan aplikasi sistem informasi yang menyajikan informasi mengenai                        indikator utama dari aktifitas organisasi secara sekilas dalam layar tunggal. 
                    Pembuatan model memperhatikan 3 (tiga) aspek utama dashboard yaitu penyajian data/informasi, 
                  personalisasi, dan kolaborasi antar pengguna.</p>
                </div>
                <a href="#portofolio5" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto6.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Benings Indonesia</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Sistem Distributor Skincare Benings Indonesia</a></h5>
                </div>
                <div class="description" id="portofolio6" style="display:none">
                  <p>Aplikasi sistem distributor ini tujuannya agar bisa memantau seberapa jauh persebasaran skincare dari PT. Bening Indonesia</p>
                </div>
                <a href="#portofolio6" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto7.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Benings Indonesia</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Web Company Profile Benings Indonesia</a></h5>
                </div>
                <div class="description" id="portofolio7" style="display:none">
                  <p>Web Company Profile dari PT Benings ini menampilkan profile dari perusahaan tersebut dari segi media partner,
                    produk, klinik yang mereka punya di tampilkan disana
                  </p>
                </div>
                <a href="#portofolio7" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto8.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Benings Indonesia</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Sistem Informasi Operasional Benings Indonesia </a></h5>
                </div>
                <div class="description" id="portofolio8" style="display:none">
                  <p>Sistem Informasi manajemen pengendalian operasional ini adalah sebuah sistem informasi
                    yang memudahkan untuk manajemen pengendalian operasional yang bertujuan mempunyai kegiatan yang cepat,
                    akurat serta up-to-date
                  </p>
                </div>
                <a href="#portofolio8" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto9.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Telkom</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Sistem Informasi E-auction Dana Pensiun Telkom </a></h5>
                </div>
                <div class="description" id="portofolio9" style="display:none">
                  <p>sistem ini dibuat dengan tujuan untuk bisa melakukan pelelangan</p>
                </div>
                <a href="#portofolio9" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto10.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Telkom</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Sistem Informasi E-procurement Dana Pensiun Telkom </a></h5>
                </div>
                <div class="description" id="portofolio10" style="display:none">
                  <p>Aplikasi ini dibuat dengan tujuan untuk menyediakan informasi pengadaan dana pensiun pada PT. Telkom</p>
                </div>
                <a href="#portofolio10" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto11.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Telkom</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Aplikasi Integrasi SK Dana Pensiun Telkom </a></h5>
                </div>
                <div class="description" id="portofolio11" style="display:none">
                  <p>Aplikasi integrasi SK ini disedaiakan untuk bisa menghubungkan rangkaian proses sistem komputerisasi dan software aplikasi, baik secara fisik maupun fungsional</p>
                </div>
                <a href="#portofolio11" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="post style-2">
            <div class="post-image">
              <img class="img-fluid h-100 w-100" src="images/porto/porto12.png" alt="">
            </div>
            <div class="post-desc">
              <div class="post-desc">
                <div class="post-date">Client: <span>PT. Telkom</span>
                </div>
                <div class="post-title">
                  <h5><a href="/portofolio">Aplikasi Manajemen Organisasi dan Pengembangan Karyawan Dana Pensiun Telkom </a></h5>
                </div>
                <div class="description" id="portofolio12" style="display:none">
                  <p>Aplikasi manajemen organisasi dan pengembangan karyawan ini ditujukan untuk mengatur pengembangan karyawan dan organisasi yang di capai.</p>
                </div>
                <a href="#portofolio12" class="nav-toggle">Read More</a>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </section>
<!--portofolio end-->

<!--body content start-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj2GrDSBy6ISeGg3aWUM4mn3izlA1wgt0&language=in&region=ID">
</script>
<section id="contactus" class="contact-1 pt-0" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/02.png">
<div class="container">
<div class="row row-eq-height">
  <div class="col-lg-6 col-md-12 order-lg-12">
	<div class="contact-main py-5">
	  <div class="section-title">
		<h2 style="margin-top: 60px">Contact</h2>
	  </div>
	  <!-- <form id="contact-form" class="row" method="post" action="http://www.crop.co.id/website/contact"> -->
		  <form action="http://www.crop.co.id/website/contact" class="row"  method="POST">
		<div class="messages"></div>
		<div class="form-group col-md-6">
		  <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="Name is required.">
		  <div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-6">
		  <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
		  <div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-12">
		  <input id="form_subject" type="tel" name="subject" class="form-control" placeholder="Subject" required="required" data-error="Subject is required">
		  <div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-12">
		  <textarea id="form_message" name="message" class="form-control" placeholder="Message" rows="4" required="required" data-error="Please,leave us a message."></textarea>
		  <div class="help-block with-errors"></div>
		</div>
		<div class="col-md-12">
		  <!-- <button class="btn btn-theme btn-radius"><span>Send Message</span> -->
			<input type="submit" class="btn btn-theme btn-radius" name="send" value="Send Message">
		
		</div>
	  </form>
	</div>
  </div>
  <div class="col-lg-6 col-md-12 order-lg-1">
	<!-- <div id="tempat_peta"></div> -->
	<!-- <div id="map" ></div>  -->
    <iframe id="map" class="map-canvas md-iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15844.24911434328!2d107.577529!3d-6.8831433!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x57ca8e0ceca4a19!2sPT%20Crop%20Inspirasi%20Digital!5e0!3m2!1sid!2sid!4v1617695867538!5m2!1sid!2sid" width="600"  style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	<!-- <div class="map-canvas md-iframe" data-zoom="22" data-lat="-6.848546" data-lng="107.492616" data-type="roadmap" data-hue="#ffc400" data-title="CROP, PT Crop Inspirasi Digital, IT Consultan, Software Development, Website, Mobile Apps" data-icon-path="http://www.crop.co.id/assetuser/images/marker.png" data-content="Bandung, Indonesia"></div>  -->
  </div>
</div> 
</div>
</section>

<!--body content end--> 
<script type="text/javascript">
function initialize() {
	var locations = [
	  ['<h5>PT. Crop Inspirasi Digital</h5>',-6.874059, 107.576974], 
		  
	];
	var infowindow = new google.maps.InfoWindow();

	var options = {
	  zoom: 16, 
	  center: new google.maps.LatLng(-6.874059, 107.576974 ),
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById('map'), options);

var marker, i;
for (i = 0; i < locations.length; i++) {  
  marker = new google.maps.Marker({
	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	map: map,
	 
  }); 
  google.maps.event.addListener(marker, 'click', (function(marker, i) {
	return function() {
	  infowindow.setContent(locations[i][0]);
	  infowindow.open(map, marker);
	}
  })(marker, i));
}

};
google.maps.event.addDomListener(window, 'load', initialize);
</script>


<!--footer start-->

<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" data-bg-img="http://www.crop.co.id/assetuser/images/bg/02.png">
            <div class="footer-logo">
              <img src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" class="img-center" alt="">
            </div>
            <p class="mb-3">Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p> 
            <!-- <a class="btn-simple" href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> -->
            <div class="social-icons social-border circle social-hover mt-5">
              <h4 class="title">Follow Us</h4>
              <ul class="list-inline">
                <li class="social-facebook"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="http://https://twitter.com/"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="http://https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8 py-8 md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              <h4 class="title">Tautan</h4>
              <div class="row">
                <div class="col-sm-5">
                  <ul class="list-unstyled">                    
                    <li><a class="nav-link " href="index.html"> Home</a></li><li><a class="nav-link " href="about.html"> About</a></li><li><a class="nav-link " href="service.html"> Service</a></li><li><a class="nav-link " href="knowledge.html"> Knowledge</a></li><li><a class="nav-link " href="career.html">Career</a></li><li><a class="nav-link " href="portofolio.html">Portofolio</a><li><a class="nav-link " href="contact.html">Contact Us</a></li></ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 sm-mt-5">
              <h4 class="title">Contact us</h4>
              <ul class="media-icon list-unstyled">
                <li><p class="mb-0">Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p></li>
                <li><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a></li>
                <li><a href="tel:"></a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>Copyright 2022 | All Rights Reserved</span>
          </div>
          <div class="col-md-6 text-md-right sm-mt-2"> <span>PT. Crop Inspirasi Digital</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

  
<!-- inject js start -->

@endsection