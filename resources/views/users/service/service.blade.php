@extends('layouts.users.app')
@section('content')

<!-- page wrapper start -->

<div class="page-wrapper">


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="http://www.crop.co.id/"> 
          <img id="logo-img" class="img-center" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" width="200px" alt=""></a>
          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <div>&nbsp; &nbsp; &nbsp;</div>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left nav -->
              <ul class = "navbar-nav mr-auto">
      
                    <li class="nav-item"><a class="nav-link " href="/home"> Home</a></li>
                    <li class="nav-item"><a class="nav-link " href="/about"> About</a></li>
                    <li class="nav-item"><a class="nav-link active" href="/service"> Service</a></li>
                    <li class="nav-item"><a class="nav-link " href="/knowledge"> Knowledge</a></li>
                    <li class="nav-item"><a class="nav-link " href="/career">Career</a></li>
                    <li class="nav-item"><a class="nav-link " href="/portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link " href="/contact">Contact Us</a></li>
            </div>

           <div class="right-nav align-items-center d-flex justify-content-end list-inline">
              <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav" class="navbar navbar-expand-lg"> <a href="#" class="ht-nav-toggle active"><span></span></a>
<div class="row">
  <div class="col-sm">&nbsp;</div>
  <div clas="col-sm">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" alt="">
        <p class="mb-5"><b>PT. Crop Inspirasi Digital</b> <br> Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p>
        <div class="form-info">
          <h4>Contact info</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span>ALAMAT:</span>
              <p>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Telepon:</span><a href="tel:"></a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>

<!--header end-->
<!--service start-->

<section class="grey-bg pos-r text-center" id="service">
    <div class="pattern-3">
      <img class="img-fluid rotateme" src="http://www.crop.co.id/assetuser/images/pattern/03.png" alt="">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-12 ml-auto mr-auto">
          <div class="section-title">
            <h2 style="margin-top: 50px">Service</h2>
            <p class="mb-0">PT. Crop Inspirasi Digital adalah perusahaan yang bergerak di bidang <b>Teknologi Informasi dan Komunikasi (TIK)</b>.
           Untuk saat ini kami telah membuka layanan diantaranya:</p> 
          </div>
        </div>
      </div>
      <div class="row">
             <div class="col-lg-4 col-md-6 mt-3">
            <div class="featured-item text-center">
              <div class="featured-icon">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552998153-09.png" alt="">
              </div>
              <div class="featured-title">
                <h5>IT MANAGEMENT CONSULTANT</h5>
              </div>
              <div class="featured-desc">
                <p><p>Memberi konsultasi atau nasihat kepada perusahaan-perusahaan mengenai cara terbaik untuk mengelola dan mengoperasikan bisnis di bidang IT.
                    <br>Layanan kami berupa :
                </p></p>
                <ul class="list-unstyled list-icon-2" style="text-align: left;">
                    <li><a href="#itau" class="nav-toggle">IT Audit</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="itau" style="display:none">
                            <p>Pemeriksaan dan evaluasi infrastruktur teknologi informasi organisasi, aplikasi, penggunaan dan manajemen data, kebijakan, prosedur, dan proses operasional terhadap standar yang diakui atau kebijakan yang ditetapkan.</p>
                          </div>
                        </div>
                      </div>
                    </li>

                    <li><a href="#itg" class="nav-toggle">IT Governance, Risk & Compliance</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="itg" style="display:none">
                            <p>Sebuah konsep yang komprehensif dalam pengintegrasian penerapan Manajemen Risiko, Tatakelola Organisasi yang Baik, dan Kesesuaian/Kepatuhan.</p>
                          </div>
                        </div>
                      </div>
                    </li>

                    <li><a href="#itp" class="nav-toggle">IT Procurement & PMO Services</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="itp" style="display:none">
                            <p>Pengadaan dan analisa kebutuhan/pengeluaran perusahaan (spend analysis) Menganalisa, mencari & menentukan barang/jasa yang memenuhi kebutuhan dari perusahaan (procurement & management) Mengetahui struktur biata barang, struktur biaya jasa, produksi dan biaya-biaya yang menyertai suatu pengadaan.</p>
                          </div>
                        </div>
                      </div>
                    </li>

                    <li><a href="#its" class="nav-toggle">IT Strategic Plan</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="its" style="display:none">
                            <p>Rencana induk jangka panjang yang bersifat strategis dan sangat krusial bagi sebuah perusahaan atau institusi yang mendambakan dukungan TI dalam pencapaian tujuan strategis perusahaanya.</p>
                          </div>
                        </div>
                      </div>
                    </li>
                </ul>
              </div>
            </div>
          </div>
              <div class="col-lg-4 col-md-6 mt-3">
            <div class="featured-item text-center">
              <div class="featured-icon">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552640868-06.png" alt="">
              </div>
              <div class="featured-title">
                <h5>SOFTWARE DEVELOPMENT</h5>
              </div>
              <div class="featured-desc">
                <p><p>Proyek IT yang berfokus pada penciptaan atau pengembangan perangkat lunak.
                    <br>Layanan kami berupa :
                </p></p>
                <ul class="list-unstyled list-icon-2" style="text-align: left;">
                  <li><a href="#wd" class="nav-toggle">Web Design</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="wd" style="display:none">
                          <p>Pembuatan desain yang berfokus pada tampilan website tanpa menghilangkan fungsi utama dari website tersebut dan memberikan kenyamanan tambahan pada pembaca atau pengunjung website.</p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <ul class="list-unstyled list-icon-2" style="text-align: left;">
                    <li><a href="#wde" class="nav-toggle">Web Development</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="wde" style="display:none">
                            <p>Pengembangan sebuah situs web untuk world wide web atau internet</p>
                          </div>
                        </div>
                      </div>
                    </li>

                    <ul class="list-unstyled list-icon-2" style="text-align: left;">
                      <li><a href="#ma" class="nav-toggle">Mobile Application</a>
                        <div class="post-desc">
                          <div class="post-desc">
                            <div class="description" id="ma" style="display:none">
                              <p>Proses pengembangan aplikasi untuk perangkat genggam seperti PDA, asisten digital perusahaan atau telepon genggam.</p>
                            </div>
                          </div>
                        </div>
                      </li>
                  </ul>
              </div>
            </div>
          </div>
              <div class="col-lg-4 col-md-6 mt-3">
            <div class="featured-item text-center">
              <div class="featured-icon">
                <img class="img-center" src="http://www.crop.co.id/uploads/1552998144-08.png" alt="">
              </div>
              <div class="featured-title">
                <h5>IT MANAGED SERVICE</h5>
              </div>
              <div class="featured-desc">
                <p><p>Penyedia layanan atau jasa untuk mengelola sistem atau infrastruktur yang di dalamnya mencakup mengelola sistem keamanan pada server, melakukan proses backup dan pengelolaan database, memantau kesehatan server, dan lainnya.
                    <br>Layanan kami berupa :
                </p></p>
                <ul class="list-unstyled list-icon-2" style="text-align: left;">
                  <li><a href="#cc" class="nav-toggle">Cloud Computing</a>
                    <div class="post-desc">
                      <div class="post-desc">
                        <div class="description" id="cc" style="display:none">
                          <p>Proses pengolahan daya komputasi  melalui jaringan internet  yang memiliki fungsi agar dapat menjalankan program melalui komputer yang telah terkoneksi satu sama lain pada waktu yang sama / Data Storage.</p>
                        </div>
                      </div>
                    </div>
                  </li>

                  <ul class="list-unstyled list-icon-2" style="text-align: left;">
                    <li><a href="#ims" class="nav-toggle">Infrastructur Managed Service</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="ims" style="display:none">
                            <p>Mengelola teknologi, informasi, dan data dengan cara yang proaktif. </p>
                          </div>
                        </div>
                      </div>
                    </li>

                    <ul class="list-unstyled list-icon-2" style="text-align: left;">
                    <li><a href="#ams" class="nav-toggle">Application Managed Service</a>
                      <div class="post-desc">
                        <div class="post-desc">
                          <div class="description" id="ams" style="display:none">
                            <p>Mengacu pada layanan manajemen aplikasi perusahaan yang disediakan oleh berbagai organisasi kepada perusahaan yang perlu melakukan outsourcing proses manajemen aplikasi perusahaan mereka.</p>
                          </div>
                        </div>
                      </div>
                </ul>
              </div>
            </div>
          </div>
          </div>
    </div>
    </section>
<!--service end-->

<!--footer start-->

<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" data-bg-img="http://www.crop.co.id/assetuser/images/bg/02.png">
            <div class="footer-logo">
              <img src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" class="img-center" alt="">
            </div>
            <p class="mb-3">Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p> 
            <!-- <a class="btn-simple" href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> -->
            <div class="social-icons social-border circle social-hover mt-5">
              <h4 class="title">Follow Us</h4>
              <ul class="list-inline">
                <li class="social-facebook"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="http://https://twitter.com/"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="http://https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8 py-8 md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              <h4 class="title">Tautan</h4>
              <div class="row">
                <div class="col-sm-5">
                  <ul class="list-unstyled">                    
                    <li><a class="nav-link " href="index.html"> Home</a></li><li><a class="nav-link " href="about.html"> About</a></li><li><a class="nav-link " href="service.html"> Service</a></li><li><a class="nav-link " href="knowledge.html"> Knowledge</a></li><li><a class="nav-link " href="career.html">Career</a></li><li><a class="nav-link " href="portofolio.html">Portofolio</a><li><a class="nav-link " href="contact.html">Contact Us</a></li></ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 sm-mt-5">
              <h4 class="title">Contact us</h4>
              <ul class="media-icon list-unstyled">
                <li><p class="mb-0">Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p></li>
                <li><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a></li>
                <li><a href="tel:"></a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>Copyright 2022 | All Rights Reserved</span>
          </div>
          <div class="col-md-6 text-md-right sm-mt-2"> <span>PT. Crop Inspirasi Digital</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

  
@endsection