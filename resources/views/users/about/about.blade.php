  `@extends('layouts.users.app')
@section('content')

<!-- page wrapper start -->

<div class="page-wrapper">


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="http://www.crop.co.id/"> 
          <img id="logo-img" class="img-center" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" width="200px" alt=""></a>
          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <div>&nbsp; &nbsp; &nbsp;</div>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left nav -->
              <ul class = "navbar-nav mr-auto">
      
                    <li class="nav-item"><a class="nav-link " href="/home"> Home</a></li>
                    <li class="nav-item"><a class="nav-link active" href="/about"> About</a></li>
                    <li class="nav-item"><a class="nav-link " href="/service"> Service</a></li>
                    <li class="nav-item"><a class="nav-link " href="/knowledge"> Knowledge</a></li>
                    <li class="nav-item"><a class="nav-link " href="/career">Career</a></li>
                    <li class="nav-item"><a class="nav-link " href="/portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link " href="/contact">Contact Us</a></li>
            </div>

           <div class="right-nav align-items-center d-flex justify-content-end list-inline" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav" class="navbar navbar-expand-lg"> <a href="#" class="ht-nav-toggle active"><span></span></a>
<div class="row">
  <div class="col-sm">&nbsp;</div>
  <div clas="col-sm">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" alt="">
        <p class="mb-5"><b>PT. Crop Inspirasi Digital</b> <br> Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p>
        <div class="form-info">
          <h4>Contact info</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span>ALAMAT:</span>
              <p>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Telepon:</span><a href="tel:"></a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>

<!--header end-->
  
<!--about start-->
<section id="about" class="pos-r" >
    <div class="container">
    <div class="row">
      
     <div class="col-lg-5 image-column bg-contain bg-pos-l">
      <div class="section-title mb-4">
      <!-- <h6>About Us</h6> -->
      <h2 style="margin-top: 50px; text-align: center"> About Us</h2> 
      </div>
      @foreach($konten as $k)
      <p>{{$k->konten}}</p>

      <div class="white-bg box-shadow radius px-4 py-4 mt-5">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="counter style-2">
            </div>
          </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="counter style-2">
              <img class="img-center" src="{{url('images/'.$k->gambar_project)}}" alt=""> <span class="count-number" data-to="12" data-speed="200">12</span>
              <h5>Project Done</h5>
            </div>
          </div>
        <div class="col-lg-3 col-md-3 col-sm-6 sm-mt-5">
            <div class="counter style-2">
              <img class="img-center" src="{{url('images/'.$k->gambar_client)}}" alt=""> <span class="count-number" data-to="16" data-speed="200">16</span>
              <h5>Client</h5>
            </div>
          </div>
      </div>
      </div> 
    </div>
    
    
    <div  class="col-lg-7 col-md-12 ml-auto md-mt-5" data-bg-img="https://crop.co.id/assetuser/images/pattern/07.png">
      <div class="round-animation">
       <img class="img-fluid" src="{{url('images/'.$k->gambar)}}" alt="">
       </div>
       @endforeach
     </div>
     </div>
    </div>
    </section>
<!--about end-->

<!--visi misi start-->
<br><br><br><br><br>
<section class="grey-bg animatedBackground" data-bg-img="{{URL::asset('users/images/pattern/05.png')}}">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2>Visi & Misi Perusahaan</h2>
      </div>
    </div>
  </div>
  <br><br>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="tab style-2 ">
          <!-- Nav tabs -->
          <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist"> 
              <a class="nav-item nav-link active col-lg-6" id="nav-tab1" data-toggle="tab" href="#tab1-1" role="tab" aria-selected="true">Visi</a>
              <a class="nav-item nav-link col-lg-6" id="nav-tab2" data-toggle="tab" href="#tab1-2" role="tab" aria-selected="false">Misi</a>
            </div>
          </nav>
          <!-- Tab panes -->
          <div class="tab-content" id="nav-tabContent">
            <div role="tabpanel" class="tab-pane fade show active" id="tab1-1">
              <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                  <img class="img-fluid" src="{{URL::asset('users/images/about/visi.png')}}" alt="">
                </div>
                <div class="col-lg-6 col-md-12 md-mt-5">
                  <h4 class="title">Visi Perusahaan</h4>
                  <p>Menjadi
                  perusahaan berbasis pengetahuan yang profesional, unggul, handal dan
                  kompetitif dalam memberikan solusi terbaik dan terpercaya dalam bidang
                  teknologi informasi dan transformasi digital</p>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="tab1-2">
              <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                  <img class="img-fluid" src="{{URL::asset('users/images/about/misi1.png')}}" alt="">
                </div>
                <div class="col-lg-6 col-md-12 md-mt-5">
                  <h4 class="title">Misi Perusahaan</h4>
                  <ul class="list-unstyled list-icon">
                    <li class="mb-3"><i class="flaticon-tick"></i> Mengembangkan
produk dan manajemen perusahaan berbasis
pengetahuan yang berkualitas dan bernilai tinggil yang tepat guna</li>
                    <li class="mb-3"><i class="flaticon-tick"></i> Menyediakan
layanan konsultansi transformasi digital yang handal dan
professional</li>
                    <li><i class="flaticon-tick"></i> Meningkatkan
kinerja melalui efisiensi dan profesionalisme dalam rangka
menjaga komitmen kami sebagai perusahaan yang sehat dan dapat
diandalkan secara berkesinambungan</li>
                    <li><i class="flaticon-tick"></i>Membangun
dan mengembangkan kemitraan yang saling
menguntungkan</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--visi misi end-->

<!--client start-->
<section  class="pt-0"  data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="container text-center">
  <div class="row">
    <div class="col-lg-8 col-md-12 ml-auto mr-auto">
      <div class="section-title">
        <h2 style="margin-top: 50px"> Our Client</h2>
        <!-- <p class="mb-0">PT. Crop Inspirasi Digital telah mempunyai client untuk o.</hp> -->
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="ht-clients d-flex flex-wrap align-items-center text-center">
                  <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552632186-Logo-Bank-BTN.jpg" alt="" width="140px" height="70px" title="PT Bank Tabungan Negara (Persero) Tbk">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552748781-dapen.png" alt="" width="140px" height="70px" title="Dapen Telkom">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1553067037-Bank-Jateng.png" alt="" width="140px" height="70px" title="PT Bank Pembangunan Daerah Jawa Tengah">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552748945-indonesiare.jpg" alt="" width="140px" height="70px" title="PT Reasuransi Indonesia Utama (Persero)">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552748903-Bank Nagari.jpg" alt="" width="140px" height="70px" title="PT Bank Pembangunan Daerah Sumatera Barat">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552826274-1465660736-Logosmkn4padalarang.jpg" alt="" width="140px" height="70px" title="SMKN 4 Padalarang">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552826360-1453986030-logo poltekpos.png" alt="" width="140px" height="70px" title="Politeknik Pos Indonesia">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1552826395-1479300576-Logo-Pemerintah-Kota-Bandung-transparent.png" alt="" width="140px" height="70px" title="Dinas Kecamatan Bandung Wetan">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1553065277-bdv.png" alt="" width="140px" height="70px" title="Bandung Digital Valley">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1553065362-bekasi.jpg" alt="" width="140px" height="70px" title="BAPPEDA Kab. Bekasi">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1553065445-jabar.png" alt="" width="140px" height="70px" title="Dinas Tenaga Kerja dan Transmigrasi Provinsi Jawa Barat">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1583822707-DISHUB.png" alt="" width="140px" height="70px" title="Dinas Perhubungan Kabupaten Bandung Barat">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1605719913-benings.jpg" alt="" width="140px" height="70px" title="Benings Clinic">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1618952951-akti.png" alt="" width="140px" height="70px" title="Akademi Komunitas Toyota Indonesia">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1630167484-benings_indonesia_black.png" alt="" width="140px" height="70px" title="Benings Indonesia">
            </div>
                    <div class="clients-logo">
              <img class="img-center" src="http://www.crop.co.id/uploads/1595480113-kidzadventura.png" alt="" width="140px" height="70px" title="Kidz Adeventura">
            </div>
                </div>
      </div>
    </div>
  </div>
  </section>
<!--client end-->

<!--footer start-->

<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" data-bg-img="http://www.crop.co.id/assetuser/images/bg/02.png">
            <div class="footer-logo">
              <img src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" class="img-center" alt="">
            </div>
            <p class="mb-3">Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p> 
            <!-- <a class="btn-simple" href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> -->
            <div class="social-icons social-border circle social-hover mt-5">
              <h4 class="title">Follow Us</h4>
              <ul class="list-inline">
                <li class="social-facebook"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="http://https://twitter.com/"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="http://https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8 py-8 md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              <h4 class="title">Tautan</h4>
              <div class="row">
                <div class="col-sm-5">
                  <ul class="list-unstyled">                    
                    <li><a class="nav-link " href="index.html"> Home</a></li><li><a class="nav-link " href="about.html"> About</a></li><li><a class="nav-link " href="service.html"> Service</a></li><li><a class="nav-link " href="knowledge.html"> Knowledge</a></li><li><a class="nav-link " href="career.html">Career</a></li><li><a class="nav-link " href="portofolio.html">Portofolio</a><li><a class="nav-link " href="contact.html">Contact Us</a></li></ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 sm-mt-5">
              <h4 class="title">Contact us</h4>
              <ul class="media-icon list-unstyled">
                <li><p class="mb-0">Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p></li>
                <li><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a></li>
                <li><a href="tel:"></a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>Copyright 2022 | All Rights Reserved</span>
          </div>
          <div class="col-md-6 text-md-right sm-mt-2"> <span>PT. Crop Inspirasi Digital</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

  
@endsection