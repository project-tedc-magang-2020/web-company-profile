@extends('layouts.users.app')
@section('content')

<!-- page wrapper start -->

<div class="page-wrapper">


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="http://www.crop.co.id/"> 
          <img id="logo-img" class="img-center" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" width="200px" alt=""></a>
          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <div>&nbsp; &nbsp; &nbsp;</div>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left nav -->
              <ul class = "navbar-nav mr-auto">
      
                    <li class="nav-item"><a class="nav-link " href="/home"> Home</a></li>
                    <li class="nav-item"><a class="nav-link " href="/about"> About</a></li>
                    <li class="nav-item"><a class="nav-link " href="/service"> Service</a></li>
                    <li class="nav-item"><a class="nav-link active" href="/knowledge"> Knowledge</a></li>
                    <li class="nav-item"><a class="nav-link " href="/career">Career</a></li>
                    <li class="nav-item"><a class="nav-link " href="/portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link " href="/contact">Contact Us</a></li>
            </div>

           <div class="right-nav align-items-center d-flex justify-content-end list-inline">
              <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav" class="navbar navbar-expand-lg"> <a href="#" class="ht-nav-toggle active"><span></span></a>
<div class="row">
  <div class="col-sm">&nbsp;</div>
  <div clas="col-sm">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" alt="">
        <p class="mb-5"><b>PT. Crop Inspirasi Digital</b> <br> Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p>
        <div class="form-info">
          <h4>Contact info</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span>ALAMAT:</span>
              <p>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Telepon:</span><a href="tel:"></a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>

<!--header end-->            


<!--blog start-->
<br><br>
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="left-side">
          <div class="post">
            <center>
              <div class="post-image">
                <img class="img-fluid" src="images/berita/berita2.jpeg" alt="">
              </div>
            </center>
            
            <div class="post-desc">
              <div class="post-date mb-2">7 <span>Juli 2022</span>
              </div>
              <div class="post-title">
                <h2>4 Skill Pekerjaan yang Harus Dimiliki pada Era Metaverse, Kamu Sudah Punya?</h2>
              </div>
              <p class="lead">Jakarta - Konsep metaverse adalah dunia virtual yang menjadi inovasi untuk masa depan. Metaverse diciptakan oleh manusia untuk berinteraksi dan melakukan berbagai aktivitas.
                Hal penting dalam menghadapi era metaverse dapat dilihat dari tiga elemen yaitu ketersediaan hardware, software atau infrastruktur yang telah terbangun, dan sumber daya manusia yang berperan di dalamnya.<br><br>
                
                Bagi kamu yang ingin terjun ke dunia metaverse, setidaknya ada beberapa skill yang harus dimiliki oleh sumber daya manusianya. Berikut ini rangkumannya dikutip dari laman resmi Binus University. <br><br>
                </p>
              <p class="mb-0">4 Skill Pekerjaan yang Harus Dimiliki pada Era Metaverse: <br><br>

                1. Memahami Peran
                Dr. Indrawan Nugroho, CEO dan Co-Founder CIAS (Corporate Innovation Asia), mengatakan bahwa peran yang diambil harus jelas sebelum terjun ke dunia metaverse. <br><br>
                
                Misal apakah ingin membangun device, mempelajari model 3D, dan lainnya. <br>
                
                "Kalau Anda ingin mengambil peran sebagai orang yang membangun device-nya, cari program studi yang memungkinkan Anda berkontribusi di situ. Jika Anda ingin membangun dunia metaverse, mulai dari blockchain, NFT, cryptocurrency, atau world building, Anda harus mempelajari 3D modelling," ungkapnya dikutip dari laman Binus, Kamis (7/7/2022). <br><br>
                
                Dr. Indrawan Nugroho mengatakan, dengan mengetahui peran yang diambil, seseorang akan lebih mudah untuk mempersiapkan diri memasuki dunia metaverse. <br><br>
                
                Persiapan tersebut dapat dilakukan dengan memasuki program studi yang relevan. Nantinya, pengetahuan yang diperoleh dari bangku kuliah akan menjadi bekal yang sangat penting dalam membangun metaverse.<br><br><br>
                
                2. Literasi Teknologi<br><br>
                Sementara itu, Chief Metaverse Officer WIR Group, Stephen Ng, MIM. MITM. MIR, mengatakan bahwa kunci dari memasuki dunia metaverse adalah technology literacy. <br><br>
                
                "Apa pun passion kita, technology literacy harus dibangun dari awal. Apabila Anda seorang sarjana hukum dan ingin masuk ke metaverse, masalah kepemilikan di metaverse sangat tricky. Seorang dokter harus tahu bagaimana cara menggabungkan offline-online di metaverse," terangnya. <br><br>
                
                Menurut Stephen, siapa pun yang ingin masuk ke dunia metaverse harus memiliki skill untuk membangun maupun memanfaatkan ilmu yang dimilikinya di dunia metaverse. <br><br>
                
                "Bagi yang membangun, mereka bisa memulainya dari jurusan computing, design, dan sebagainya. Bagi pihak yang memanfaatkan, seluruh bidang ilmu bisa terlibat asal memiliki technology literacy," tambahnya. <br><br><br>
                
                3. Adaptasi dengan Berbagai Tantangan<br><br>
                Kemampuan lain yang harus dimiliki di dunia metaverse adalah kemampuan untuk beradaptasi dengan keadaan. Hal ini karena sebagai dunia baru, metaverse menghadirkan berbagai tantangan yang harus dihadapi. <br><br>
                
                Apabila perusahaan ingin terlibat dan sukses di dalamnya, maka kemampuan beradaptasi sangat penting. Tujuannya agar perusahaan dapat tetap relevan dengan kebutuhan pelanggan. <br><br><br>
                
                4. Berani Inovasi<br><br>
                Skill terakhir yang penting untuk SDM adalah berani untuk berinovasi. Keberanian ini bukan hanya sebagai pengguna di dunia metaverse, tetapi hadir sebagai creator. <br><br>
                
                Dengan cara ini, seseorang tidak hanya berkontribusi dalam metaverse, namun bisa memperoleh penghasilan. <br><br>
                
                Jadi, itulah beberapa skill yang dibutuhkan pekerja pada era metaverse. Apakah detikers punya skill itu? <br><br>
                </p>
            </div>
          </div>
        </div>    
      </div>
      </div>
      </div>
      </section>

      <!--footer start-->

<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" data-bg-img="http://www.crop.co.id/assetuser/images/bg/02.png">
            <div class="footer-logo">
              <img src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" class="img-center" alt="">
            </div>
            <p class="mb-3">Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p> 
            <!-- <a class="btn-simple" href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> -->
            <div class="social-icons social-border circle social-hover mt-5">
              <h4 class="title">Follow Us</h4>
              <ul class="list-inline">
                <li class="social-facebook"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="http://https://twitter.com/"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="http://https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8 py-8 md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              <h4 class="title">Tautan</h4>
              <div class="row">
                <div class="col-sm-5">
                  <ul class="list-unstyled">                    
                    <li><a class="nav-link " href="index.html"> Home</a></li><li><a class="nav-link " href="about.html"> About</a></li><li><a class="nav-link " href="service.html"> Service</a></li><li><a class="nav-link " href="knowledge.html"> Knowledge</a></li><li><a class="nav-link " href="career.html">Career</a></li><li><a class="nav-link " href="portofolio.html">Portofolio</a><li><a class="nav-link " href="contact.html">Contact Us</a></li></ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 sm-mt-5">
              <h4 class="title">Contact us</h4>
              <ul class="media-icon list-unstyled">
                <li><p class="mb-0">Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p></li>
                <li><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a></li>
                <li><a href="tel:"></a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>Copyright 2022 | All Rights Reserved</span>
          </div>
          <div class="col-md-6 text-md-right sm-mt-2"> <span>PT. Crop Inspirasi Digital</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

@endsection