@extends('layouts.users.app')
@section('content')

<!-- page wrapper start -->

<div class="page-wrapper">


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="http://www.crop.co.id/"> 
          <img id="logo-img" class="img-center" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" width="200px" alt=""></a>
          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <div>&nbsp; &nbsp; &nbsp;</div>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left nav -->
              <ul class = "navbar-nav mr-auto">
      
                    <li class="nav-item"><a class="nav-link " href="/home"> Home</a></li>
                    <li class="nav-item"><a class="nav-link " href="/about"> About</a></li>
                    <li class="nav-item"><a class="nav-link " href="/service"> Service</a></li>
                    <li class="nav-item"><a class="nav-link active" href="/knowledge"> Knowledge</a></li>
                    <li class="nav-item"><a class="nav-link " href="/career">Career</a></li>
                    <li class="nav-item"><a class="nav-link " href="/portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link " href="/contact">Contact Us</a></li>
            </div>

           <div class="right-nav align-items-center d-flex justify-content-end list-inline">
              <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav" class="navbar navbar-expand-lg"> <a href="#" class="ht-nav-toggle active"><span></span></a>
<div class="row">
  <div class="col-sm">&nbsp;</div>
  <div clas="col-sm">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" alt="">
        <p class="mb-5"><b>PT. Crop Inspirasi Digital</b> <br> Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p>
        <div class="form-info">
          <h4>Contact info</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span>ALAMAT:</span>
              <p>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Telepon:</span><a href="tel:"></a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>

<!--header end-->
            <br><br>
            <section>
              <div class="container">
                <div class="row">
                  <div class="col-lg-12 col-md-12">
                    <div class="justify-content-lg-center">
                      <div class="post">
                        <center>
                        <div class="post-image">
                          <img class="img-fluid" src="images/berita/berita1.png" style="text-align: center;" alt="">
                        </div>
                      </center>
                        <div class="post-desc">
                          <div class="post-date mb-2">8 <span>Juli 2022</span>
                          </div>
                          <div class="post-title">
                            <h2>Platform Metaverse Buatan Anak Negeri Segera Dirilis, Bisa Apa Aja?</h2>
                          </div>
                          <p class="lead">Jakarta - Platform social metaverse dari Indonesia Jagat siap menghadirkan dunia virtual dan pengalaman interaksi sosial yang imersif. Perusahaan yang menaungi Jagat, PT Avatara Jagat Nusantara, menargetkan kaum muda sebagai sasaran utamanya. Perusahaan yang didirikan pada Desember 2021 ini akan segera meluncurkan versi alpha dari produk perdananya pada pertengahan tahun ini.<br>
                            Sebagai informasi, metaverse merupakan konsep dunia virtual yang bersifat tanpa batas dan saling terhubung antara satu komunitas virtual dengan lainnya. Riset dari Populix pada Juni 2022 juga mencatat sebanyak 44% masyarakat Indonesia melihat metaverse sebagai ekstensi platform sosial media yang menyediakan berbagai cara baru berkomunikasi secara bebas. <br><br>
                            
                            Co-Founder Jagat Barry Beagen menilai metaverse dan Web3 akan menjadi masa depan internet dan interaksi sosial di Indonesia. Untuk itu, Jagat sebagai perpanjangan media sosial akan menjadi wadah tempat berkreasi dan hangout favorit bagi masyarakat Indonesia. <br><br>
                            
                            "Sebagai negara dengan masyarakat yang aktif bersosial media dan memiliki rasa gotong royong yang tinggi, kami melihat metaverse dan Web3 menjadi masa depan internet dan interaksi sosial di Indonesia saat ini. Sebagai perpanjangan media sosial, Jagat fokus untuk menjadi tempat berkreasi dan hangout favorit bagi masyarakat Indonesia, khususnya kaum muda produktif dan digital savvy untuk mengerahkan potensi mereka," ujarnya dalam keterangan tertulis, Jumat (8/7/2022). <br><br>
                            
                            Ia menambahkan melalui ekosistem kreator baru Jagat, semua orang dapat berkarya, terhubung dengan teman hingga memperoleh manfaat ekonomi dari hal tersebut.
                            <br>
                            
                            "Tentu saja tidak ada yang dapat menggantikan interaksi dunia nyata, namun dengan kemampuan avatar berbasis video 3D, kami dapat meningkatkan empati antar orang selama bekerja dan berinteraksi secara remote, sangat cocok untuk diterapkan di Indonesia sebagai negara kepulauan yang secara geografis sangat luas. Di dalam ekosistem kreator baru Jagat, semua orang dapat menciptakan dunia baru, memiliki karya mereka sendiri, terhubung dengan teman, dan menghasilkan manfaat ekonomi dari hal tersebut," terang Barry. <br><br>
                            
                            Jagat akan tersedia baik di web maupun aplikasi mobile tanpa membutuhkan dukungan perangkat keras tambahan seperti Virtual Reality (VR) atau Augmented Reality (AR). Sebagai platform berbasis user-generated-content (UGC), Jagat juga memungkinkan pengguna untuk merancang dan bahkan menggunakan berbagai aplikasi dalam platform untuk berinteraksi dengan teman baru, rekan kerja, hingga fans mereka. <br><br>
                            Selain itu, baik masyarakat maupun brand nantinya dapat membuat avatar dan aset digital seperti fesyen, furnitur dan aksesoris lain dalam bentuk Non Fungible Token (NFT) secara gratis dan mudah, tanpa perlu memiliki pengetahuan teknis atau tentang cryptocurrency sekalipun. <br>
                            
                            "Untuk pertama kali kami mendorong konsep hybrid atau keterkaitan secara nyata antara metaverse dengan pembangunan fisik pada kota tinggal yang menjadi partner kami nanti. Dalam membangun platform bersama layaknya kota digital, kami mendorong nilai strategis dari metaverse ini dalam hal kepemilikan ekonomi dan sosial yang nyata," tambah Barry.
                            Melalui teknologi blockchain, tutur Barry, Jagat dapat mendorong terbentuknya komunitas yang bisa menciptakan dan memperoleh manfaat ekonomi melalui berbagai bisnis model X-to-earn dengan potensi tak terbatas. Jagat juga berkomitmen untuk menghadirkan pengalaman Web3 yang sesuai dan terjangkau baik secara teknologi maupun finansial bagi masyarakat secara umum. <br><br>
                            
                            "Ketika berbicara mengenai metaverse, pandangan masyarakat di Indonesia saat ini masih skeptis, dengan fokus kepada tantangan akses internet yang belum merata, jargon yang tidak mudah dipahami, dan akses perangkat keras yang belum terjangkau. Melihat bahwa banyaknya orang yang baru-baru ini terhubung pertama kali dengan internet dan berbagai macam layanan seperti sosial media, uang elektronik, dan gim melalui alat mobile, kami akan hadir dengan strategi mobile-first yang dapat mengakomodasi smartphone yang sederhana sekalipun," paparnya. <br>
                            
                            "Sehingga, diharapkan pendekatan ini akan membuka akses pada manfaat sosial dan ekonomi yang ditawarkan oleh metaverse, bagi seluruh lapisan masyarakat, baik di Indonesia maupun dunia," sambung Barry. <br><br><br>
                            </p>
                          <p class="mb-0">Berkolaborasi dengan Raksasa Teknologi Dunia
            
                            <br><br>
                            Jagat juga menggandeng perusahaan teknologi kelas dunia seperti Advance Intelligence Group, perusahaan kecerdasan buatan (AI) di Asia Tenggara serta platform metaverse asal Singapura, Utown. <br>
                            
                            Advance Intelligence Group terdiri dari platform buy now, pay later terbesar di Asia Atome, perusahaan AI terdepan ADVANCE.AI, dan layanan merchant e-commerce regional Ginee. Sementara Utown adalah perusahaan metaverse yang telah mengembangkan aplikasi sosial dengan lebih dari 90 juta pengguna, dan ingin mendorong lebih jauh ke dalam konsep metaverse dan menciptakan platform dengan kapabilitas dan kepraktisan yang sesungguhnya. <br>
                            
                            "Kami melihat bahwa Jagat menaruh perhatian yang mendalam agar Web3 dapat dipahami dan diakses oleh lebih banyak pihak, terutama anak-anak muda kreatif di Indonesia. Dengan kekuatan Barry dan tim dalam isu-isu kota urban dan community building, kami percaya bahwa Jagat dapat membangun ekonomi baru bagi semua pihak, dimana potensi Web3 dapat dimaksimalkan bagi seluruh pengguna dengan membuat, memiliki hingga memonetisasi berbagai tools yang disiapkan bagi kreator," tutur Co-founder, Chairman, dan Chief Executive Officer Advance Intelligence Group Jefferson Chen. <br><br>
                            
                            "Dengan pengetahuan mendalam dan ekosistem produk dan layanan kami di Indonesia dan global, Advance Intelligence Group akan mengkolaborasikan pengetahuan, pengalaman, dan sumber daya dalam aspek fintech, e-commerce, ritel, pembayaran, teknologi, dan kecerdasan buatan (AI) untuk dapat mengembangkan generasi internet berikutnya melalui Jagat di Indonesia," ujarnya lagi. <br>
                            
                            Sementara itu, Co-founder Utown Zach Loy juga mengatakan pihaknya siap memberikan pengalaman dan solusi interaktif termutakhir untuk Jagat sambil menawarkan interaksi kepada pengguna Jagat dalam membangun dan mengembangkan koneksinya. <br><br>
                            
                            "Membangun ekosistem metaverse tentu membutuhkan penerapan teknologi yang beragam, tetapi Jagat ingin melakukan lebih dari itu. Sebagai elemen yang tidak dapat terpisahkan dari kehidupan bermasyarakat, bersosialisasi merupakan bagian penting dalam dunia metaverse. Tanpa itu, metaverse akan menjadi sebuah kekosongan. Dengan pengalaman bertahun-tahun dalam pengembangan dan pengoperasian aplikasi sosial, Utown akan memberikan pengalaman dan solusi interaktif termutakhir untuk Jagat, menawarkan interaksi yang mendalam kepada pengguna Jagat sambil memungkinkan mereka untuk membangun dan mengembangkan koneksi sosialnya di Jagat," ungkapnya. <br>
                            
                            "Kami percaya bahwa hubungan sosial yang dibangun pengguna di Jagat akan menjadi perpanjangan dari hubungan yang sudah mereka miliki di kehidupan nyata dan di Internet. Menurut kami, Jagat memiliki potensi besar untuk menjadi produk ikonik di era metaverse," sambung Zach. <br>
                            
                            Dengan memanfaatkan potensi dunia virtual tanpa batas, metaverse dari Jagat dapat menjadi wajah Indonesia di komunitas global. Hal ini senada dengan pernyataan Koordinator Asistensi dan Kemitraan Presidensi G20 Indonesia Wishnutama Kusubandio. <br>
                            
                            "Sebagai masa depan dalam digital di Indonesia, metaverse lokal adalah sebuah keniscayaan bagi Indonesia, yang juga negara dengan ekonomi digital terbesar di Asia Tenggara, agar dapat bersaing dengan negara-negara lain di kawasan tersebut bahkan bersaing secara global. Hal ini selaras dengan arahan Bapak Presiden yang telah mengemukakan pentingnya Negara kita menyiapkan sebuah terobosan agar kita dapat unggul dari negara-negara lain dalam era digital ini," ungkat Wishnutama. <br>
                            
                            "Dengan seluruh mata dunia tertuju pada Indonesia pada puncak perhelatan akbar Konferensi Tingkat Tinggi G20 di Indonesia di bulan Oktober 2022 nanti, kita harus siap membawa potensi masa depan itu hadir sekarang dan melakukan akselerasi transformasi digital Indonesia dengan metaverse karya anak muda Indonesia melalui Jagat," pungkasnya.
                            </p>
                        </div>
                      </div>
                  </div>    
                </div>
                </div>
                </div>
                </section>

<!--footer start-->

<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" data-bg-img="http://www.crop.co.id/assetuser/images/bg/02.png">
            <div class="footer-logo">
              <img src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" class="img-center" alt="">
            </div>
            <p class="mb-3">Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p> 
            <!-- <a class="btn-simple" href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> -->
            <div class="social-icons social-border circle social-hover mt-5">
              <h4 class="title">Follow Us</h4>
              <ul class="list-inline">
                <li class="social-facebook"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="http://https://twitter.com/"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="http://https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8 py-8 md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              <h4 class="title">Tautan</h4>
              <div class="row">
                <div class="col-sm-5">
                  <ul class="list-unstyled">                    
                    <li><a class="nav-link " href="index.html"> Home</a></li><li><a class="nav-link " href="about.html"> About</a></li><li><a class="nav-link " href="service.html"> Service</a></li><li><a class="nav-link " href="knowledge.html"> Knowledge</a></li><li><a class="nav-link " href="career.html">Career</a></li><li><a class="nav-link " href="portofolio.html">Portofolio</a><li><a class="nav-link " href="contact.html">Contact Us</a></li></ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 sm-mt-5">
              <h4 class="title">Contact us</h4>
              <ul class="media-icon list-unstyled">
                <li><p class="mb-0">Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p></li>
                <li><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a></li>
                <li><a href="tel:"></a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>Copyright 2022 | All Rights Reserved</span>
          </div>
          <div class="col-md-6 text-md-right sm-mt-2"> <span>PT. Crop Inspirasi Digital</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

  
<!-- inject js start -->
@endsection