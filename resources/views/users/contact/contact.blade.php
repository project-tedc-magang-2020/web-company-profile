@extends('layouts.users.app')
@section('content')

<!-- page wrapper start -->

<div class="page-wrapper">


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="http://www.crop.co.id/"> 
          <img id="logo-img" class="img-center" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" width="200px" alt=""></a>
          
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span></span>
    <span></span>
    <span></span>
  </button>
  <div>&nbsp; &nbsp; &nbsp;</div>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left nav -->
              <ul class = "navbar-nav mr-auto">
      
                    <li class="nav-item"><a class="nav-link " href="/home"> Home</a></li>
                    <li class="nav-item"><a class="nav-link " href="/about"> About</a></li>
                    <li class="nav-item"><a class="nav-link " href="/service"> Service</a></li>
                    <li class="nav-item"><a class="nav-link " href="/knowledge"> Knowledge</a></li>
                    <li class="nav-item"><a class="nav-link " href="/career">Career</a></li>
                    <li class="nav-item"><a class="nav-link " href="/portofolio">Portofolio</a></li>
                    <li class="nav-item"><a class="nav-link active " href="/contact">Contact Us</a></li>
            </div>

           <div class="right-nav align-items-center d-flex justify-content-end list-inline">
              <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav" class="navbar navbar-expand-lg"> <a href="#" class="ht-nav-toggle active"><span></span></a>
<div class="row">
  <div class="col-sm">&nbsp;</div>
  <div clas="col-sm">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" alt="">
        <p class="mb-5"><b>PT. Crop Inspirasi Digital</b> <br> Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p>
        <div class="form-info">
          <h4>Contact info</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span>ALAMAT:</span>
              <p>Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Telepon:</span><a href="tel:"></a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
</nav>

<!--header end-->
<section></section>
    <!--body content start-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj2GrDSBy6ISeGg3aWUM4mn3izlA1wgt0&language=in&region=ID">
</script>
<section id="contactus" class="contact-1 pt-0" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/02.png">
<div class="container">
<div class="row row-eq-height">
  <div class="col-lg-6 col-md-12 order-lg-12">
	<div class="contact-main py-5">
	  <div class="section-title">
		<h2 style="margin-top: 60px">Contact</h2>
	  </div>
	  <!-- <form id="contact-form" class="row" method="post" action="http://www.crop.co.id/website/contact"> -->
		  <form action="http://www.crop.co.id/website/contact" class="row"  method="POST">
		<div class="messages"></div>
		<div class="form-group col-md-6">
		  <input id="form_name" type="text" name="name" class="form-control" placeholder="Name" required="required" data-error="Name is required.">
		  <div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-6">
		  <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
		  <div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-12">
		  <input id="form_subject" type="tel" name="subject" class="form-control" placeholder="Subject" required="required" data-error="Subject is required">
		  <div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-12">
		  <textarea id="form_message" name="message" class="form-control" placeholder="Message" rows="4" required="required" data-error="Please,leave us a message."></textarea>
		  <div class="help-block with-errors"></div>
		</div>
		<div class="col-md-12">
		  <!-- <button class="btn btn-theme btn-radius"><span>Send Message</span> -->
			<input type="submit" class="btn btn-theme btn-radius" name="send" value="Send Message">
		
		</div>
	  </form>
	</div>
  </div>
  <div class="col-lg-6 col-md-12 order-lg-1">
	<!-- <div id="tempat_peta"></div> -->
	<!-- <div id="map" ></div>  -->
    <iframe id="map" class="map-canvas md-iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15844.24911434328!2d107.577529!3d-6.8831433!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x57ca8e0ceca4a19!2sPT%20Crop%20Inspirasi%20Digital!5e0!3m2!1sid!2sid!4v1617695867538!5m2!1sid!2sid" width="600"  style="border:0;" allowfullscreen="" loading="lazy"></iframe>
	<!-- <div class="map-canvas md-iframe" data-zoom="22" data-lat="-6.848546" data-lng="107.492616" data-type="roadmap" data-hue="#ffc400" data-title="CROP, PT Crop Inspirasi Digital, IT Consultan, Software Development, Website, Mobile Apps" data-icon-path="http://www.crop.co.id/assetuser/images/marker.png" data-content="Bandung, Indonesia"></div>  -->
  </div>
</div> 
</div>
</section>

<!--body content end--> 
<script type="text/javascript">
function initialize() {
	var locations = [
	  ['<h5>PT. Crop Inspirasi Digital</h5>',-6.874059, 107.576974], 
		  
	];
	var infowindow = new google.maps.InfoWindow();

	var options = {
	  zoom: 16, 
	  center: new google.maps.LatLng(-6.874059, 107.576974 ),
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var map = new google.maps.Map(document.getElementById('map'), options);

var marker, i;
for (i = 0; i < locations.length; i++) {  
  marker = new google.maps.Marker({
	position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	map: map,
	 
  }); 
  google.maps.event.addListener(marker, 'click', (function(marker, i) {
	return function() {
	  infowindow.setContent(locations[i][0]);
	  infowindow.open(map, marker);
	}
  })(marker, i));
}

};
google.maps.event.addDomListener(window, 'load', initialize);
</script>

    
<!--footer start-->

<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="http://www.crop.co.id/assetuser/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" data-bg-img="http://www.crop.co.id/assetuser/images/bg/02.png">
            <div class="footer-logo">
              <img src="http://www.crop.co.id/uploads/1630167557-cap_crop_relationships_border.png" class="img-center" alt="">
            </div>
            <p class="mb-3">Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda.</p> 
            <!-- <a class="btn-simple" href="#"><span>Read More <i class="fas fa-long-arrow-alt-right"></i></span></a> -->
            <div class="social-icons social-border circle social-hover mt-5">
              <h4 class="title">Follow Us</h4>
              <ul class="list-inline">
                <li class="social-facebook"><a href="http://www.facebook.com"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="http://https://twitter.com/"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="http://https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8 py-8 md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              <h4 class="title">Tautan</h4>
              <div class="row">
                <div class="col-sm-5">
                  <ul class="list-unstyled">                    
                    <li><a class="nav-link " href="index.html"> Home</a></li><li><a class="nav-link " href="about.html"> About</a></li><li><a class="nav-link " href="service.html"> Service</a></li><li><a class="nav-link " href="knowledge.html"> Knowledge</a></li><li><a class="nav-link " href="career.html">Career</a></li><li><a class="nav-link " href="portofolio.html">Portofolio</a><li><a class="nav-link " href="contact.html">Contact Us</a></li></ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 sm-mt-5">
              <h4 class="title">Contact us</h4>
              <ul class="media-icon list-unstyled">
                <li><p class="mb-0">Jl. Sukahaji No.102, Sukarasa, Kec. Sukasari, <br> Kota Bandung, Jawa Barat 40152</p></li>
                <li><a href="mailto:info@crop.co.id <br> crop.indonesia@gmail.com">info@crop.co.id <br> crop.indonesia@gmail.com</a></li>
                <li><a href="tel:"></a>
                </li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>Copyright 2022 | All Rights Reserved</span>
          </div>
          <div class="col-md-6 text-md-right sm-mt-2"> <span>PT. Crop Inspirasi Digital</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

  
@endsection