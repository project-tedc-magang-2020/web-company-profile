<?php
$hostname = "localhost";
$database = "db_app_compro_crop";
$username = "root";
$password = "";
$kon = mysqli_connect($hostname, $username, $password, $database);
// script cek koneksi
if (!$kon) {
    die("Koneksi Tidak Berhasil: " . mysqli_connect_error());
}

$query = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '1'");
$result    =mysqli_fetch_array($query);

$query2 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '2'");
$result2    =mysqli_fetch_array($query2);

$query3 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '3'");
$result3    =mysqli_fetch_array($query3);

$query4 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '4'");
$result4    =mysqli_fetch_array($query4);

$query5 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '5'");
$result5    =mysqli_fetch_array($query5);

$query6 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '6'");
$result6    =mysqli_fetch_array($query6);

$query7 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '7'");
$result7    =mysqli_fetch_array($query7);

$query8 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '8'");
$result8    =mysqli_fetch_array($query8);

$query9 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '9'");
$result9    =mysqli_fetch_array($query9);

$query10 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '10'");
$result10    =mysqli_fetch_array($query10);

$query11 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '11'");
$result11    =mysqli_fetch_array($query11);

$query12 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '12'");
$result12    =mysqli_fetch_array($query12);

$query13 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '13'");
$result13    =mysqli_fetch_array($query13);

$query14 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '14'");
$result14    =mysqli_fetch_array($query14);

$query15 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '15'");
$result15    =mysqli_fetch_array($query15);

$query16 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '16'");
$result16    =mysqli_fetch_array($query16);

$query17 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '17'");
$result17    =mysqli_fetch_array($query17);

$query18 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '18'");
$result18    =mysqli_fetch_array($query18);

$query19 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '19'");
$result19    =mysqli_fetch_array($query19);

$query20 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '20'");
$result20    =mysqli_fetch_array($query20);

$query21 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '21'");
$result21    =mysqli_fetch_array($query21);

$query22 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '22'");
$result22    =mysqli_fetch_array($query22);

$query23 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '23'");
$result23    =mysqli_fetch_array($query23);

$query24 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '24'");
$result24    =mysqli_fetch_array($query24);

$query25 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '25'");
$result25    =mysqli_fetch_array($query25);

$query26 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '26'");
$result26    =mysqli_fetch_array($query26);

$query27 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '27'");
$result27    =mysqli_fetch_array($query27);

$query28 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '28'");
$result28    =mysqli_fetch_array($query28);

$query29 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '29'");
$result29    =mysqli_fetch_array($query29);

$query30 = mysqli_query($kon, "SELECT * FROM tb_admin WHERE urutan = '30'");
$result30    =mysqli_fetch_array($query30);

?> 

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Company Profile</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::asset('images/logo1.png')}}">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

    <!-- Custom fonts for this template-->
    <link href="{{URL::asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- CSS DATATABLES -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="{{URL::asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css?family=Rubik&display=swap');



button{
    font-size: calc(12px + (16 - 12) * ((100vw - 360px) / (1600 - 300))) !important;
}


button:focus {
    box-shadow: none !important;
    outline-width: 0;
}

.card{
    border-radius :12px;
    width: calc(500px + 10 * ((100vw - 320px) / 680)) ;
    box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.8) ;
}

.card-header{
    border-radius :12px !important;
}

.modal-body .btn-danger{
    border-radius: 11px ;
    box-shadow:  0px 5px 5px rgba(0, 0, 0, 0.2) ;
}
 .btn-light{
     background: transparent !important;
     border:0px !important;
 }

 .btn-light:hover{
    border-color:#fff !important;
 }

 .btn-light:active{
    border-color:#fff !important;
 }

@media (max-width: 526px) {
    .card{
        width: unset;
    }
}

    </style>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
    <br />
    <!-- Sidebar - Brand -->    
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <?php 
            $logoadmins = DB::table('tb_logo_admin')->get(); 
        ?>
        @foreach ($logoadmins as $lg)
        <div>
        <img src="{{ url('images/'.$lg->logo) }}" width="160" height="80">
        </div>
        @endforeach
    </a>
    <br />
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link <?php echo $result['status'] ?>" href="/admin/home">
            <i class="<?php echo $result['icon'] ?>"></i>
            <span>&nbsp; <?php echo $result['nama_menu'] ?></span></a>
    </li>
    
    <li class="nav-item">
        <a class="nav-link collapsed <?php echo $result2['status'] ?>" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="<?php echo $result2['icon'] ?>"></i>
            <span>&nbsp;&nbsp; <?php echo $result2['nama_menu'] ?></span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu <?php echo $result2['nama_menu'] ?> :</h6>
                <a class="collapse-item <?php echo $result3['status'] ?>" href="/datamaster/about"><?php echo $result3['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result4['status'] ?>" href="/datamaster/career"><?php echo $result4['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result5['status'] ?>" href="/datamaster/knowledge"><?php echo $result5['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result6['status'] ?>" href="/datamaster/portofolio"><?php echo $result6['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result7['status'] ?>" href="/datamaster/contact"><?php echo $result7['nama_menu'] ?></a>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed <?php echo $result8['status'] ?>" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="<?php echo $result8['icon'] ?>"></i>
            <span>&nbsp;&nbsp; <?php echo $result8['nama_menu'] ?></span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu <?php echo $result8['nama_menu'] ?> :</h6>
                <a class="collapse-item <?php echo $result9['status'] ?>" href="/website/agenda"><?php echo $result9['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result10['status'] ?>" href="/website/downloadfile"><?php echo $result10['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result11['status'] ?>" data-bs-toggle="dropdown" href="/website/media" aria-expanded="true" aria-controls="collapseUtilities"><?php echo $result11['nama_menu'] ?></a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="/website/foto">Galeri Foto</a></li>
                        <li><a class="dropdown-item" href="/website/video">Galeri Video</a></li>
                    </ul>
                <a class="collapse-item <?php echo $result12['status'] ?>" href="/website/newsletter"><?php echo $result12['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result13['status'] ?>" href="/website/promosi"><?php echo $result13['nama_menu'] ?></a>
                
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed <?php echo $result14['status'] ?>" href="#" data-toggle="collapse" data-target="#collapsePages"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="<?php echo $result14['icon'] ?>"></i>
            <span>&nbsp;&nbsp; <?php echo $result14['nama_menu'] ?></span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu <?php echo $result14['nama_menu'] ?> :</h6>
                <a class="collapse-item <?php echo $result15['status'] ?>" href="/administration/kategori"><?php echo $result15['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result16['status'] ?>" href="/administration/tags"><?php echo $result16['nama_menu'] ?></a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed <?php echo $result17['status'] ?>" href="#" data-toggle="collapse" data-target="#collapse"
            aria-expanded="true" aria-controls="collapse">
            <i class="<?php echo $result17['icon'] ?>"></i>
            <span>&nbsp;&nbsp; <?php echo $result17['nama_menu'] ?></span>
        </a>
        <div id="collapse" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu <?php echo $result17['nama_menu'] ?> :</h6>
                <a class="collapse-item <?php echo $result18['status'] ?>" href="/management/public"><?php echo $result18['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result19['status'] ?>" href="/management/admin"><?php echo $result19['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result20['status'] ?>" href="/management/authority"><?php echo $result20['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result21['status'] ?>" href="/management/usermanagement"><?php echo $result21['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result22['status'] ?>" href="/management/role"><?php echo $result22['nama_menu'] ?></a>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed <?php echo $result23['status'] ?>" href="#" data-toggle="collapse" data-target="#collapse2"
            aria-expanded="true" aria-controls="collapse2">
            <i class="<?php echo $result23['icon'] ?>"></i>
            <span>&nbsp;&nbsp; <?php echo $result23['nama_menu'] ?></span>
        </a>
        <div id="collapse2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Menu <?php echo $result23['nama_menu'] ?> :</h6>
                <a class="collapse-item <?php echo $result24['status'] ?>" href="/backup/logpublic"><?php echo $result24['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result25['status'] ?>" href="/backup/logadmin"><?php echo $result25['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result27['status'] ?>" href="/backup/backup"><?php echo $result26['nama_menu'] ?></a>

            </div>
        </div>
    <li class="nav-item active">
        <a class="nav-link <?php echo $result27['status'] ?>" href="/appconfig">
            <i class="<?php echo $result27['icon'] ?>"></i>
            <span>&nbsp; <?php echo $result27['nama_menu'] ?></span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed <?php echo $result28['status'] ?>" href="#" data-toggle="collapse" data-target="#collapse3"
            aria-expanded="true" aria-controls="collapse3">
            <i class="<?php echo $result28['icon'] ?>"></i>
            <span>&nbsp;&nbsp; <?php echo $result28['nama_menu'] ?></span>
        </a>
        <div id="collapse3" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?php echo $result29['status'] ?>" href="/logo/logoadmin"><?php echo $result29['nama_menu'] ?></a>
                <a class="collapse-item <?php echo $result30['status'] ?>" href="/logo/logouser"><?php echo $result30['nama_menu'] ?></a>
            </div>
        </div>
    </li>
    

    
    <br><br><br>
    
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

</ul>
<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

            <h1 class="h3 mb-0 text-gray-800">Company Profile</h1>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                    </a>
                    <!-- Dropdown - Messages -->
                    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                        aria-labelledby="searchDropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                            <div class="input-group">
                                <input type="text" class="form-control bg-light border-0 small"
                                    placeholder="Search for..." aria-label="Search"
                                    aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>

                

                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- Nav Item - User Information -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav me-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <div class="d-flex">
      <br />
      <br />
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <b>
        <li class="nav-item dropdown no-arrow" style="color: floralwhite;">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: black;"> 
                {{ Auth::user()->name }}
            </a>

            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('actionlogout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off"></i> &nbsp; {{ __('Logout') }}
                </a>

            <form id="logout-form" action="{{ route('actionlogout') }}" class="d-none">
                @csrf
            </form>
            </div>
        </li>
        </b>
      </ul>
    </div>
    </div>
        </div>

            </ul>

        </nav>
        <!-- End of Topbar -->

    <!-- Page content-->
    @yield('content')
</div>

<!-- Footer -->
<footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Company Profile </span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
            

    <!-- Bootstrap core JavaScript-->
    <script src="{{URL::asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{URL::asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{URL::asset('js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{URL::asset('vendor/chart.js/Chart.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{URL::asset('js/demo/chart-area-demo.js')}}"></script>
    <script src="{{URL::asset('js/demo/chart-pie-demo.js')}}"></script>

    <script src="{{URL::asset('js/demo/datatables-demo.js')}}"></script>
    <script src="{{URL::asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- DATATABLES --> 
     <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
       <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
       <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
       <script type="text/javascript" language="javascript">
           $(document).ready(function () {
           $('#datatab').DataTable();
            });
       </script>
</body>

</html>