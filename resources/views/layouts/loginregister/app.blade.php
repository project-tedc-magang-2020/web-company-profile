<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Company Profile</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="{{URL::asset('images/logo.png')}}">
    <style type="text/css">
        .body{
            background-image: linear-gradient(315deg, #19A186 10%, #F2CF43 74%);
            width: 1000px;
            height: 850px;
            position: auto;
            padding-left: 210px;
        }
    </style>
</head>
<body class="body">
    @yield('content')

</body>
</html>