<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>PT. Crop Inspirasi Digital</title>
<link rel="shortcut icon" href="http://www.crop.co.id/uploads/1630124270-favicon.png" />
<meta name="keywords" content="Crop, IT, Solution, Kreatif, Website, Consultant, Sistem Informasi, Sistem, Aplikasi" />
<meta name="description" content="Menjadi solusi diberbagai hal dibidang IT secara kreatif, inovatif dan profesional berlandasan kebersamaan untuk memberikan solusi bisnis anda." />

<!-- favicon icon -->
<link rel="shortcut icon" href="{{URL::asset('users/images/crop.png')}}" />

<!-- inject css start -->

<!--== bootstrap -->
<link href="{{URL::asset('users/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

<!--== animate -->
<link href="{{URL::asset('users/css/animate.css')}}" rel="stylesheet" type="text/css" />

<!--== fontawesome -->
<link href="{{URL::asset('users/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css" />

<!--== audioplayer -->
<link href="{{URL::asset('users/css/audioplayer/media-player.css')}}" rel="stylesheet" type="text/css" />

<!--== magnific-popup -->
<link href="{{URL::asset('users/css/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />

<!--== owl-carousel -->
<link href="{{URL::asset('users/css/owl-carousel/owl.carousel.css')}}" rel="stylesheet" type="text/css" />

<!--== base -->
<link href="{{URL::asset('users/css/base.css')}}" rel="stylesheet" type="text/css" />

<!--== shortcodes -->
<link href="{{URL::asset('users/css/shortcodes.css')}}" rel="stylesheet" type="text/css" />

<!--== default-theme -->
<link href="{{URL::asset('users/css/style.css')}}" rel="stylesheet" type="text/css" />

<!--== responsive -->
<link href="{{URL::asset('users/css/responsive.css')}}" rel="stylesheet" type="text/css" />

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-3668534986984467",
    enable_page_level_ads: true
  });
</script>
<!-- inject css end -->
<style type="text/css">
html {
 	scroll-behavior: smooth;
 	-webkit-transition: all 0.5s ease-in-out 0s;
    -moz-transition: all 0.5s ease-in-out 0s;
    -ms-transition: all 0.5s ease-in-out 0s;
    -o-transition: all 0.5s ease-in-out 0s;
    transition: all 0.5s ease-in-out 0s;
	}
.team-social-icon {display: inline-block; margin: 0 0 -18px; opacity: 0; transition: margin 0.2s ease 0s, opacity 0.2s ease 0s;}
.team-social-icon {margin: 16px 0 0; opacity: 1;}
.active .team-social-icon{ margin: 16px 0 0; opacity: 1;}
.team-social-icon ul {display: inline-block;}
.team-social-icon ul li {margin: 0; display: inline-block; position: relative; }
.team-social-icon ul li a {background: #ffffff; border-radius: 5px; height: 40px; width: 40px; line-height: 40px; color: #111121; display: inline-block; text-align: center;}
.team-social-icon ul li a:hover {background: none; color: #ff7810; border-radius: 50%}

.modal-content {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 100%; /* Could be more or less, depending on screen size */
}

</style>

</head>

<body>

  <!-- Page content-->
    @yield('content')

<!-- inject js start -->

<!--== jquery -->
<script src="{{URL::asset('users/js/jquery.3.3.1.min.js')}}"></script>

<!--== popper -->
<script src="{{URL::asset('users/js/popper.min.js')}}"></script>

<!--== bootstrap -->
<script src="{{URL::asset('users/js/bootstrap.min.js')}}"></script>

<!--== appear -->
<script src="{{URL::asset('users/js/jquery.appear.js')}}"></script> 

<!--== modernizr -->
<script src="{{URL::asset('users/js/modernizr.js')}}"></script> 

<!--== menu --> 
<script src="{{URL::asset('users/js/menu/jquery.smartmenus.js')}}"></script>

<!--== audioplayer -->
<script src="{{URL::asset('users/js/audioplayer/media-player.js')}}"></script>

<!--== magnific-popup -->
<script src="{{URL::asset('users/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script> 

<!--== owl-carousel -->
<script src="{{URL::asset('users/js/owl-carousel/owl.carousel.min.js')}}"></script> 

<!--== counter -->
<script src="{{URL::asset('users/js/counter/counter.js')}}"></script> 

<!--== countdown -->
<script src="{{URL::asset('users/js/countdown/jquery.countdown.min.js')}}"></script> 

<!--== isotope -->
<script src="{{URL::asset('users/js/isotope/isotope.pkgd.min.js')}}"></script> 

<!--== mouse-parallax -->
<script src="{{URL::asset('users/js/mouse-parallax/tweenmax.min.js')}}"></script>
<script src="{{URL::asset('users/js/mouse-parallax/jquery-parallax.js')}}"></script> 

<!--== contact-form -->
<script src="{{URL::asset('users/js/contact-form/contact-form.js')}}"></script>

<!--== validate -->
<script src="{{URL::asset('users/js/contact-form/jquery.validate.min.js')}}"></script>

<!--== map api -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!--== map -->
<script src="{{URL::asset('users/js/map.js')}}"></script>

<!--== wow -->
<script src="{{URL::asset('users/js/wow.min.js')}}"></script>

<!--== theme-script -->
<script src="{{URL::asset('users/js/theme-script.js')}}"></script>

<!-- inject js end -->
<!-- inject js end -->
<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle').click(function () {
      var collapse_content_selector = $(this).attr('href');
      var toggle_switch = $(this);
      $(collapse_content_selector).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch.html('Read More');
        } else {
          toggle_switch.html('Read Less');
        }
      });
    });

  });
</script>

<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle2').click(function () {
      var collapse_content_selector2 = $(this).attr('href');
      var toggle_switch2 = $(this);
      $(collapse_content_selector2).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch2.html('Read More');
        } else {
          toggle_switch2.html('Read Less');
        }
      });
    });

  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle3').click(function () {
      var collapse_content_selector3 = $(this).attr('href');
      var toggle_switch3 = $(this);
      $(collapse_content_selector3).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch3.html('Read More');
        } else {
          toggle_switch3.html('Read Less');
        }
      });
    });

  });
</script>

<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle').click(function () {
      var collapse_content_selector = $(this).attr('href');
      var toggle_switch = $(this);
      $(collapse_content_selector).toggle(function () {
      //   if ($(this).css('display') == 'none') {
      //     toggle_switch.html('IT Audit');
      //   } else {
      //     toggle_switch.html('Read Less');
      //   }
      });
    });

  });
</script>

<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle4').click(function () {
      var collapse_content_selector = $(this).attr('href');
      var toggle_switch = $(this);
      $(collapse_content_selector).toggle(function () {
      //   if ($(this).css('display') == 'none') {
      //     toggle_switch.html('IT Audit');
      //   } else {
      //     toggle_switch.html('Read Less');
      //   }
      });
    });

  });
</script>
<!--service-->

<!--portofolio-->
<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle').click(function () {
      var collapse_content_selector = $(this).attr('href');
      var toggle_switch = $(this);
      $(collapse_content_selector).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch.html('Read More');
        } else {
          toggle_switch.html('Read Less');
        }
      });
    });

  });
</script>

<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle2').click(function () {
      var collapse_content_selector2 = $(this).attr('href');
      var toggle_switch2 = $(this);
      $(collapse_content_selector2).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch2.html('Read More');
        } else {
          toggle_switch2.html('Read Less');
        }
      });
    });

  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.nav-toggle3').click(function () {
      var collapse_content_selector3 = $(this).attr('href');
      var toggle_switch3 = $(this);
      $(collapse_content_selector3).toggle(function () {
        if ($(this).css('display') == 'none') {
          toggle_switch3.html('Read More');
        } else {
          toggle_switch3.html('Read Less');
        }
      });
    });

  });


</script>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  
</body>
</html>