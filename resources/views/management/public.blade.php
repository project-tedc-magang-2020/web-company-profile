@extends('layouts.home.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="container">
<br><br>
<h2 class="h3 mb-2 text-gray-800" style="text-align:center;">Module Public</h2>
<br />
@include('layouts.messages')
<br />
</div>

<a data-toggle="modal" data-target="#addModal">
    <button class="bi bi-plus-circle-fill btn" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); color:white;"> Tambah Data</button>
</a>
<br>
<br/>

<!-- DataTales Example -->
<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); width: 100%;">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">No</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">ID Public</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Nama Menu</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%;">Link</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%;">Keterangan</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Icon</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Status</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">Urutan</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                </tfoot>
                
                <tbody>
                <?php $no=1;?>

                @foreach ($publics as $pe)
                    <tr>
                        <td style="vertical-align: middle; text-align: center;">{{ $no }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->id_public }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->nama_menu}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->link}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->keterangan}}</td>
                        <td style="vertical-align: middle; text-align: center;">
                            <i class="{{ $pe->icon }}"></i>
                        </td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->status}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->urutan}}</td>

        
                        <td style="vertical-align: middle; text-align: center;">
                            <div class="container mb-0">
                            <button data-toggle="modal" data-target="#editModal{{ $pe->id_public }}" class="bi bi-pencil-square editbtn btn btn-warning col-xl-3 col-md-4 mb-2" style="font-size: 1.3rem; color:white;" role="button"></button>
                            <button data-toggle="modal" data-target="#my-modal{{ $pe->id_public }}" class="bi bi-trash-fill btn btn-danger col-xl-3 col-md-4 mb-2" style="font-size: 1.2rem; color:white;" role="button"></button>
                            <button class="bi bi-eye-fill detailbtn btn btn-info col-xl-3 col-md-4 mb-2" data-toggle="modal" data-target="#myModal{{ $pe->id_public }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                            </div>
                        </td>
                    </tr>
                    <?php $no++;?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br>
<!-- /.container-fluid -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
<!-- End of Main Content -->


@foreach ($publics as $pe)
<div id="my-modal{{ $pe->id_public }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content border-0">   
<div class="modal-body p-0">
<div class="card border-0 p-sm-3 p-2 justify-content-center">
    <div class="card-header pb-0 bg-white border-0 "><div class="row"><div class="col ml-auto"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div> </div>
    <p class="font-weight-bold mb-2"> Are you sure you wanna delete this ?</p><p class="text-muted "> These changes will be visible on your portal and the data will be deleted.</p>     </div>
    <div class="card-body px-sm-4 mb-2 pt-1 pb-0"> 
        <div class="row justify-content-end no-gutters"><div class="col-auto"><button type="button" class="btn btn-light text-muted" data-dismiss="modal">Cancel</button><a class="btn btn-danger px-4" href="/management/public/{{ $pe->id_public }}" role="button">Delete</a></div><div class="col-auto"></div></div>
    </div>
</div>  
</div>
</div>
</div>
</div>
@endforeach

@foreach ($publics as $pe)
<!-- Modal Detail -->
<div id="myModal{{ $pe->id_public }}" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- konten modal-->
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<!-- heading modal -->
<div class="modal-header text-white">
<h4>Detail Data Module Public</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
<!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
</div>
<!-- body modal -->
<div class="modal-body">
<div class="row">
<div class="col-lg-12">
<table class="table table-bordered no-margin" style="background-color: white;">
<thead>
    <tr>
    <th>ID Public</th>
    <td>{{ $pe->id_public }}</td>
    </tr>
    <tr>
    <th>Nama Menu</th>
    <td>{{ $pe->nama_menu }}</td>
    </tr>
    <tr>
    <th>Link</th>
    <td>{{ $pe->link}}</td>
    </tr>
    <tr>
    <th>Keterangan</th>
    <td>{{ $pe->keterangan }}</td>
    </tr>
    <tr>
    <th>Icon</th>
    <td>{{ $pe->icon }}</td>
    </tr>
    <tr>
    <th>Status</th>
    <td>{{ $pe->status }}</td>
    </tr>
    <tr>
    <th>Urutan</th>
    <td>{{ $pe->urutan }}</td>
    </tr>
</thead>
</table>
</div>
</div>
</div>
<!-- footer modal -->
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
</div>
</div>
</div>
@endforeach

@foreach ($publics as $pe)
<div class="modal fade" id="editModal{{ $pe->id_public }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Edit Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
<form action="{{ url('/management/public/update',  $pe->id_public) }}" method="POST" id="editform" enctype="multipart/form-data">
{!! csrf_field() !!}
<div class="form-group">
<label>ID Public</label>
<input type="text" name="id_public" class="form-control" required="required" readonly value="{{ $pe->id_public }}">
</div>
<div class="form-group">
<label>Nama Menu</label>
<input type="text" name="nama_menu" class="form-control" required="required" value="{{ $pe->nama_menu}}">
</div>
<div class="form-group">
<label>Link</label>
<input type="text" name="link" class="form-control" required="required" value="{{ $pe->link}}">
</div>
<div class="form-group">
<label>Keterangan</label>
<input type="text" name="keterangan" class="form-control" required="required" value="{{ $pe->keterangan}}">
</div>
<div class="form-group">
<label>Icon</label>
<input type="text" name="icon" class="form-control" required="required" value="{{ $pe->icon}}">
</div>
<div class="form-group">
<label>Status</label>
<input type="text" name="status" class="form-control" required="required" value="{{ $pe->status}}">
</div>
<div class="form-group">
<label>Urutan</label>
<input type="number" name="urutan" class="form-control" required="required" value="{{ $pe->urutan}}">
</div>

<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>
@endforeach


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Tambah Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body text-white">
<form action="{{ route('management.storepublic') }}" method="POST" id="editform" enctype="multipart/form-data">
@csrf
<div class="form-group">
<label>ID Public</label>
<input type="text" name="id_public" class="form-control" value="{{'MP-'.$kd}}" required readonly>
</div>
<div class="form-group">
<label>Nama Menu</label>
<input type="text" name="nama_menu" class="form-control" required>
</div>
<div class="form-group">
<label>Link</label>
<input type="text" name="link" class="form-control" required>
</div>
<div class="form-group">
<label>Keterangan</label>
<input type="text" name="keterangan" class="form-control" required>
</div>
<div class="form-group">
<label>Icon</label>
<input type="text" name="icon" class="form-control" required>
</div>
<div class="form-group">
<label>Status</label>
<input type="text" name="status" class="form-control" required>
</div>
<div class="form-group">
<label>Urutan</label>
<input type="number" name="urutan" class="form-control" required>
</div>

<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>

@endsection