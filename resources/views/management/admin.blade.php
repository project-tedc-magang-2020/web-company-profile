@extends('layouts.home.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="container">
<br><br>
<h2 class="h3 mb-2 text-gray-800" style="text-align:center;">Module Admin</h2>
<br />
@include('layouts.messages')
<br />
</div>

<a data-toggle="modal" data-target="#addModal">
    <button class="bi bi-plus-circle-fill btn" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); color:white;"> Tambah Menu</button>
</a>
&nbsp;
<a href="/management/submenuadmin">
    <button class="btn" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); color:white;">&nbsp; Sub Menu</button>
</a>
<br>
<br/>

<!-- DataTales Example -->
<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); width: 100%;">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">No.</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">ID Admin</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Nama Menu</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%;">Link</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%;">Keterangan</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Icon</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Status</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">Sub Menu</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                </tfoot>
                
                <tbody>
                <?php
                    $no=1;
                ?>
                @foreach ($admins as $pe)
                    <tr>
                        <td style="vertical-align: middle; text-align: center;">{{ $no++ }}.</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->id_admin }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->nama_menu}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->link}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->keterangan}}</td>
                        <td style="vertical-align: middle; text-align: center;">
                            <i class="{{ $pe->icon }}"></i>
                        </td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->status}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->sub_menu}}</td>

        
                        <td style="vertical-align: middle; text-align: center;">
                            <div class="container mb-0">
                            <button data-toggle="modal" data-target="#editModal{{ $pe->id_admin }}" class="bi bi-pencil-square editbtn btn btn-warning col-xl-3 col-md-4 mb-2" style="font-size: 1.3rem; color:white; width: 10.3rem" role="button"></button>
                            <button data-toggle="modal" data-target="#my-modal{{ $pe->id_admin }}" class="bi bi-trash-fill btn btn-danger col-xl-3 col-md-4 mb-2" style="font-size: 1.2rem; color:white;" role="button"></button>
                            <button class="bi bi-eye-fill detailbtn btn btn-info col-xl-3 col-md-4 mb-2" data-toggle="modal" data-target="#myModal{{ $pe->id_admin }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                            </div>
                        </td>

                    </tr>
                @endforeach
                
                </tbody>
            </table>

        </div>
    </div>
</div>



</div>
<br><br><br><br><br><br><br><br><br><br><br><br>
<!-- /.container-fluid -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
<!-- End of Main Content -->


@foreach ($admins as $pe)
<div id="my-modal{{ $pe->id_admin }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content border-0">   
<div class="modal-body p-0">
<div class="card border-0 p-sm-3 p-2 justify-content-center">
    <div class="card-header pb-0 bg-white border-0 "><div class="row"><div class="col ml-auto"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div> </div>
    <p class="font-weight-bold mb-2"> Are you sure you wanna delete this ?</p><p class="text-muted "> These changes will be visible on your portal and the data will be deleted.</p>     </div>
    <div class="card-body px-sm-4 mb-2 pt-1 pb-0"> 
        <div class="row justify-content-end no-gutters"><div class="col-auto"><button type="button" class="btn btn-light text-muted" data-dismiss="modal">Cancel</button><a class="btn btn-danger px-4" href="/management/admin/{{ $pe->id_admin }}" role="button">Delete</a></div><div class="col-auto"></div></div>
    </div>
</div>  
</div>
</div>
</div>
</div>
@endforeach

@foreach ($admins as $pe)
<!-- Modal Detail -->
<div id="myModal{{ $pe->id_admin }}" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- konten modal-->
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<!-- heading modal -->
<div class="modal-header text-white">
<h4>Detail Data Module Admin</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
<!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
</div>
<!-- body modal -->
<div class="modal-body">
<div class="row">
<div class="col-lg-12">
<table class="table table-bordered no-margin" style="background-color: white;">
<thead>
    <tr>
    <th>ID Admin</th>
    <td>{{ $pe->id_admin }}</td>
    </tr>
    <tr>
    <th>Nama Menu</th>
    <td>{{ $pe->nama_menu }}</td>
    </tr>
    <tr>
    <th>Link</th>
    <td>{{ $pe->link}}</td>
    </tr>z
    <tr>
    <th>Keterangan</th>
    <td>{{ $pe->keterangan }}</td>
    </tr>
    <tr>
    <th>Icon</th>
    <td>{{ $pe->icon }}</td>
    </tr>
    <tr>
    <th>Status</th>
    <td>{{ $pe->status }}</td>
    </tr>
    <tr>
    <th>Sub Menu</th>
    <td>{{ $pe->sub_menu }}</td>
    </tr>
</thead>
</table>
</div>
</div>
</div>
<!-- footer modal -->
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
</div>
</div>
</div>
@endforeach

@foreach ($admins as $pe)
<div class="modal fade" id="editModal{{ $pe->id_admin }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Edit Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
<form action="{{ url('/management/admin/update',  $pe->id_admin) }}" method="POST" id="editform">
{!! csrf_field() !!}
<div class="form-group">
<label>ID Admin</label>
<input type="text" name="" class="form-control" required="required" readonly value="{{ $pe->id_admin }}">
</div>
<div class="form-group">
<label>Nama Menu</label>
<input type="text" name="nama_menu" class="form-control" required="required" value="{{ $pe->nama_menu}}">
</div>
<div class="form-group">
<label>Link</label>
<input type="text" name="link" class="form-control" required="required" value="{{ $pe->link}}">
</div>
<div class="form-group">
<label>Keterangan</label>
<input type="text" name="keterangan" class="form-control" required="required" value="{{ $pe->keterangan}}">
</div>
<div class="form-group">
<label>Icon</label>
<input type="text" name="icon" class="form-control" required="required" value="{{ $pe->icon}}">
</div>

<div class="form-group">
    <label>Status</label> <br>
    <select name="status" class="form-select">
        <option value="{{ $pe->status }}" hidden>{{ $pe->status }}</option>
        <option value="enabled">Enabled</option>
        <option value="disabled">Disabled</option>
    </select>
</div>
<div class="form-group">
    <label>Jumlah Sub Menu</label>
    <input type="number" name="sub_menu" class="form-control" required value="{{ $pe->sub_menu }}">
</div>

<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>
@endforeach


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Tambah Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body text-white">
<form action="{{ route('management.storeadmin') }}" method="POST" id="editform" enctype="multipart/form-data">
@csrf
<div class="form-group">
<label>Nama Menu</label>
<input type="text" name="nama_menu" class="form-control" required>
</div>
<div class="form-group">
<label>Link</label>
<input type="text" name="link" class="form-control" required>
</div>
<div class="form-group">
<label>Keterangan</label>
<input type="text" name="keterangan" class="form-control" required>
</div>
<div class="form-group">
<label>Icon</label>
<input type="text" name="icon" class="form-control" required>
</div>
<div class="form-group">
    <label>Status</label> <br>
    <select name="status" class="form-select" required>
        <option value="" hidden>Status</option>
        <option value="enabled">Enabled</option>
        <option value="disabled">Disabled</option>
    </select>
</div>

<div class="form-group">
    <label>Jumlah Sub Menu</label>
    <input type="number" name="sub_menu" class="form-control" required placeholder="Jumlah Sub Menu">
</div>


<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>



@endsection