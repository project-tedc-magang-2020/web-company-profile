@extends('layouts.home.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="container">
<br><br>
<h2 class="h3 mb-2 text-gray-800" style="text-align:center;">Module Authority</h2>
<br />
@include('layouts.messages')
<br />
</div>
<div class="widget-body">
				<div class="widget-main">
					<div class="row-fluid">
						<div class="span6">
							<div class="control-group">
							<label class="span3 control-label">Nama Role</label>
								<div class="span9">
									<select onchange="showMe(this);" name="role_id" id="role_id" class="span12 select2 form-select" style="width:100%">
										<option value="" selected="selected" disabled>- Pilih -</option>
										<option value="1">Super User</option>
										<option value="2">User</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>

			<div id="idShowMe" style="display: none">
				<!-- DataTales Example -->
<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); width: 100%;">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" style="background-color: white; width: 100%;">
                <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div></th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Nama Menu</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">View</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">Create</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">Edit</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">Delete</th>
						<th style="vertical-align: middle; text-align: center; width: 5%;">Export</th>
						<th style="vertical-align: middle; text-align: center; width: 5%;">Duplicate</th>
						<th style="vertical-align: middle; text-align: center; width: 5%;">Unlock</th>
                    </tr>
                </thead>
                <tfoot>
                </tfoot>
                @foreach($menus as $mn)
                <tbody>
                <?php $no=1;?>

                    <tr>
                        <td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div></td>
                        <td style="vertical-align: middle; text-align: left;">{{$mn->nama_menu}}</td>
                        <td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
						<td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
						<td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
						<td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
						<td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
						<td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
						<td style="vertical-align: middle; text-align: center;">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
						</div>
						</td>
                    </tr>
                    <?php $no++; ?>
                </tbody>
				@endforeach
            </table>
        </div>
    </div>
</div>
			</div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<!-- /.container-fluid -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
<!-- End of Main Content -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>

<script src="jquery-2.1.4.js"></script>

<script type="text/javascript">
	   function showMe(e) {
    var strdisplay = e.options[e.selectedIndex].value;
    var e = document.getElementById("idShowMe");
    if(strdisplay == "Hide") {
        e.style.display = "none";
    } else {
        e.style.display = "block";
    }
}
</script>


@endsection