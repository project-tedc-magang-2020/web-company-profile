@extends('layouts.home.app')
@section('content')



                <!-- Begin Page Content -->
                <div class="container">
                <h2 class="h4 mb-0 text-gray-800" style="text-align:center;">Menu Dashboard</h2>
                </div>
                <br>
                <br />
                <br />
                <section>
                <center>
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-4 col-md-16 mb-12">
        <div class="card border-left-primary shadow h-100 py-1" style="background-color: #F2CF43; width: 20rem;">
          <div class="card-body" style="height: 16rem;">
            <div class="container">
              <center>
                <img src="{{URL::asset('images/menu1.png')}}" width="170" height="170">
                <div class="card-body">
                  <a href="" class="btn" role="button" style="background-color: #1f9eda;">Email Masuk</a>
                </div>
              </center>
            </div>
          </div>
        </div>
      </div>

      <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-16 mb-12">
          <div class="card border-left-primary shadow h-100 py-1" style="background-color: #F2CF43; width: 20rem;">
            <div class="card-body" style="height: 16rem;">
              <div class="container">
                <center>
                  <img src="{{URL::asset('images/menu2.png')}}" width="170" height="170">
                  <div class="card-body">
                    <button href="#" class="btn" style="background-color: #1f9eda;">Project Done</button>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>

      <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-16 mb-12">
          <div class="card border-left-primary shadow h-100 py-1" style="background-color: #F2CF43; width: 20rem;">
            <div class="card-body" style="height: 16rem;">
              <div class="container">
                <center>
                  <img src="{{URL::asset('images/menu3.png')}}" width="170" height="170">
                  <div class="card-body">
                    <a href="" class="btn" role="button" style="background-color: #1f9eda;">Jumlah Client</a>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
      </center>
      </section>
      <br><br><br>

                
            <br />
            <br />
            <br />
            <br />

            
@endsection