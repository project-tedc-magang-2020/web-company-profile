@extends('layouts.home.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="container">
<br><br>
<h2 class="h3 mb-2 text-gray-800" style="text-align:center;">Galeri Video</h2>
<br />
@include('layouts.messages')
<br />
</div>

<a data-toggle="modal" data-target="#addModal">
    <button class="bi bi-plus-circle-fill btn" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); color:white;"> Tambah Data</button>
</a>
<br>
<br/>

<!-- DataTales Example -->
<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); width: 100%;">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">No</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">ID Video</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Nama Kegiatan</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Video</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%;">Tanggal</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                </tfoot>
                
                <tbody>
                <?php $no=1;?>

                @foreach ($videos as $pe)
                    <tr>
                        <td style="vertical-align: middle; text-align: center;">{{ $no }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->id_video }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->nama_kegiatan}}</td>
                        <td style="vertical-align: middle; text-align: center;">
                            <video width="300" controls>
                                <source src="{{ url('images/'.$pe->video) }}" class="img-thumbnail" width="200" height="200" type="video/mp4">
                            </video>
                        </td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->tanggal}}</td>
        
                        <td style="vertical-align: middle; text-align: center;">
                            <div class="container mb-0">
                            <button data-toggle="modal" data-target="#editModal{{ $pe->id_video }}" class="bi bi-pencil-square editbtn btn btn-warning col-xl-3 col-md-4 mb-2" style="font-size: 1.3rem; color:white;" role="button"></button>
                            <button data-toggle="modal" data-target="#my-modal{{ $pe->id_video }}" class="bi bi-trash-fill btn btn-danger col-xl-3 col-md-4 mb-2" style="font-size: 1.2rem; color:white;" role="button"></button>
                            <button class="bi bi-eye-fill detailbtn btn btn-info col-xl-3 col-md-4 mb-2" data-toggle="modal" data-target="#myModal{{ $pe->id_video }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br>
<!-- /.container-fluid -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
<!-- End of Main Content -->


@foreach ($videos as $pe)
<div id="my-modal{{ $pe->id_video }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content border-0">   
<div class="modal-body p-0">
<div class="card border-0 p-sm-3 p-2 justify-content-center">
    <div class="card-header pb-0 bg-white border-0 "><div class="row"><div class="col ml-auto"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div> </div>
    <p class="font-weight-bold mb-2"> Are you sure you wanna delete this ?</p><p class="text-muted "> These changes will be visible on your portal and the data will be deleted.</p></div>
    <div class="card-body px-sm-4 mb-2 pt-1 pb-0"> 
        <div class="row justify-content-end no-gutters"><div class="col-auto"><button type="button" class="btn btn-light text-muted" data-dismiss="modal">Cancel</button><a class="btn btn-danger px-4" href="/website/video/{{ $pe->id_video }}" role="button">Delete</a></div><div class="col-auto"></div></div>
    </div>
</div>  
</div>
</div>
</div>
</div>
@endforeach

@foreach ($videos as $pe)
<!-- Modal Detail -->
<div id="myModal{{ $pe->id_video }}" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- konten modal-->
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<!-- heading modal -->
<div class="modal-header text-white">
<h4>Detail Data Video</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
<!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
</div>
<!-- body modal -->
<div class="modal-body">
<div class="row">
<div class="col-lg-12">
<table class="table table-bordered no-margin" style="background-color: white;">
<thead>
    <tr>
    <th>ID Video</th>
    <td>{{ $pe->id_video }}</td>
    </tr>
    <tr>
    <th>Nama Kegiatan</th>
    <td>{{ $pe->nama_kegiatan}}</td>
    </tr>
    <tr>
    <th>Video</th>
    <td>{{ $pe->video }}</td>
    </tr>
    <tr>
    <th>Tanggal</th>
    <td>{{ $pe->tanggal}}</td>
    </tr>
</thead>
</table>
</div>
</div>
</div>
<!-- footer modal -->
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
</div>
</div>
</div>
@endforeach

@foreach ($videos as $pe)
<div class="modal fade" id="editModal{{ $pe->id_video }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Edit Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
<form action="{{ url('/website/video/update',  $pe->id_video) }}" method="POST" id="editform" enctype="multipart/form-data">
{!! csrf_field() !!}
<div class="form-group">
<label>ID Video</label>
<input type="text" name="id_video" class="form-control" required="required" readonly value="{{ $pe->id_video }}">
</div>
<div class="form-group">
<label>Nama Kegiatan</label>
<input type="textarea" name="nama_kegiatan" class="form-control" required="required" value="{{ $pe->nama_kegiatan}}">
</div>
<div class="form-group">
<label>Video</label>
<input type="file" name="video" class="form-control" required="required" value="{{ $pe->video}}">
</div>
<div class="form-group">
<label>Tanggal</label>
<input type="datapicker" name="tanggal" class="form-control" required="required" value="{{ $pe->tanggal}}">
</div>

<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>
@endforeach


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Tambah Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body text-white">
<form action="{{ route('website.storevideo') }}" method="POST" id="editform" enctype="multipart/form-data">
@csrf
<div class="form-group">
<label>ID Video</label>
<input type="text" name="id_video" class="form-control" value="{{'V-'.$kd}}" required readonly>
</div>
<div class="form-group">
<label>Nama Kegiatan</label>
<textarea type="textarea" name="nama_kegiatan" class="form-control" required></textarea>
</div>
<div class="form-group">
<label>Video</label>
<input type="file" name="video" class="form-control">
</div>
<div class="form-group">
<label>Tanggal</label>
<input id="datepicker2" name="tanggal" class="form-control"  required>
</div>

<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>

<script>
        $('#datepicker').datepicker({
			format: 'yy/mm/dd',
            uiLibrary: 'bootstrap4'
        });
    </script>

<script>
        $('#datepicker2').datepicker({
			format: 'yy/mm/dd',
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection