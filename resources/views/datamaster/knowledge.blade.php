@extends('layouts.home.app')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="container">
<br><br>
<h2 class="h3 mb-2 text-gray-800" style="text-align:center;">Knowledge</h2>
<br />
@include('layouts.messages')
<br />
</div>

<a data-toggle="modal" data-target="#addModal">
    <button class="bi bi-plus-circle-fill btn" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); color:white;"> Tambah Data</button>
</a>
<br>
<br/>

<!-- DataTales Example -->
<div class="card shadow mb-4" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%); width: 100%;">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" style="background-color: white; width: 100%;">
                <thead>
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">No</th>
                        <th style="vertical-align: middle; text-align: center; width: 5%;">ID Knowledge</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Gambar</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Tanggal Berita</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Judul Berita</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%;">Deskripsi</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%;">Action</th>
                    </tr>
                </thead>
                <tfoot>
                </tfoot>
                
                <tbody>
                <?php $no=1;?>

                @foreach ($knowledges as $pe)
                    <tr>
                        <td style="vertical-align: middle; text-align: center;">{{ $no }}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->id_knowledge }}</td>
                        <td style="vertical-align: middle; text-align: center;">
                            <img src="{{ url('images/'.$pe->gambar) }}" class="img-thumbnail" width="200" height="200">
                        </td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->tanggal_berita}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->judul_berita}}</td>
                        <td style="vertical-align: middle; text-align: center;">{{ $pe->deskripsi}}</td>

                        <td style="vertical-align: middle; text-align: center;">
                            <div class="container mb-0">
                            <button data-toggle="modal" data-target="#editModal{{ $pe->id_knowledge }}" class="bi bi-pencil-square editbtn btn btn-warning col-xl-3 col-md-4 mb-2" style="font-size: 1.3rem; color:white;" role="button"></button>
                            <button data-toggle="modal" data-target="#my-modal{{ $pe->id_knowledge }}" class="bi bi-trash-fill btn btn-danger col-xl-3 col-md-4 mb-2" style="font-size: 1.2rem; color:white;" role="button"></button>
                            <button class="bi bi-eye-fill detailbtn btn btn-info col-xl-3 col-md-4 mb-2" data-toggle="modal" data-target="#myModal{{ $pe->id_knowledge }}" style="font-size: 1.3rem; color:white;" role="button"></button>
                            </div>
                        </td>
                    </tr>
                    <?php $no++;?>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
<br><br><br><br><br><br><br><br><br><br><br><br>
<!-- /.container-fluid -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
<i class="fas fa-angle-up"></i>
</a>
<!-- End of Main Content -->


@foreach ($knowledges as $pe)
<div id="my-modal{{ $pe->id_knowledge }}" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content border-0">   
<div class="modal-body p-0">
<div class="card border-0 p-sm-3 p-2 justify-content-center">
    <div class="card-header pb-0 bg-white border-0 "><div class="row"><div class="col ml-auto"><button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button></div> </div>
    <p class="font-weight-bold mb-2"> Are you sure you wanna delete this ?</p><p class="text-muted "> These changes will be visible on your portal and the data will be deleted.</p>     </div>
    <div class="card-body px-sm-4 mb-2 pt-1 pb-0"> 
        <div class="row justify-content-end no-gutters"><div class="col-auto"><button type="button" class="btn btn-light text-muted" data-dismiss="modal">Cancel</button><a class="btn btn-danger px-4" href="/datamaster/knowledge/{{ $pe->id_knowledge }}" role="button">Delete</a></div><div class="col-auto"></div></div>
    </div>
</div>  
</div>
</div>
</div>
</div>
@endforeach

@foreach ($knowledges as $pe)
<!-- Modal Detail -->
<div id="myModal{{ $pe->id_knowledge }}" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
<!-- konten modal-->
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<!-- heading modal -->
<div class="modal-header text-white">
<h4>Detail Data Knowledge</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal">&times;</button>
<!-- <h4 class="modal-title">Ini adalah heading dari Modal</h4> -->
</div>
<!-- body modal -->
<div class="modal-body">
<div class="row">
<div class="col-lg-12">
<table class="table table-bordered no-margin" style="background-color: white;">
<thead>
    <tr>
    <th>ID Knowledge</th>
    <td>{{ $pe->id_knowledge }}</td>
    </tr>
    <tr>
    <th>Gambar</th>
    <td>{{ $pe->gambar }}</td>
    </tr>
    <tr>
    <th>Tanggal Berita</th>
    <td>{{ $pe->tanggal_berita}}</td>
    </tr>
    <tr>
    <th>Judul Berita</th>
    <td>{{ $pe->judul_berita }}</td>
    </tr>
    <th>Isi Konten</th>
    <td>{{ $pe->isi_konten }}</td>
    </tr>
    <th>Deskripsi</th>
    <td>{{ $pe->deskripsi }}</td>
    </tr>
</thead>
</table>
</div>
</div>
</div>
<!-- footer modal -->
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
</div>
</div>
</div>
</div>
@endforeach

@foreach ($knowledges as $pe)
<div class="modal fade" id="editModal{{ $pe->id_knowledge }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Edit Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body">
<form action="{{ url('/datamaster/knowledge/update',  $pe->id_knowledge) }}" method="POST" id="editform" enctype="multipart/form-data">
{!! csrf_field() !!}
<div class="form-group">
<label>ID Knowledge</label>
<input type="text" name="id_knowledge" class="form-control" required="required" readonly value="{{ $pe->id_knowledge }}">
</div>
<div class="form-group">
<label>Gambar</label>
<input type="file" name="gambar" class="form-control" required="required" value="{{ $pe->gambar}}">
</div>
<div class="form-group">
<label>Tanggal Berita</label>
<input id="datepicker" name="tanggal_berita" class="form-control" required="required" value="{{ $pe->tanggal_berita}}">
</div>
<div class="form-group">
<label>Judul Berita</label>
<input type="text" name="judul_berita" class="form-control" required="required" value="{{ $pe->judul_berita}}">
</div>
<div class="form-group">
<label>Isi Konten</label>
<input type="text" name="isi_konten" class="form-control" required="required" value="{{ $pe->isi_konten}}">
</div>
<div class="form-group">
<label>Deskripsi</label>
<input type="text" name="author" class="form-control" required="required" value="{{ $pe->deskripsi}}">
</div>


<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>
@endforeach


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content" style="background-image: linear-gradient(315deg, #55D284 0%, #F2CF07 74%);">
<div class="modal-header text-white">
<h4 class="modal-title" id="exampleModalLabel">Tambah Data</h4>
<button type="button" class="close" style="color: white;" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body text-white">
<form action="{{ route('datamaster.storeknowledge') }}" method="POST" id="editform" enctype="multipart/form-data">
@csrf
<div class="form-group">
<label>ID Knowledge</label>
<input type="text" name="id_knowledge" class="form-control" value="{{'KN-'.$kd}}" required readonly>
</div>
<div class="form-group">
<label>Gambar</label>
<input type="file" name="gambar" class="form-control" required>
</div>
<div class="form-group">
<label>Tanggal Berita</label>
<input id="datepicker2" name="tanggal_berita" class="form-control" required>
</div>
<div class="form-group">
<label>Judul Berita</label>
<textarea type="textarea" name="judul_berita" class="form-control" rows="3" required></textarea>
</div>
<div class="form-group">
<label>Isi Konten</label>
<textarea type="textarea" name="isi_konten" class="form-control" rows="3" required></textarea>
</div>
<div class="form-group">
<label>Deskripsi</label>
<input type="text" name="deskripsi" class="form-control" required></textarea>
</div>

<br />
<div class="modal-footer">
<center><button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button></center>
<center><button type="submit" class="btn btn-primary">Simpan</button></center>
</div>
</form>
</div>
</div>
</div>

</div>



<script>
        $('#datepicker').datepicker({
			format: 'yy/mm/dd',
            uiLibrary: 'bootstrap4'
        });
    </script>

<script>
        $('#datepicker2').datepicker({
			format: 'yy/mm/dd',
            uiLibrary: 'bootstrap4'
        });
    </script>

@endsection