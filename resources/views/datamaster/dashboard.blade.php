@extends('layouts.home.app')
@section('content')

<!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <br />
            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div>
                    <img src="{{ URL::asset('images/logo.png'); }}" width="160" height="80" alt="" />
                </div>
            </a>
            <br />
            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="/home">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>&nbsp; Dashboard</span></a>
            </li>
            
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-envelope"></i>
                    <span>&nbsp;&nbsp; Surat</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Menu Surat :</h6>
                        <a class="collapse-item" href="utilities-color.html">Surat Invoice</a>
                        <a class="collapse-item" href="utilities-border.html">Surat BAST</a>
                        <a class="collapse-item" href="utilities-animation.html">Surat Pemesanan</a>
                        <a class="collapse-item" href="utilities-other.html">Other</a>
                    </div>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>&nbsp;Laporan Keuangan</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Menu Laporan Keuangan :</h6>
                        <a class="collapse-item" href="utilities-color.html">Kode Akun</a>
                        <a class="collapse-item" href="utilities-border.html">Input Jurnal</a>
                        <a class="collapse-item" href="utilities-animation.html">Kode Bantu</a>
                        <a class="collapse-item" href="utilities-other.html">Jurnal Umum</a>
                        <a class="collapse-item" href="utilities-other.html">Buku Besar</a>
                        <a class="collapse-item" href="utilities-other.html">Buku Pembantu</a>
                        <a class="collapse-item" href="utilities-other.html">Neraca Lajur</a>
                        <a class="collapse-item" href="utilities-other.html">Laba Rugi</a>
                        <a class="collapse-item" href="utilities-other.html">Neraca</a>
                        <a class="collapse-item" href="utilities-other.html">Perubahan Modal</a>
                    </div>
                </div>
            </li>

            <li class="nav-item active">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-coins"></i>
                    <span>&nbsp; Data Master</span>
                </a>
                <div id="collapsePages" class="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Menu Data Master</h6>
                        <a class="collapse-item active" href="/datamaster/dashboard">Dashboard</a>
                        <a class="collapse-item" href="/datamaster/client">Client</a>
                        <a class="collapse-item" href="/datamaster/jabatan">Jabatan</a>
                        <a class="collapse-item" href="/datamaster/karyawan">Karyawan</a>
                        <a class="collapse-item" href="/datamaster/tipesurat">Tipe Surat</a>
                    </div>
                </div>
            </li>
            <br><br><br>
            
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <h1 class="h3 mb-0 text-gray-800">Surat dan Laporan Keuangan</h1>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

                        

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <div class="d-flex">
              <br />
              <br />
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right">
                <b>
                <li class="nav-item dropdown no-arrow" style="color: floralwhite;">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="color: black;"> 
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('actionlogout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> &nbsp; {{ __('Logout') }}
                        </a>

                    <form id="logout-form" action="{{ route('actionlogout') }}" class="d-none">
                        @csrf
                    </form>
                    </div>
                </li>
                </b>
              </ul>
            </div>
            </div>
        </div>

                    </ul>
                    <br>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container">
                <h2 class="h4 mb-0 text-gray-800">Dashboard</h2>
                </div>
                <br>
                <br />
                <br />
                <div class="container-fluid">


                    <!-- Content Row -->
                    <div class="row justify-content-evenly">

                        <!-- Earnings (Monthly) Card Example -->
                        
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-primary shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-4">
                                                <h3>Jumlah Client</h3></div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($clients) }} Orang</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-users fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-success shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-success text-uppercase mb-4">
                                                <h3>Jumlah Jabatan</h3></div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($jabatans) }} Jabatan</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-briefcase fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br />
                    <br />
                    <div class="row justify-content-evenly">
                        
                        <!-- Pending Requests Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-danger shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-4"><h3>Jumlah Karyawan</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($karyawans) }} Orang</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-user-tie fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Earnings (Monthly) Card Example -->
                        <div class="col-xl-4 col-md-16 mb-12">
                            <div class="card border-left-info shadow h-100 py-1">
                                <div class="card-body">
                                    <div class="container">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-4"><h3>Jumlah Jenis Surat</h3>
                                            </div>
                                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ count($tipesurats) }} Buah</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-envelope-open-text fa-4x text-gray-600"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                        
                </div>
            <!-- End of Main Content -->

            </div>
            <br />
            <br />

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; 2022 Surat dan Laporan Keuangan</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
@endsection