<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Datamaster;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\CareerController;
use App\App\Http\Controllers\Users\AboutusersController;
use App\App\Http\Controllers\Users\Berita1usersController;
use App\App\Http\Controllers\Users\Berita2usersController;
use App\App\Http\Controllers\Users\Berita3usersController;
use App\App\Http\Controllers\Users\Berita4usersController;
use App\App\Http\Controllers\Users\CareerusersController;
use App\App\Http\Controllers\Users\HomeusersController;
use App\App\Http\Controllers\Users\IndexusersController;
use App\App\Http\Controllers\Users\KnowledgeusersController;
use App\App\Http\Controllers\Users\PortofoliousersController;
use App\App\Http\Controllers\Users\ServiceusersController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/backend', [LoginController::class, 'login'])->name('login');
Route::post('actionlogin', [LoginController::class, 'actionlogin'])->name('actionlogin');

Route::get('/admin/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('actionlogout', [LoginController::class, 'actionlogout'])->name('actionlogout')->middleware('auth');


Route::prefix('datamaster')->group(function(){	
//about
	Route::get('/about', [App\Http\Controllers\Datamaster\AboutController::class, 'index'])->name('datamaster.about');
	Route::get('/about/create', [App\Http\Controllers\Datamaster\AboutController::class, 'create'])->name('datamaster.createabout');
    Route::post('/about/store', [App\Http\Controllers\Datamaster\AboutController::class, 'store'])->name('datamaster.storeabout');
    Route::post('/about/update/{id_about}', [App\Http\Controllers\Datamaster\AboutController::class, 'update'])->name('datamaster.updateabout');
    Route::get('/about/edit/{id_about}', [App\Http\Controllers\Datamaster\AboutController::class, 'edit'])->name('datamaster.editabout');
    Route::get('/about/{id_about}', [App\Http\Controllers\Datamaster\AboutController::class, 'destroy'])->name('datamaster.destroyabout');

//career
    Route::get('/career', [App\Http\Controllers\Datamaster\CareerController::class, 'index'])->name('datamaster.career');
	Route::get('/career/create', [App\Http\Controllers\Datamaster\CareerController::class, 'create'])->name('datamaster.createcareer');
    Route::post('/career/store', [App\Http\Controllers\Datamaster\CareerController::class, 'store'])->name('datamaster.storecareer');
    Route::post('/career/update/{id_career}', [App\Http\Controllers\Datamaster\CareerController::class, 'update'])->name('datamaster.updatecareer');
    Route::get('/career/edit/{id_career}', [App\Http\Controllers\Datamaster\CareerController::class, 'edit'])->name('datamaster.editcareer');
    Route::get('/career/{id_career}', [App\Http\Controllers\Datamaster\CareerController::class, 'destroy'])->name('datamaster.destroycareer');

//knowledge
    Route::get('/knowledge', [App\Http\Controllers\Datamaster\KnowledgeController::class, 'index'])->name('datamaster.knowledge');
    Route::get('/knowledge/create', [App\Http\Controllers\Datamaster\KnowledgeController::class, 'create'])->name('datamaster.createknowledge');
    Route::post('/knowledge/store', [App\Http\Controllers\Datamaster\KnowledgeController::class, 'store'])->name('datamaster.storeknowledge');
    Route::post('/knowledge/update/{id_knowledge}', [App\Http\Controllers\Datamaster\KnowledgeController::class, 'update'])->name('datamaster.updateknowledge');
    Route::get('/knowledge/edit/{id_knowledge}', [App\Http\Controllers\Datamaster\KnowledgeController::class, 'edit'])->name('datamaster.editknowledge');
    Route::get('/knowledge/{id_knowledge}', [App\Http\Controllers\Datamaster\KnowledgeController::class, 'destroy'])->name('datamaster.destroyknowledge');

//portofolio
    Route::get('/portofolio', [App\Http\Controllers\Datamaster\PortofolioController::class, 'index'])->name('datamaster.portofolio');
    Route::get('/portofolio/create', [App\Http\Controllers\Datamaster\PortofolioController::class, 'create'])->name('datamaster.createportofolio');
    Route::post('/portofolio/store', [App\Http\Controllers\Datamaster\PortofolioController::class, 'store'])->name('datamaster.storeportofolio');
    Route::post('/portofolio/update/{id_portofolio}', [App\Http\Controllers\Datamaster\PortofolioController::class, 'update'])->name('datamaster.updateportofolio');
    Route::get('/portofolio/edit/{id_portofolio}', [App\Http\Controllers\Datamaster\PortofolioController::class, 'edit'])->name('datamaster.editportofolio');
    Route::get('/portofolio/{id_portofolio}', [App\Http\Controllers\Datamaster\PortofolioController::class, 'destroy'])->name('datamaster.destroyportofolio');

//contact
    Route::get('/contact', [App\Http\Controllers\Datamaster\ContactController::class, 'index'])->name('datamaster.contact');
    Route::get('/contact/create', [App\Http\Controllers\Datamaster\ContactController::class, 'create'])->name('datamaster.createcontact');
    Route::post('/contact/store', [App\Http\Controllers\Datamaster\ContactController::class, 'store'])->name('datamaster.storecontact');
    Route::post('/contact/update/{id_contact}', [App\Http\Controllers\Datamaster\ContactController::class, 'update'])->name('datamaster.updatecontact');
    Route::get('/contact/edit/{id_contact}', [App\Http\Controllers\Datamaster\ContactController::class, 'edit'])->name('datamaster.editcontact');
    Route::get('/contact/{id_contact}', [App\Http\Controllers\Datamaster\ContactController::class, 'destroy'])->name('datamaster.destroycontact');

});

Route::prefix('website')->group(function(){
//agenda
    Route::get('/agenda', [App\Http\Controllers\Website\AgendaController::class, 'index'])->name('website.agenda');
    Route::get('/agenda/create', [App\Http\Controllers\Website\AgendaController::class, 'create'])->name('website.createagenda');
    Route::post('/agenda/store', [App\Http\Controllers\Website\AgendaController::class, 'store'])->name('website.storeagenda');
    Route::post('/agenda/update/{id_agenda}', [App\Http\Controllers\Website\AgendaController::class, 'update'])->name('website.updateagenda');
    Route::get('/agenda/edit/{id_agenda}', [App\Http\Controllers\Website\AgendaController::class, 'edit'])->name('website.editagenda');
    Route::get('/agenda/{id_agenda}', [App\Http\Controllers\Website\AgendaController::class, 'destroy'])->name('website.destroyagenda');

//download
    Route::get('/downloadfile', [App\Http\Controllers\Website\DownloadFileController::class, 'index'])->name('website.downloadfile');
    Route::get('/downloadfile/create', [App\Http\Controllers\Website\DownloadFileController::class, 'create'])->name('website.createdownloadfile');
    Route::post('/downloadfile/store', [App\Http\Controllers\Website\DownloadFileController::class, 'store'])->name('website.storedownloadfile');
    Route::post('/downloadfile/update/{id_download}', [App\Http\Controllers\Website\DownloadFileController::class, 'update'])->name('website.updatedownloadfile');
    Route::get('/downloadfile/edit/{id_download}', [App\Http\Controllers\Website\DownloadFileController::class, 'edit'])->name('website.editdownloadfile');
    Route::get('/downloadfile/{id_download}', [App\Http\Controllers\Website\DownloadFileController::class, 'destroy'])->name('website.destroydownloadfile');

//foto
    Route::get('/foto', [App\Http\Controllers\Website\FotoController::class, 'index'])->name('website.foto');
    Route::get('/foto/create', [App\Http\Controllers\Website\FotoController::class, 'create'])->name('website.createfoto');
    Route::post('/foto/store', [App\Http\Controllers\Website\FotoController::class, 'store'])->name('website.storefoto');
    Route::post('/foto/update/{id_foto}', [App\Http\Controllers\Website\FotoController::class, 'update'])->name('website.updatefoto');
    Route::get('/foto/edit/{id_foto}', [App\Http\Controllers\Website\FotoController::class, 'edit'])->name('website.editfoto');
    Route::get('/foto/{id_foto}', [App\Http\Controllers\Website\FotoController::class, 'destroy'])->name('website.destroyfoto');

//video
    Route::get('/video', [App\Http\Controllers\Website\VideoController::class, 'index'])->name('website.video');
    Route::get('/video/create', [App\Http\Controllers\Website\VideoController::class, 'create'])->name('website.createvideo');
    Route::post('/video/store', [App\Http\Controllers\Website\VideoController::class, 'store'])->name('website.storevideo');
    Route::post('/video/update/{id_video}', [App\Http\Controllers\Website\VideoController::class, 'update'])->name('website.updatevideo');
    Route::get('/video/edit/{id_video}', [App\Http\Controllers\Website\VideoController::class, 'edit'])->name('website.editvideo');
    Route::get('/video/{id_video}', [App\Http\Controllers\Website\VideoController::class, 'destroy'])->name('website.destroyvideo');

//newsletter
    Route::get('/newsletter', [App\Http\Controllers\Website\NewsletterController::class, 'index'])->name('website.newsletter');
    Route::get('/newsletter/create', [App\Http\Controllers\Website\NewsletterController::class, 'create'])->name('website.createnewsletter');
    Route::post('/newsletter/store', [App\Http\Controllers\Website\NewsletterController::class, 'store'])->name('website.storenewsletter');
    Route::post('/newsletter/update/{id_newsletter}', [App\Http\Controllers\Website\NewsletterController::class, 'update'])->name('website.updatenewsletter');
    Route::get('/newsletter/edit/{id_newsletter}', [App\Http\Controllers\Website\NewsletterController::class, 'edit'])->name('website.editnewsletter');
    Route::get('/newsletter/{id_newsletter}', [App\Http\Controllers\Website\NewsletterController::class, 'destroy'])->name('website.destroynewsletter');

//promosi
    Route::get('/promosi', [App\Http\Controllers\Website\PromosiController::class, 'index'])->name('website.promosi');
    Route::get('/promosi/create', [App\Http\Controllers\Website\PromosiController::class, 'create'])->name('website.createpromosi');
    Route::post('/promosi/store', [App\Http\Controllers\Website\PromosiController::class, 'store'])->name('website.storepromosi');
    Route::post('/promosi/update/{id_promosi}', [App\Http\Controllers\Website\PromosiController::class, 'update'])->name('website.updatepromosi');
    Route::get('/promosi/edit/{id_promosi}', [App\Http\Controllers\Website\PromosiController::class, 'edit'])->name('website.editpromosi');
    Route::get('/promosi/{id_promosi}', [App\Http\Controllers\Website\PromosiController::class, 'destroy'])->name('website.destroypromosi');

});

Route::prefix('administration')->group(function(){
//kategori
    Route::get('/kategori', [App\Http\Controllers\Administration\KategoriController::class, 'index'])->name('administration.kategori');
    Route::get('/kategori/create', [App\Http\Controllers\Administration\KategoriController::class, 'create'])->name('administration.createkategori');
    Route::post('/kategori/store', [App\Http\Controllers\Administration\KategoriController::class, 'store'])->name('administration.storekategori');
    Route::post('/kategori/update/{id_kategori}', [App\Http\Controllers\Administration\KategoriController::class, 'update'])->name('administration.updatekategori');
    Route::get('/kategori/edit/{id_kategori}', [App\Http\Controllers\Administration\KategoriController::class, 'edit'])->name('administration.editkategori');
    Route::get('/kategori/{id_kategori}', [App\Http\Controllers\Administration\KategoriController::class, 'destroy'])->name('administration.destroykategori');

//tags
    Route::get('/tags', [App\Http\Controllers\Administration\TagsController::class, 'index'])->name('administration.tags');
    Route::get('/tags/create', [App\Http\Controllers\Administration\TagsController::class, 'create'])->name('administration.createtags');
    Route::post('/tags/store', [App\Http\Controllers\Administration\TagsController::class, 'store'])->name('administration.storetags');
    Route::post('/tags/update/{id_tags}', [App\Http\Controllers\Administration\TagsController::class, 'update'])->name('administration.updatetags');
    Route::get('/tags/edit/{id_tags}', [App\Http\Controllers\Administration\TagsController::class, 'edit'])->name('administration.edittags');
    Route::get('/tags/{id_tags}', [App\Http\Controllers\Administration\TagsController::class, 'destroy'])->name('administration.destroytags');

});

Route::prefix('management')->group(function(){
//public
    Route::get('/public', [App\Http\Controllers\Management\PublicController::class, 'index'])->name('management.public');
    Route::get('/public/create', [App\Http\Controllers\Management\PublicController::class, 'create'])->name('management.createpublic');
    Route::post('/public/store', [App\Http\Controllers\Management\PublicController::class, 'store'])->name('management.storepublic');
    Route::post('/public/update/{id_public}', [App\Http\Controllers\Management\PublicController::class, 'update'])->name('management.updatepublic');
    Route::get('/public/edit/{id_public}', [App\Http\Controllers\Management\PublicController::class, 'edit'])->name('management.editpublic');
    Route::get('/public/{id_public}', [App\Http\Controllers\Management\PublicController::class, 'destroy'])->name('management.destroypublic');

//admin
    Route::get('/admin', [App\Http\Controllers\Management\AdminController::class, 'index'])->name('management.admin');
    Route::get('/admin/create', [App\Http\Controllers\Management\AdminController::class, 'create'])->name('management.createadmin');
    Route::post('/admin/store', [App\Http\Controllers\Management\AdminController::class, 'store'])->name('management.storeadmin');
    Route::post('/admin/update/{id_admin}', [App\Http\Controllers\Management\AdminController::class, 'update'])->name('management.updateadmin');
    Route::get('/admin/edit/{id_admin}', [App\Http\Controllers\Management\AdminController::class, 'edit'])->name('management.editadmin');
    Route::get('/admin/{id_admin}', [App\Http\Controllers\Management\AdminController::class, 'destroy'])->name('management.destroyadmin');

    Route::get('/submenuadmin', [App\Http\Controllers\Management\SubmenuAdminController::class, 'index'])->name('management.subadmin');
    Route::post('/submenuadmin/store', [App\Http\Controllers\Management\SubmenuAdminController::class, 'store'])->name('management.storesubadmin');
    Route::post('/submenuadmin/update/{id}', [App\Http\Controllers\Management\SubmenuAdminController::class, 'update'])->name('management.updatesubadmin');
    Route::get('/submenuadmin/{id}', [App\Http\Controllers\Management\SubmenuAdminController::class, 'destroy'])->name('management.destroysubadmin');

//authority
    Route::get('/authority', [App\Http\Controllers\Management\AuthorityController::class, 'index'])->name('management.authority');
    Route::get('/authority/create', [App\Http\Controllers\Management\AuthorityController::class, 'create'])->name('management.createauthority');
    Route::post('/authority/store', [App\Http\Controllers\Management\AuthorityController::class, 'store'])->name('management.storeauthority');
    Route::post('/authority/update/{id_authority}', [App\Http\Controllers\Management\AuthorityController::class, 'update'])->name('management.updateauthority');
    Route::get('/authority/edit/{id_authority}', [App\Http\Controllers\Management\AuthorityController::class, 'edit'])->name('management.editauthority');
    Route::get('/authority/{id_authority}', [App\Http\Controllers\Management\AuthorityController::class, 'destroy'])->name('management.destroyauthority');

//user
    Route::get('/usermanagement', [App\Http\Controllers\Management\UserManagementController::class, 'index'])->name('management.usermanagement');
    Route::get('/usermanagement/create', [App\Http\Controllers\Management\UserManagementController::class, 'create'])->name('management.createusermanagement');
    Route::post('/usermanagement/store', [App\Http\Controllers\Management\UserManagementController::class, 'store'])->name('management.storeusermanagement');
    Route::post('/usermanagement/update/{id_user_management}', [App\Http\Controllers\Management\UserManagementController::class, 'update'])->name('management.updateusermanagement');
    Route::get('/usermanagement/edit/{id_user_management}', [App\Http\Controllers\Management\UserManagementController::class, 'edit'])->name('management.editusermanagement');
    Route::get('/usermanagement/{id_user_management}', [App\Http\Controllers\Management\UserManagementController::class, 'destroy'])->name('management.destroyusermanagement');

//role
    Route::get('/role', [App\Http\Controllers\Management\RoleController::class, 'index'])->name('management.role');
    Route::get('/role/create', [App\Http\Controllers\Management\RoleController::class, 'create'])->name('management.createrole');
    Route::post('/role/store', [App\Http\Controllers\Management\RoleController::class, 'store'])->name('management.storerole');
    Route::post('/role/update/{id_role}', [App\Http\Controllers\Management\RoleController::class, 'update'])->name('management.updaterole');
    Route::get('/role/edit/{id_role}', [App\Http\Controllers\Management\RoleController::class, 'edit'])->name('management.editrole');
    Route::get('/role/{id_role}', [App\Http\Controllers\Management\RoleController::class, 'destroy'])->name('management.destroyrole');

});

Route::prefix('backup')->group(function(){
//logpublic
    Route::get('/logpublic', [App\Http\Controllers\Backup\LogPublicController::class, 'index'])->name('backup.logpublic');
    Route::get('/logpublic/create', [App\Http\Controllers\Backup\LogPublicController::class, 'create'])->name('backup.createlogpublic');
    Route::post('/logpublic/store', [App\Http\Controllers\Backup\LogPublicController::class, 'store'])->name('backup.storelogpublic');
    Route::post('/logpublic/update/{id_log_public}', [App\Http\Controllers\Backup\LogPublicController::class, 'update'])->name('backup.updatelogpublic');
    Route::get('/logpublic/edit/{id_log_public}', [App\Http\Controllers\Backup\LogPublicController::class, 'edit'])->name('backup.editlogpublic');
    Route::get('/logpublic/{id_log_public}', [App\Http\Controllers\Backup\LogPublicController::class, 'destroy'])->name('backup.destroylogpublic');

//logadmin
    Route::get('/logadmin', [App\Http\Controllers\Backup\LogAdminController::class, 'index'])->name('backup.logadmin');
    Route::get('/logadmin/create', [App\Http\Controllers\Backup\LogAdminController::class, 'create'])->name('backup.createlogadmin');
    Route::post('/logadmin/store', [App\Http\Controllers\Backup\LogAdminController::class, 'store'])->name('backup.storelogadmin');
    Route::post('/logadmin/update/{id_log_admin}', [App\Http\Controllers\Backup\LogAdminController::class, 'update'])->name('backup.updatelogadmin');
    Route::get('/logadmin/edit/{id_log_admin}', [App\Http\Controllers\Backup\LogAdminController::class, 'edit'])->name('backup.editlogadmin');
    Route::get('/logadmin/{id_log_admin}', [App\Http\Controllers\Backup\LogAdminController::class, 'destroy'])->name('backup.destroylogadmin');

//backup
    Route::get('/backup', [App\Http\Controllers\Backup\BackupController::class, 'index'])->name('backup.backup');
    Route::get('/backup/create', [App\Http\Controllers\Backup\BackupController::class, 'create'])->name('backup.createbackup');
    Route::post('/backup/store', [App\Http\Controllers\Backup\BackupController::class, 'store'])->name('backup.storebackup');
    Route::post('/backup/update/{id_backup}', [App\Http\Controllers\Backup\BackupController::class, 'update'])->name('backup.updatebackup');
    Route::get('/backup/edit/{id_backup}', [App\Http\Controllers\Backup\BackupController::class, 'edit'])->name('backup.editbackup');
    Route::get('/backup/{id_backup}', [App\Http\Controllers\Backup\BackupController::class, 'destroy'])->name('backup.destroybackup');


});


//appconfig
    Route::get('/appconfig', [App\Http\Controllers\AppConfigController::class, 'index'])->name('appconfig');
    Route::get('/appconfig/create', [App\Http\Controllers\AppConfigController::class, 'create'])->name('createappconfig');
    Route::post('/appconfig/store', [App\Http\Controllers\AppConfigController::class, 'store'])->name('storeappconfig');
    Route::post('/appconfig/update/{id_app_config}', [App\Http\Controllers\AppConfigController::class, 'update'])->name('updateappconfig');
    Route::get('/appconfig/edit/{id_app_config}', [App\Http\Controllers\AppConfigController::class, 'edit'])->name('editappconfig');
    Route::get('/appconfig/{id_app_config}', [App\Http\Controllers\AppConfigController::class, 'destroy'])->name('destroyappconfig');

Route::prefix('logo')->group(function(){
//logoadmin
    Route::get('/logoadmin', [App\Http\Controllers\Logo\LogoAdminController::class, 'index'])->name('logoadmin');
    Route::get('/logoadmin/create', [App\Http\Controllers\Logo\LogoAdminController::class, 'create'])->name('createlogoadmin');
    Route::post('/logoadmin/update/{id_logo_admin}', [App\Http\Controllers\Logo\LogoAdminController::class, 'update'])->name('updatelogoadmin');
    Route::get('/logoadmin/edit/{id_logo_admin}', [App\Http\Controllers\Logo\LogoAdminController::class, 'edit'])->name('editlogoadmin');
    Route::get('/logoadmin/{id_logo_admin}', [App\Http\Controllers\Logo\LogoAdminController::class, 'destroy'])->name('destroylogoadmin');

//logouser
    Route::get('/logouser', [App\Http\Controllers\Logo\LogoUserController::class, 'index'])->name('logouser');
    Route::get('/logouser/create', [App\Http\Controllers\Logo\LogoUserController::class, 'create'])->name('createlogouser');
    Route::post('/logouser/store', [App\Http\Controllers\Logo\LogoUserController::class, 'store'])->name('storelogouser');
    Route::post('/logouser/update/{id_logo_user}', [App\Http\Controllers\Logo\LogoUserController::class, 'update'])->name('updatelogouser');
    Route::get('/logouser/edit/{id_logo_user}', [App\Http\Controllers\Logo\LogoUserController::class, 'edit'])->name('editlogouser');
    Route::get('/logouser/{id_logo_user}', [App\Http\Controllers\Logo\LogoUserController::class, 'destroy'])->name('destroylogouser');

});


//User
Route::get('home', [App\Http\Controllers\Users\HomeusersController::class, 'index'])->name('home.users');
Route::get('about', [App\Http\Controllers\Users\AboutusersController::class, 'index'])->name('about.users');
Route::get('berita1', [App\Http\Controllers\Users\Berita1usersController::class, 'index'])->name('berita1.users');
Route::get('berita2', [App\Http\Controllers\Users\Berita2usersController::class, 'index'])->name('berita2.users');
Route::get('berita3', [App\Http\Controllers\Users\Berita3usersController::class, 'index'])->name('berita3.users');
Route::get('berita4', [App\Http\Controllers\Users\Berita4usersController::class, 'index'])->name('berita4.users');
Route::get('career', [App\Http\Controllers\Users\CareerusersController::class, 'index'])->name('career.users');
Route::get('contact', [App\Http\Controllers\Users\ContactusersController::class, 'index'])->name('contact.users');
Route::get('/', [App\Http\Controllers\Users\IndexusersController::class, 'index'])->name('index.users');
Route::get('knowledge', [App\Http\Controllers\Users\KnowledgeusersController::class, 'index'])->name('knowledge.users');
Route::get('/knowledge/{id}', [App\Http\Controllers\Users\KnowledgeusersController::class, 'show'])->name('knowledge.users.show');
Route::get('portofolio', [App\Http\Controllers\Users\PortofoliousersController::class, 'index'])->name('portofolio.users');
Route::get('service', [App\Http\Controllers\Users\ServiceusersController::class, 'index'])->name('service.users');