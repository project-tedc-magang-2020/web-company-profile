-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 10, 2022 at 01:43 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_app_compro_crop`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_about`
--

CREATE TABLE `tb_about` (
  `id_about` varchar(20) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `gambar_client` varchar(255) NOT NULL,
  `gambar_project` varchar(255) NOT NULL,
  `konten` longtext NOT NULL,
  `nama_client` varchar(50) NOT NULL,
  `alamat_client` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_about`
--

INSERT INTO `tb_about` (`id_about`, `gambar`, `gambar_client`, `gambar_project`, `konten`, `nama_client`, `alamat_client`) VALUES
('AB-00001', '62f26e35c222doffice.png', '62f26e35c26c304.png', '62f26e35c2a1301.png', 'Menciptakan layanan pengembangan IT yang kreatif untuk kepentingan pelanggan, internal perusahaan, UMKM dan usaha lainnya. Yang mengembangkan layanan IT yang berlandaskan pada pemanfaatan teknologi informasi yang bernilai tinggi untuk menjadi perusahaan pengembangan IT terbsesar dan terbaik di Indonesia', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(6) NOT NULL,
  `nama_menu` varchar(225) DEFAULT NULL,
  `link` varchar(225) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `icon` varchar(225) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `sub_menu` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `nama_menu`, `link`, `keterangan`, `icon`, `status`, `sub_menu`) VALUES
(1, 'Dashboard', '/admin/home', 'Dashboard', 'fas fa-home', 'enabled', 0),
(2, 'Company Profile', '/admin/company_profile', 'Company Profile', 'fas fa-briefcase', 'enabled', 5),
(3, 'Data Website', '/admin/data_website', 'Data Website', 'fas fa-desktop', 'enabled', 5),
(4, 'Data Administration', '/administration/tags', 'Tags', 'fas fa-laptop', 'enabled', 2),
(5, 'Access Management', '/admin/access_management', 'Access Management', 'fas fa-users', 'enabled', 5),
(6, 'Backup and Log', '/admin/backup', 'Backup and Log', 'fas fa-database', 'enabled', 3),
(7, 'App Configuration', '/appconfig', 'App Configuration', 'fa fa-tag', 'enabled', 0),
(8, 'Logo', '/admin/logo', 'Logo', 'fa fa-file', 'enabled', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_agenda`
--

CREATE TABLE `tb_agenda` (
  `id_agenda` varchar(100) NOT NULL,
  `nama_agenda` varchar(100) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `waktu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_agenda`
--

INSERT INTO `tb_agenda` (`id_agenda`, `nama_agenda`, `tempat`, `waktu`) VALUES
('AG-00001', 'Rapat Dengan Client', 'Aula', '17:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_config`
--

CREATE TABLE `tb_app_config` (
  `id_app_config` int(11) NOT NULL,
  `nama_website` varchar(500) NOT NULL,
  `inisial` varchar(500) NOT NULL,
  `deskripsi` varchar(500) NOT NULL,
  `keywoard` varchar(500) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `no_telp` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `facebook` varchar(500) NOT NULL,
  `instagram` varchar(500) NOT NULL,
  `twitter` varchar(500) NOT NULL,
  `google_plus` varchar(500) NOT NULL,
  `youtube` varchar(500) NOT NULL,
  `maps` varchar(500) NOT NULL,
  `favicon` varchar(500) NOT NULL,
  `logo_user` varchar(500) NOT NULL,
  `logo_admin` varchar(500) NOT NULL,
  `background` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_app_config`
--

INSERT INTO `tb_app_config` (`id_app_config`, `nama_website`, `inisial`, `deskripsi`, `keywoard`, `alamat`, `no_telp`, `email`, `facebook`, `instagram`, `twitter`, `google_plus`, `youtube`, `maps`, `favicon`, `logo_user`, `logo_admin`, `background`) VALUES
(1, 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'Tes', 'logo.png', 'logo.png', 'logo.png', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE `tb_artikel` (
  `id_artikel` varchar(100) NOT NULL,
  `gambar` mediumblob NOT NULL,
  `Judul` varchar(200) NOT NULL,
  `Isi` longtext NOT NULL,
  `Kategori` varchar(100) NOT NULL,
  `Dibuat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_authority`
--

CREATE TABLE `tb_authority` (
  `id_authority` varchar(225) NOT NULL,
  `nama_role` varchar(225) DEFAULT NULL,
  `type` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_backup`
--

CREATE TABLE `tb_backup` (
  `id_backup` varchar(225) NOT NULL,
  `user` varchar(225) DEFAULT NULL,
  `activity` varchar(225) DEFAULT NULL,
  `waktu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_backup`
--

INSERT INTO `tb_backup` (`id_backup`, `user`, `activity`, `waktu`) VALUES
('BK-00001', 'Savira Yosita', 'Backup', '10:57:11');

-- --------------------------------------------------------

--
-- Table structure for table `tb_career`
--

CREATE TABLE `tb_career` (
  `id_career` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `gambar` mediumblob NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_career`
--

INSERT INTO `tb_career` (`id_career`, `judul`, `gambar`, `deskripsi`) VALUES
(1, 'Mobile Development', 0x3632663266623962663265306630312e706e67, 'Mobile'),
(2, 'Backend Developer', 0x3632663266633965633336643130362e706e67, 'Backend'),
(3, 'Frontend Developer', 0x3632663266636336333165303030392e706e67, 'Frontend'),
(4, 'Desain Ui-Ux', 0x3632663266636639303633343330332e706e67, 'Desain'),
(5, 'Deskripsi', 0x363266333030326336653662396361726565722e6a7067, 'Mulai Kariermu Bersama Kami!!!\r\nDi PT. Crop Inspirasi Digital, kami merangkul dan mengembangkan para talenta muda. Namun, kami juga membutuhkan individu yang memiliki jiwa kepemimpinan dan tentunya berpengalaman. Mulai perjalanan kariermu bersama kami!');

-- --------------------------------------------------------

--
-- Table structure for table `tb_contact`
--

CREATE TABLE `tb_contact` (
  `id_contact` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `pesan` longtext NOT NULL,
  `waktu` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_contact`
--

INSERT INTO `tb_contact` (`id_contact`, `nama`, `email`, `subject`, `pesan`, `waktu`) VALUES
('1', 'Savira', 'savira@gmail.com', '....', '...', NULL),
('2', 'Zafar', 'admin@gmail.com', '....', '......', NULL),
('4', 'tes', 'test@mail.com', 'tes', 'tse', '12:44:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_download_file`
--

CREATE TABLE `tb_download_file` (
  `id_download` varchar(50) NOT NULL,
  `nama_file` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_download_file`
--

INSERT INTO `tb_download_file` (`id_download`, `nama_file`) VALUES
('DF-00001', 'Rapat');

-- --------------------------------------------------------

--
-- Table structure for table `tb_foto`
--

CREATE TABLE `tb_foto` (
  `id_foto` varchar(100) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `gambar` mediumblob NOT NULL,
  `tanggal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_foto`
--

INSERT INTO `tb_foto` (`id_foto`, `nama_kegiatan`, `gambar`, `tanggal`) VALUES
('F-00001', 'Rapat', 0x36326234353133323037383464766973692e706e67, '22/06/03'),
('F-00001', 'Rapat', 0x36326265353831626366363434313132303631395f627573696e6573736d616e5f636c69656e745f6d616e5f6d616e616765725f706572736f6e5f69636f6e2e706e67, '22/06/03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` varchar(255) NOT NULL,
  `nama_kategori` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`, `type`, `keterangan`) VALUES
('MK-00001', 'Web Development', 'Portofolio', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_knowledge`
--

CREATE TABLE `tb_knowledge` (
  `id` int(11) NOT NULL,
  `id_knowledge` varchar(20) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `tanggal_berita` varchar(30) NOT NULL,
  `judul_berita` longtext NOT NULL,
  `isi_konten` longtext NOT NULL,
  `deskripsi` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_knowledge`
--

INSERT INTO `tb_knowledge` (`id`, `id_knowledge`, `gambar`, `tanggal_berita`, `judul_berita`, `isi_konten`, `deskripsi`) VALUES
(2, 'KN-00001', '62f27b0bc23f4berita1.png', '22/07/08', 'Platform Metaverse Buatan Anak Negeri Segera Dirilis, Bisa Apa Aja?', 'Jakarta - Platform social metaverse dari Indonesia Jagat siap menghadirkan dunia virtual dan pengalaman interaksi sosial yang imersif. Perusahaan yang menaungi Jagat, PT Avatara Jagat Nusantara, menargetkan kaum muda sebagai sasaran utamanya. Perusahaan yang didirikan pada Desember 2021 ini akan segera meluncurkan versi alpha dari produk perdananya pada pertengahan tahun ini.\r\nSebagai informasi, metaverse merupakan konsep dunia virtual yang bersifat tanpa batas dan saling terhubung antara satu komunitas virtual dengan lainnya. Riset dari Populix pada Juni 2022 juga mencatat sebanyak 44% masyarakat Indonesia melihat metaverse sebagai ekstensi platform sosial media yang menyediakan berbagai cara baru berkomunikasi secara bebas.\r\n\r\nCo-Founder Jagat Barry Beagen menilai metaverse dan Web3 akan menjadi masa depan internet dan interaksi sosial di Indonesia. Untuk itu, Jagat sebagai perpanjangan media sosial akan menjadi wadah tempat berkreasi dan hangout favorit bagi masyarakat Indonesia.\r\n\r\n\"Sebagai negara dengan masyarakat yang aktif bersosial media dan memiliki rasa gotong royong yang tinggi, kami melihat metaverse dan Web3 menjadi masa depan internet dan interaksi sosial di Indonesia saat ini. Sebagai perpanjangan media sosial, Jagat fokus untuk menjadi tempat berkreasi dan hangout favorit bagi masyarakat Indonesia, khususnya kaum muda produktif dan digital savvy untuk mengerahkan potensi mereka,\" ujarnya dalam keterangan tertulis, Jumat (8/7/2022).\r\n\r\nIa menambahkan melalui ekosistem kreator baru Jagat, semua orang dapat berkarya, terhubung dengan teman hingga memperoleh manfaat ekonomi dari hal tersebut.\r\n\"Tentu saja tidak ada yang dapat menggantikan interaksi dunia nyata, namun dengan kemampuan avatar berbasis video 3D, kami dapat meningkatkan empati antar orang selama bekerja dan berinteraksi secara remote, sangat cocok untuk diterapkan di Indonesia sebagai negara kepulauan yang secara geografis sangat luas. Di dalam ekosistem kreator baru Jagat, semua orang dapat menciptakan dunia baru, memiliki karya mereka sendiri, terhubung dengan teman, dan menghasilkan manfaat ekonomi dari hal tersebut,\" terang Barry.\r\n\r\nJagat akan tersedia baik di web maupun aplikasi mobile tanpa membutuhkan dukungan perangkat keras tambahan seperti Virtual Reality (VR) atau Augmented Reality (AR). Sebagai platform berbasis user-generated-content (UGC), Jagat juga memungkinkan pengguna untuk merancang dan bahkan menggunakan berbagai aplikasi dalam platform untuk berinteraksi dengan teman baru, rekan kerja, hingga fans mereka.\r\n\r\nSelain itu, baik masyarakat maupun brand nantinya dapat membuat avatar dan aset digital seperti fesyen, furnitur dan aksesoris lain dalam bentuk Non Fungible Token (NFT) secara gratis dan mudah, tanpa perlu memiliki pengetahuan teknis atau tentang cryptocurrency sekalipun.\r\n\"Untuk pertama kali kami mendorong konsep hybrid atau keterkaitan secara nyata antara metaverse dengan pembangunan fisik pada kota tinggal yang menjadi partner kami nanti. Dalam membangun platform bersama layaknya kota digital, kami mendorong nilai strategis dari metaverse ini dalam hal kepemilikan ekonomi dan sosial yang nyata,\" tambah Barry. Melalui teknologi blockchain, tutur Barry, Jagat dapat mendorong terbentuknya komunitas yang bisa menciptakan dan memperoleh manfaat ekonomi melalui berbagai bisnis model X-to-earn dengan potensi tak terbatas. Jagat juga berkomitmen untuk menghadirkan pengalaman Web3 yang sesuai dan terjangkau baik secara teknologi maupun finansial bagi masyarakat secara umum.\r\n\r\n\"Ketika berbicara mengenai metaverse, pandangan masyarakat di Indonesia saat ini masih skeptis, dengan fokus kepada tantangan akses internet yang belum merata, jargon yang tidak mudah dipahami, dan akses perangkat keras yang belum terjangkau. Melihat bahwa banyaknya orang yang baru-baru ini terhubung pertama kali dengan internet dan berbagai macam layanan seperti sosial media, uang elektronik, dan gim melalui alat mobile, kami akan hadir dengan strategi mobile-first yang dapat mengakomodasi smartphone yang sederhana sekalipun,\" paparnya.\r\n\"Sehingga, diharapkan pendekatan ini akan membuka akses pada manfaat sosial dan ekonomi yang ditawarkan oleh metaverse, bagi seluruh lapisan masyarakat, baik di Indonesia maupun dunia,\" sambung Barry.\r\n\r\n\r\nBerkolaborasi dengan Raksasa Teknologi Dunia\r\n\r\nJagat juga menggandeng perusahaan teknologi kelas dunia seperti Advance Intelligence Group, perusahaan kecerdasan buatan (AI) di Asia Tenggara serta platform metaverse asal Singapura, Utown.\r\nAdvance Intelligence Group terdiri dari platform buy now, pay later terbesar di Asia Atome, perusahaan AI terdepan ADVANCE.AI, dan layanan merchant e-commerce regional Ginee. Sementara Utown adalah perusahaan metaverse yang telah mengembangkan aplikasi sosial dengan lebih dari 90 juta pengguna, dan ingin mendorong lebih jauh ke dalam konsep metaverse dan menciptakan platform dengan kapabilitas dan kepraktisan yang sesungguhnya.\r\n\"Kami melihat bahwa Jagat menaruh perhatian yang mendalam agar Web3 dapat dipahami dan diakses oleh lebih banyak pihak, terutama anak-anak muda kreatif di Indonesia. Dengan kekuatan Barry dan tim dalam isu-isu kota urban dan community building, kami percaya bahwa Jagat dapat membangun ekonomi baru bagi semua pihak, dimana potensi Web3 dapat dimaksimalkan bagi seluruh pengguna dengan membuat, memiliki hingga memonetisasi berbagai tools yang disiapkan bagi kreator,\" tutur Co-founder, Chairman, dan Chief Executive Officer Advance Intelligence Group Jefferson Chen.\r\n\r\n\"Dengan pengetahuan mendalam dan ekosistem produk dan layanan kami di Indonesia dan global, Advance Intelligence Group akan mengkolaborasikan pengetahuan, pengalaman, dan sumber daya dalam aspek fintech, e-commerce, ritel, pembayaran, teknologi, dan kecerdasan buatan (AI) untuk dapat mengembangkan generasi internet berikutnya melalui Jagat di Indonesia,\" ujarnya lagi.\r\nSementara itu, Co-founder Utown Zach Loy juga mengatakan pihaknya siap memberikan pengalaman dan solusi interaktif termutakhir untuk Jagat sambil menawarkan interaksi kepada pengguna Jagat dalam membangun dan mengembangkan koneksinya.\r\n\r\n\"Membangun ekosistem metaverse tentu membutuhkan penerapan teknologi yang beragam, tetapi Jagat ingin melakukan lebih dari itu. Sebagai elemen yang tidak dapat terpisahkan dari kehidupan bermasyarakat, bersosialisasi merupakan bagian penting dalam dunia metaverse. Tanpa itu, metaverse akan menjadi sebuah kekosongan. Dengan pengalaman bertahun-tahun dalam pengembangan dan pengoperasian aplikasi sosial, Utown akan memberikan pengalaman dan solusi interaktif termutakhir untuk Jagat, menawarkan interaksi yang mendalam kepada pengguna Jagat sambil memungkinkan mereka untuk membangun dan mengembangkan koneksi sosialnya di Jagat,\" ungkapnya.\r\n\"Kami percaya bahwa hubungan sosial yang dibangun pengguna di Jagat akan menjadi perpanjangan dari hubungan yang sudah mereka miliki di kehidupan nyata dan di Internet. Menurut kami, Jagat memiliki potensi besar untuk menjadi produk ikonik di era metaverse,\" sambung Zach.\r\nDengan memanfaatkan potensi dunia virtual tanpa batas, metaverse dari Jagat dapat menjadi wajah Indonesia di komunitas global. Hal ini senada dengan pernyataan Koordinator Asistensi dan Kemitraan Presidensi G20 Indonesia Wishnutama Kusubandio.\r\n\"Sebagai masa depan dalam digital di Indonesia, metaverse lokal adalah sebuah keniscayaan bagi Indonesia, yang juga negara dengan ekonomi digital terbesar di Asia Tenggara, agar dapat bersaing dengan negara-negara lain di kawasan tersebut bahkan bersaing secara global. Hal ini selaras dengan arahan Bapak Presiden yang telah mengemukakan pentingnya Negara kita menyiapkan sebuah terobosan agar kita dapat unggul dari negara-negara lain dalam era digital ini,\" ungkat Wishnutama.\r\n\"Dengan seluruh mata dunia tertuju pada Indonesia pada puncak perhelatan akbar Konferensi Tingkat Tinggi G20 di Indonesia di bulan Oktober 2022 nanti, kita harus siap membawa potensi masa depan itu hadir sekarang dan melakukan akselerasi transformasi digital Indonesia dengan metaverse karya anak muda Indonesia melalui Jagat,\" pungkasnya.', 'Jakarta - Platform social metaverse dari Indonesia Jagat siap menghadirkan dunia virtual dan pengalaman interaksi sosial yang imersif.'),
(3, 'KN-00002', '62f286c7571c5berita2.jpeg', '22/07/07', '4 Skill Pekerjaan Yang Harus Dimiliki Pada Era Metaverse, Kamu Sudah Punya?', 'Jakarta - Konsep metaverse adalah dunia virtual yang menjadi inovasi untuk masa depan. Metaverse diciptakan oleh manusia untuk berinteraksi dan melakukan berbagai aktivitas. Hal penting dalam menghadapi era metaverse dapat dilihat dari tiga elemen yaitu ketersediaan hardware, software atau infrastruktur yang telah terbangun, dan sumber daya manusia yang berperan di dalamnya.\r\n\r\nBagi kamu yang ingin terjun ke dunia metaverse, setidaknya ada beberapa skill yang harus dimiliki oleh sumber daya manusianya. Berikut ini rangkumannya dikutip dari laman resmi Binus University.\r\n\r\n4 Skill Pekerjaan yang Harus Dimiliki pada Era Metaverse:\r\n\r\n1. Memahami Peran Dr. Indrawan Nugroho, CEO dan Co-Founder CIAS (Corporate Innovation Asia), mengatakan bahwa peran yang diambil harus jelas sebelum terjun ke dunia metaverse.\r\n\r\nMisal apakah ingin membangun device, mempelajari model 3D, dan lainnya.\r\n\"Kalau Anda ingin mengambil peran sebagai orang yang membangun device-nya, cari program studi yang memungkinkan Anda berkontribusi di situ. Jika Anda ingin membangun dunia metaverse, mulai dari blockchain, NFT, cryptocurrency, atau world building, Anda harus mempelajari 3D modelling,\" ungkapnya dikutip dari laman Binus, Kamis (7/7/2022).\r\n\r\nDr. Indrawan Nugroho mengatakan, dengan mengetahui peran yang diambil, seseorang akan lebih mudah untuk mempersiapkan diri memasuki dunia metaverse.\r\n\r\nPersiapan tersebut dapat dilakukan dengan memasuki program studi yang relevan. Nantinya, pengetahuan yang diperoleh dari bangku kuliah akan menjadi bekal yang sangat penting dalam membangun metaverse.\r\n\r\n\r\n2. Literasi Teknologi\r\n\r\nSementara itu, Chief Metaverse Officer WIR Group, Stephen Ng, MIM. MITM. MIR, mengatakan bahwa kunci dari memasuki dunia metaverse adalah technology literacy.\r\n\r\n\"Apa pun passion kita, technology literacy harus dibangun dari awal. Apabila Anda seorang sarjana hukum dan ingin masuk ke metaverse, masalah kepemilikan di metaverse sangat tricky. Seorang dokter harus tahu bagaimana cara menggabungkan offline-online di metaverse,\" terangnya.\r\n\r\nMenurut Stephen, siapa pun yang ingin masuk ke dunia metaverse harus memiliki skill untuk membangun maupun memanfaatkan ilmu yang dimilikinya di dunia metaverse.\r\n\r\n\"Bagi yang membangun, mereka bisa memulainya dari jurusan computing, design, dan sebagainya. Bagi pihak yang memanfaatkan, seluruh bidang ilmu bisa terlibat asal memiliki technology literacy,\" tambahnya.\r\n\r\n\r\n3. Adaptasi dengan Berbagai Tantangan\r\n\r\nKemampuan lain yang harus dimiliki di dunia metaverse adalah kemampuan untuk beradaptasi dengan keadaan. Hal ini karena sebagai dunia baru, metaverse menghadirkan berbagai tantangan yang harus dihadapi.\r\n\r\nApabila perusahaan ingin terlibat dan sukses di dalamnya, maka kemampuan beradaptasi sangat penting. Tujuannya agar perusahaan dapat tetap relevan dengan kebutuhan pelanggan.\r\n\r\n\r\n4. Berani Inovasi\r\n\r\nSkill terakhir yang penting untuk SDM adalah berani untuk berinovasi. Keberanian ini bukan hanya sebagai pengguna di dunia metaverse, tetapi hadir sebagai creator.\r\n\r\nDengan cara ini, seseorang tidak hanya berkontribusi dalam metaverse, namun bisa memperoleh penghasilan.\r\n\r\nJadi, itulah beberapa skill yang dibutuhkan pekerja pada era metaverse. Apakah detikers punya skill itu?', 'Jakarta - Konsep metaverse adalah dunia virtual yang menjadi inovasi untuk masa depan.Metaverse diciptakan oleh manusia untuk berinteraksi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_logo_admin`
--

CREATE TABLE `tb_logo_admin` (
  `id_logo_admin` varchar(50) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_logo_admin`
--

INSERT INTO `tb_logo_admin` (`id_logo_admin`, `logo`) VALUES
('L-00001', '62bd8df5c6507crop.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_logo_user`
--

CREATE TABLE `tb_logo_user` (
  `id_logo_user` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_logo_user`
--

INSERT INTO `tb_logo_user` (`id_logo_user`, `logo`) VALUES
('LU-00001', '62bd881603cd3misi1.png'),
('LU-00002', '62bd88e2bd0c7blog1.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_log_admin`
--

CREATE TABLE `tb_log_admin` (
  `id_log_admin` varchar(225) NOT NULL,
  `user` varchar(225) DEFAULT NULL,
  `waktu` varchar(100) NOT NULL,
  `keterangan` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_log_admin`
--

INSERT INTO `tb_log_admin` (`id_log_admin`, `user`, `waktu`, `keterangan`) VALUES
('1', '-', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_log_public`
--

CREATE TABLE `tb_log_public` (
  `id_log_public` varchar(225) NOT NULL,
  `ip_address` varchar(225) DEFAULT NULL,
  `browser` varchar(225) DEFAULT NULL,
  `os` varchar(225) DEFAULT NULL,
  `waktu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_log_public`
--

INSERT INTO `tb_log_public` (`id_log_public`, `ip_address`, `browser`, `os`, `waktu`) VALUES
('-', '-', '-', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id` int(11) NOT NULL,
  `nama_menu` varchar(255) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `create` varchar(255) DEFAULT NULL,
  `edit` varchar(255) DEFAULT NULL,
  `delete` varchar(255) DEFAULT NULL,
  `export` varchar(255) DEFAULT NULL,
  `duplicate` varchar(255) DEFAULT NULL,
  `unlock` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_menu`
--

INSERT INTO `tb_menu` (`id`, `nama_menu`, `view`, `create`, `edit`, `delete`, `export`, `duplicate`, `unlock`) VALUES
(1, 'Dashboard', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Company Profile', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'About', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Career', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Knowledge', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Portofolio', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Contact', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Data Website', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Agenda', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Download File', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Media', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Galeri Foto', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Galeri Video', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Newsletter', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Promosi', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Data Administration', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Master Kategori', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Master Tags', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Access Management', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Module Public', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Module Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Module Authority', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'User Management', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Role Management', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Backup and Log', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Log Activity Public', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Log Activity Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'Backup', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'App Configuration', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'Logo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'Logo User', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'Logo Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_newsletter`
--

CREATE TABLE `tb_newsletter` (
  `id_newsletter` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pesan` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_newsletter`
--

INSERT INTO `tb_newsletter` (`id_newsletter`, `nama`, `email`, `pesan`) VALUES
('NL-00001', 'Savira', 'savira@gmail.com', 'Jadwal');

-- --------------------------------------------------------

--
-- Table structure for table `tb_portofolio`
--

CREATE TABLE `tb_portofolio` (
  `id_portofolio` varchar(20) NOT NULL,
  `id_client` int(20) NOT NULL,
  `gambar` mediumblob NOT NULL,
  `nama_client` varchar(50) NOT NULL,
  `judul_portofolio` text NOT NULL,
  `desk_portofolio` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_portofolio`
--

INSERT INTO `tb_portofolio` (`id_portofolio`, `id_client`, `gambar`, `nama_client`, `judul_portofolio`, `desk_portofolio`) VALUES
('PR-00001', 2, 0x36326264363134386338616537313132303631395f627573696e6573736d616e5f636c69656e745f6d616e5f6d616e616765725f706572736f6e5f69636f6e2e706e67, '-', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `tb_promosi`
--

CREATE TABLE `tb_promosi` (
  `id_promosi` varchar(100) NOT NULL,
  `nama_promosi` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `keterangan` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_promosi`
--

INSERT INTO `tb_promosi` (`id_promosi`, `nama_promosi`, `tanggal`, `keterangan`) VALUES
('P-00001', 'Pembuatan Aplikasi', '22/06/01', 'Disc 10% pembuatan aplikasi sistem informasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_public`
--

CREATE TABLE `tb_public` (
  `id_public` varchar(225) NOT NULL,
  `nama_menu` varchar(225) DEFAULT NULL,
  `link` varchar(225) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `icon` varchar(225) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `urutan` int(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_public`
--

INSERT INTO `tb_public` (`id_public`, `nama_menu`, `link`, `keterangan`, `icon`, `status`, `urutan`) VALUES
('MP-00001', 'Home', '#home', 'Home', '-', 'Aktif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` varchar(225) NOT NULL,
  `role_name` varchar(225) DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `role_name`, `keterangan`) VALUES
('RM-00001', 'User', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sub_menu_admin`
--

CREATE TABLE `tb_sub_menu_admin` (
  `id` int(6) NOT NULL,
  `id_admin` int(6) NOT NULL,
  `nama_sub_menu` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_sub_menu_admin`
--

INSERT INTO `tb_sub_menu_admin` (`id`, `id_admin`, `nama_sub_menu`, `link`, `keterangan`, `icon`, `status`) VALUES
(1, 2, 'About', '/datamaster/about', 'About', NULL, 'enabled'),
(2, 2, 'Career', '/datamaster/career', 'Career', '', 'enabled'),
(3, 2, 'Knowledge', '/datamaster/knowledge', 'Knowledge', '', 'enabled'),
(4, 2, 'Portofolio', '/datamaster/portofolio', 'Portofolio', '', 'enabled'),
(5, 2, 'Contact', '/datamaster/contact', 'Contact', '', 'enabled'),
(6, 3, 'Agenda', '/website/agenda', 'Agenda', '', 'enabled'),
(7, 3, 'Download File', '/website/downloadfile', 'Download File', '', 'enabled'),
(8, 3, 'Media', '/media', 'Media', '', 'enabled'),
(9, 3, 'Newsletter', '/website/newsletter', 'Newsletter', '', 'enabled'),
(10, 3, 'Promosi', '/website/promosi', 'Promosi', '', 'enabled'),
(11, 4, 'Kategori', '/administration/kategori', 'Kategori', '', 'enabled'),
(12, 4, 'Tags', '/administration/tags', 'Tags', '', 'enabled'),
(13, 5, 'Public', '/management/public', 'Public', '', 'enabled'),
(14, 5, 'Admin', '/management/admin', 'Admin', '', 'enabled'),
(15, 5, 'Authority', '/management/authority', 'Authority', '', 'enabled'),
(16, 5, 'User Management', '/management/usermanagement', 'User Management', '', 'enabled'),
(17, 5, 'Role', '/management/role', 'Role', '', 'enabled'),
(18, 6, 'Log Public', '/backup/logpublic', 'Log Public', '', 'enabled'),
(19, 6, 'Log Admin', '/backup/logadmin', 'Log Admin', '', 'enabled'),
(20, 6, 'Backup', '/backup/backup', 'Backup', '', 'enabled'),
(21, 8, 'Logo Admin', '/logo/logoadmin', 'Logo Admin', '', 'enabled'),
(22, 8, 'Logo User', '/logo/logouser', 'Logo User', '', 'enabled'),
(23, 10, 'Customer', '/admin/service/pelanggan', 'Pelanggan', NULL, 'enabled'),
(27, 11, 'Costumer', '/admin/complain/costumer', 'Costumer', NULL, 'enabled'),
(28, 12, 'Costumer', '/admin/costumer/costumer', 'Costumer', NULL, 'enabled');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tags`
--

CREATE TABLE `tb_tags` (
  `id_tags` varchar(225) NOT NULL,
  `nama_tags` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_tags`
--

INSERT INTO `tb_tags` (`id_tags`, `nama_tags`) VALUES
('MT-00001', 'Mobile App');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `no` int(11) NOT NULL,
  `id_user` int(20) NOT NULL,
  `nama_user` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_management`
--

CREATE TABLE `tb_user_management` (
  `id_user_management` varchar(225) NOT NULL,
  `username` varchar(225) DEFAULT NULL,
  `nama` varchar(225) DEFAULT NULL,
  `role` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `update_password` varchar(100) NOT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user_management`
--

INSERT INTO `tb_user_management` (`id_user_management`, `username`, `nama`, `role`, `email`, `update_password`, `status`) VALUES
('UM-00001', 'savirayosita', 'Savira', 'Super User', 'savira@gmail.com', '-', 'Aktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_video`
--

CREATE TABLE `tb_video` (
  `id_video` varchar(100) NOT NULL,
  `nama_kegiatan` varchar(100) NOT NULL,
  `video` varchar(100) NOT NULL,
  `tanggal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_video`
--

INSERT INTO `tb_video` (`id_video`, `nama_kegiatan`, `video`, `tanggal`) VALUES
('V-00001', 'Rapat', '62b468754688aSavira Yosita_Demo Prototype.mp4', '22/06/01'),
('V-00002', 'safa', '62d11a3833bb9videoplayback.mp4', '22/07/23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admincrop@gmail.com', NULL, '$2y$10$/LLGzX4RY.v8wrJe0Mj0LuMIEiJjKheDhu1z69WvqUbBG8kHAibSi', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `tb_about`
--
ALTER TABLE `tb_about`
  ADD PRIMARY KEY (`id_about`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_agenda`
--
ALTER TABLE `tb_agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `tb_app_config`
--
ALTER TABLE `tb_app_config`
  ADD PRIMARY KEY (`id_app_config`);

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `tb_authority`
--
ALTER TABLE `tb_authority`
  ADD PRIMARY KEY (`id_authority`);

--
-- Indexes for table `tb_backup`
--
ALTER TABLE `tb_backup`
  ADD PRIMARY KEY (`id_backup`);

--
-- Indexes for table `tb_career`
--
ALTER TABLE `tb_career`
  ADD PRIMARY KEY (`id_career`);

--
-- Indexes for table `tb_contact`
--
ALTER TABLE `tb_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `tb_download_file`
--
ALTER TABLE `tb_download_file`
  ADD PRIMARY KEY (`id_download`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_knowledge`
--
ALTER TABLE `tb_knowledge`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_knowledge` (`id_knowledge`);

--
-- Indexes for table `tb_logo_admin`
--
ALTER TABLE `tb_logo_admin`
  ADD PRIMARY KEY (`id_logo_admin`);

--
-- Indexes for table `tb_logo_user`
--
ALTER TABLE `tb_logo_user`
  ADD PRIMARY KEY (`id_logo_user`);

--
-- Indexes for table `tb_log_admin`
--
ALTER TABLE `tb_log_admin`
  ADD PRIMARY KEY (`id_log_admin`);

--
-- Indexes for table `tb_log_public`
--
ALTER TABLE `tb_log_public`
  ADD PRIMARY KEY (`id_log_public`);

--
-- Indexes for table `tb_newsletter`
--
ALTER TABLE `tb_newsletter`
  ADD PRIMARY KEY (`id_newsletter`);

--
-- Indexes for table `tb_portofolio`
--
ALTER TABLE `tb_portofolio`
  ADD PRIMARY KEY (`id_portofolio`);

--
-- Indexes for table `tb_promosi`
--
ALTER TABLE `tb_promosi`
  ADD PRIMARY KEY (`id_promosi`);

--
-- Indexes for table `tb_public`
--
ALTER TABLE `tb_public`
  ADD PRIMARY KEY (`id_public`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tb_sub_menu_admin`
--
ALTER TABLE `tb_sub_menu_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tags`
--
ALTER TABLE `tb_tags`
  ADD PRIMARY KEY (`id_tags`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tb_user_management`
--
ALTER TABLE `tb_user_management`
  ADD PRIMARY KEY (`id_user_management`);

--
-- Indexes for table `tb_video`
--
ALTER TABLE `tb_video`
  ADD PRIMARY KEY (`id_video`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_app_config`
--
ALTER TABLE `tb_app_config`
  MODIFY `id_app_config` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_career`
--
ALTER TABLE `tb_career`
  MODIFY `id_career` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_knowledge`
--
ALTER TABLE `tb_knowledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_sub_menu_admin`
--
ALTER TABLE `tb_sub_menu_admin`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
